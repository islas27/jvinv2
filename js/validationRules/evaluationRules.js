$('#FRMEval').formValidation({
    framework: 'bootstrap',
    fields: {
        'p[]': {
            validators: {
                notEmpty: {
                    message: 'Debe de seleccionar un valor'
                }
            }
        },comentario:{
            validators:{
                notEmpty: {
                    message: 'No puede dejar este campo en blanco'
                },
                stringLength: {
                    max: 10000,
                    message: 'Máximo 10000 caracteres'
                },
                regexp: {
                    regexp: /^[a-zA-Z0-9áéíóúñüÁÉÍÓÚÑÜ.,¿¡?! ]+$/,
                    message: 'Utilize sólo letras y puntuaci&oacute;n básica'
                }
            }
        }
    }
});