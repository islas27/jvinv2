/**
 * Created by Jonathan on 4/6/2015.
 */

$('#registro').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
            titulo: {
                validators: {
                    notEmpty: {
                        message: 'El grado no puede estar vacío'
                    },
                    regexp: {
                        regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ. ]+$/,
                        message: 'Utilize sólo letras'
                    },stringLength: {
                        max: 200,
                        message: 'Debe de ser máximo 200 caracteres'
                    }
                }
            },nombre: {
                validators: {
                    notEmpty: {
                        message: 'El nombre no puede estar vacío'
                    },
                    regexp: {
                        regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                        message: 'Utilize sólo letras'
                    }
                }
            },apellido_pat: {
                validators: {
                    notEmpty: {
                        message: 'El apellido paterno no puede estar vacío'
                    },
                    regexp: {
                        regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                        message: 'Utilize sólo letras'
                    }
                }
            },apellido_mat: {
                validators: {
                    notEmpty: {
                        message: 'El apelido materno no puede estar vacío'
                    },
                    regexp: {
                        regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                        message: 'Utilize sólo letras'
                    }
                }
            },calle: {
                validators: {
                    notEmpty: {
                        message: 'El nombre de la calle no puede estar vacío'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9áéíóúñüÁÉÍÓÚÑÜ ]+$/,
                        message: 'Utilize sólo letras y números'
                    }
                }
            },numero_ext: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese el número exterior de la casa'
                    },
                    integer: {
                        message: 'Debe ser un número entero'
                    },
                    greaterThan:{
                        message: 'No puede ser igual o menor a 0',
                        value: 0
                    }
                }
            },numero_int: {
                validators: {
                    integer: {
                        message: 'Debe ser un número entero'
                    },
                    greaterThan:{
                        message: 'No puede ser igual o menor a 0',
                        value: 0
                    }
                }
            },colonia: {
                validators: {
                    notEmpty: {
                        message: 'El nombre de la colonia no puede estar vacío'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9áéíóúñüÁÉÍÓÚÑÜ ]+$/,
                        message: 'Utilize sólo letras y números'
                    }
                }
            },localidad: {
                validators: {
                    notEmpty: {
                        message: 'El nombre de la ciudad no puede estar vacío'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9áéíóúñüÁÉÍÓÚÑÜ ]+$/,
                        message: 'Utilize sólo letras y números'
                    }
                }
            },codigo_postal: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese un código postal válido'
                    },
                    integer: {
                        message: 'Debe ser un número entero'
                    },
                    stringLength: {
                        min: 5,
                        max: 5,
                        message: 'Debe de ser un número de 5 dígitos'
                    }
                }
            },municipio: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione el municipio donde radica'
                    }
                }
            },telefono_casa: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese un teléfono local válido'
                    },
                    integer: {
                        message: 'Debe ser un número entero'
                    },
                    stringLength: {
                        min: 10,
                        max: 10,
                        message: 'Debe de ser un número de 10 dígitos'
                    }
                }
            },telefono_cel: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese un número celular válido'
                    },
                    integer: {
                        message: 'Debe ser un número entero'
                    },
                    stringLength: {
                        min: 10,
                        max: 10,
                        message: 'Debe de ser un número de 10 dígitos'
                    }
                }
            },correo: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese un correo válido'
                    },emailAddress: {
                        message: 'Ingrese un correo válido'
                    }
                }
            },pass1: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese una contraseña válida'
                    },
                    stringLength: {
                        min: 6,
                        message: 'Debe de tener al menos 6 caractéres'
                    },regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: 'Utilize sólo letras, números y guiones bajos'
                    }, different: {
                        field: 'email',
                        message: 'No pueden ser iguales las contraseñas y el correo'
                    }
                }
            },pass2: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese una contraseña válida'
                    },
                    stringLength: {
                        min: 6,
                        message: 'Debe de tener al menos 6 caractéres'
                    },regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: 'Utilize sólo letras, números y guiones bajos'
                    }, identical: {
                        field: 'pass1',
                        message: 'Deben de coincidir las contraseñas'
                    }
                }
            },institucion: {
                validators:{
                    notEmpty:{
                        message: 'Debe de seleccionar la Institución a la que pertenece'
                    }
                }
            }
            ,facultad: {
                validators: {
                    notEmpty:{
                        message: 'Debe de introducir su facultad'
                    }
                    ,regexp: {
                        regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                        message: 'Utilize sólo letras'
                    }
                }
            },carrera: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese el nombre de la carrera que estudia(ó)'
                    },regexp: {
                        regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ. ]+$/,
                        message: 'Utilize sólo letras'
                    }
                }
            },situacion: {
                validators: {
                    notEmpty:{
                        message: 'Debe de seleccionar su situación actual'
                    }
                }
            },semestre: {
                validators: {
                    greaterThan:{
                        message: 'Debe de ser igual o mayor a 7',
                        value: 1
                    }
                }
            },meses: {
                validators: {
                    greaterThan:{
                        message: 'Debe de ser mayor a 0',
                        value: 1
                    }
                }
            }
        }
    });