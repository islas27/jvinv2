/**
 * Created by Jonathan on 5/2/2015.
 */

$('#registro').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        saludo: {
            validators: {
                notEmpty: {
                    message: 'El saludo no puede estar vacío'
                },
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ. ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        },titulo_academico: {
            validators: {
                notEmpty: {
                    message: 'El grado no puede estar vacío'
                },
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ. ]+$/,
                    message: 'Utilize sólo letras'
                },stringLength: {
                    max: 200,
                    message: 'Debe de ser máximo 200 caracteres'
                }
            }
        },nombre: {
            validators: {
                notEmpty: {
                    message: 'El nombre no puede estar vacío'
                },
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        },apellido_pat: {
            validators: {
                notEmpty: {
                    message: 'El apellido paterno no puede estar vacío'
                },
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        },apellido_mat: {
            validators: {
                notEmpty: {
                    message: 'El apelido materno no puede estar vacío'
                },
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        },localidad: {
            validators: {
                notEmpty: {
                    message: 'El nombre de la ciudad no puede estar vacío'
                },
                regexp: {
                    regexp: /^[a-zA-Z0-9áéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras y números'
                }
            }
        },municipio: {
            validators: {
                notEmpty: {
                    message: 'Seleccione el municipio donde radica'
                }
            }
        },telefono_casa: {
            validators: {
                notEmpty: {
                    message: 'Ingrese un teléfono local válido'
                },
                integer: {
                    message: 'Debe ser un número entero'
                },
                stringLength: {
                    min: 10,
                    max: 10,
                    message: 'Debe de ser un número de 10 dígitos'
                }
            }
        },telefono_cel: {
            validators: {
                notEmpty: {
                    message: 'Ingrese un número celular válido'
                },
                integer: {
                    message: 'Debe ser un número entero'
                },
                stringLength: {
                    min: 10,
                    max: 10,
                    message: 'Debe de ser un número de 10 dígitos'
                }
            }
        },correo: {
            validators: {
                notEmpty: {
                    message: 'Ingrese un correo válido'
                },emailAddress: {
                    message: 'Ingrese un correo válido'
                }
            }
        },institucion: {
            validators:{
                notEmpty:{
                    message: 'Debe de seleccionar la Institución a la que pertenece'
                }
            }
        },facultad: {
            validators: {
                notEmpty:{
                    message: 'Debe de introducir su facultad'
                }
                ,regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        },area:{
            validators:{
                notEmpty: {
                    message: 'Seleccione el area al que pertence'
                }
            }
        },modalidad:{
            validators:{
                notEmpty: {
                    message: 'Seleccione la modalidad en que trabajara'
                }
            }
        }
    }
});