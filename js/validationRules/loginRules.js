/**
 * Created by Jonathan on 4/6/2015.
 */

$(document).ready(function() {
    $('#login').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            correo: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese su correo electrónico'
                    },
                    emailAddress: {
                        message: 'Ingrese un correo válido'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese su contraseña'
                    },
                    stringLength: {
                        min: 6,
                        message: 'La contraseña contiene al menos 6 caracteres'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'No ingrese caracteres inválidos'
                    }
                }
            }
        }
    });
});