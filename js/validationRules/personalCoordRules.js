/**
 * Created by Jonathan on 5/2/2015.
 */

$('#coord').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        nombre: {
            validators: {
                notEmpty: {
                    message: 'El nombre no puede estar vacío'
                },
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        },apellido_pat: {
            validators: {
                notEmpty: {
                    message: 'El apellido paterno no puede estar vacío'
                },
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        },apellido_mat: {
            validators: {
                notEmpty: {
                    message: 'El apelido materno no puede estar vacío'
                },
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        },correo: {
            validators: {
                notEmpty: {
                    message: 'Ingrese un correo válido'
                },emailAddress: {
                    message: 'Ingrese un correo válido'
                }
            }
        },rol:{
            validators:{
                notEmpty: {
                    message: 'Seleccione el rol que desempeñara el usuario'
                }
            }
        }
    }
});