$('#registro_trabajo').formValidation({
    framework: 'bootstrap',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields:{
        titulo:{
            validators:{
                notEmpty:{
                    message: 'Ingrese el título de su trabajo'
                },regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ:_?¿ -]+$/,
                    message: 'Utilize sólo letras'
                },stringLength: {
                    max: 500,
                    message: 'Debe de ser máximo 500 caracteres'
                }
            }
        }, 'modalidad':{
            validators: {
                notEmpty: {
                    message: 'Seleccione el municipio donde radica'
                }
            }
        }, 'area':{
            validators: {
                notEmpty: {
                    message: 'Seleccione el municipio donde radica'
                }
            }
        }, 'asistente[]':{
            validators:{
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        },gradoAcademico: {
            validators: {
                notEmpty: {
                    message: 'El grado no puede estar vacío'
                },
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ. ]+$/,
                    message: 'Utilize sólo letras'
                },stringLength: {
                    max: 200,
                    message: 'Debe de ser máximo 200 caracteres'
                }
            }
        },nombreAsesor: {
            validators: {
                notEmpty: {
                    message: 'El nombre no puede estar vacío'
                },
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        },apellidosAsesor: {
            validators: {
                notEmpty: {
                    message: 'Los apellidos tienen que ser indicados'
                },
                regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        }, 'institucionAsesor':{
            validators: {
                notEmpty: {
                    message: 'Seleccione el municipio donde radica'
                }
            }
        },facultadAsesor: {
            validators: {
                notEmpty:{
                    message: 'Debe de introducir su facultad'
                }
                ,regexp: {
                    regexp: /^[a-zA-ZáéíóúñüÁÉÍÓÚÑÜ ]+$/,
                    message: 'Utilize sólo letras'
                }
            }
        },correoAsesor: {
            validators: {
                notEmpty: {
                    message: 'Ingrese un correo válido'
                },emailAddress: {
                    message: 'Ingrese un correo válido'
                }
            }
        }
    }
});