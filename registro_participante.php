<?php
    include("resources/connection.php");
    $conexion = my_connection();
    session_start();
    if($_SESSION['autenticacion'] === true){
        header("Location: inicio.php");
    }
    //Agregando una fecha automatizo el cierre de sesion
    if($_SESSION['autenticacion'] != true){
        header("Location: login.php?error=10");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <?php
        include("resources/navbar.php");
        include("resources/carrusel.php");
    ?>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1">
        <h2 style="text-align: center; margin-bottom: 24px">Participantes (Etapa 1/3)</h2>
    </div>
    <?php
        if(isset($_GET['error'])){
            include('resources/msg_loading.php');
            error_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
        }
        if(isset($_GET['success'])){
            include('resources/msg_loading.php');
            success_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['success']);
        }
    ?>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
        <form name="registro" id="registro" action="control/jovenCTL.php" method="post" class="form-horizontal">
            <!--Asi como usamos col para el espacio horizontal, tenemos row para el espacio vertical-->
            <div class="row" style="padding-left: 15px; padding-top: 15px">
                <p>Favor de llenar el siguiente formulario de registro.
                    <br>Es importante conservar su contraseña de usuario, ya que es necesaria para iniciar sesión
                    y poder subir el trabajo de investigación con el cual desea participar en el evento.<br><br></p>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <!--En acuerdo con la documentacio de bootstrap, los labels e inputs se ponen dentro de un-->
                    <!--contenedor con la clase "form-group"-->
                    <legend style="text-align: right">Datos Personales &nbsp; &nbsp;<span class="glyphicon glyphicon-user"></span></legend>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="nombre">Nombre</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="nombre" name="nombre"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="apellido_pat">Apellido Pat.</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="apellido_pat" name="apellido_pat"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="apellido_mat">Apellido Mat.</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="apellido_mat" name="apellido_mat"/>
                        </div>
                    </div>
                    <legend style="text-align: right">Dirección &nbsp; &nbsp;<span class="glyphicon glyphicon-home"></span></legend>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="calle">Calle</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="calle" name="calle"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="numero_ext"># Ext.</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="numero_ext" name="numero_ext"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="numero_int"># Int.</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="numero_int" name="numero_int"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="colonia">Colonia</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="colonia" name="colonia"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="codigo_postal">C. Postal</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="codigo_postal" name="codigo_postal"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="localidad">Ciudad</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="localidad" name="localidad"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="municipio">Municipio</label>
                        <div class="col-md-8 selectContainer">
                            <select class="form-control input-sm" id="municipio" name="municipio">
                                <option value="">Seleccionar</option>
                                <?php
                                $query = 'select id_municipio, nombre_municipio from municipios;';
                                $resultado = $conexion->query($query);
                                $resultado->data_seek(0);
                                while($registro = $resultado->fetch_assoc()){
                                    echo '<option value="'.$registro['id_municipio'].'">'.$registro['nombre_municipio'].'</option>';
                                }
                                $resultado->free();
                                ?>
                            </select>
                        </div>
                    </div>
                    <legend style="text-align: right">Contacto &nbsp; &nbsp;<span class="glyphicon glyphicon-phone-alt"></span></legend>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="telefono_casa">Teléfono</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="telefono_casa" name="telefono_casa"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="telefono_cel">Celular</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="telefono_cel" name="telefono_cel"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <legend style="text-align: right">Datos de Usuario &nbsp; &nbsp;<span class="glyphicon glyphicon-cog"></span></legend>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="correo">Correo</label>
                        <div class="col-md-8">
                            <input type="email" class="form-control input-sm" id="correo" name="correo" placeholder="Correo Electrónico"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="pass1">Contraseña</label>
                        <div class="col-md-8">
                            <input type="password" class="form-control input-sm" placeholder="Contraseña" id="pass1" name="pass1"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="pass2"> </label>
                        <div class="col-md-8">
                            <input type="password" class="form-control input-sm" placeholder="Reingrese Contraseña" id="pass2" name="pass2"/>
                        </div>
                    </div>
                    <legend style="text-align: right">Situación Académica &nbsp; &nbsp;<span class="glyphicon glyphicon-pencil"></span></legend>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Seleccione su status:</label>
                        <div class="col-md-8">
                            <div class="radio">
                                <label for="situacion1">
                                    <input name="situacion" id="situacion1" value="1" type="radio" >
                                    Estudiante
                                </label>
                            </div>
                            <div class="radio">
                                <label for="situacion2">
                                    <input name="situacion" id="situacion2" value="2" type="radio" >
                                    Egresado
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="semestre">Semestre actual: </label>
                        <div class="col-md-8 selectContainer">
                            <select class="form-control input-sm" id="semestre" name="semestre">
                                <option value="0">Seleccionar</option>
                                <?php for($x = 7; $x <= 10; $x++){echo '<option value="'.$x.'">'.$x.'</option>';}?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="meses">Tiempo de egresado: </label>
                        <div class="col-md-8 selectContainer">
                            <select class="form-control input-sm" id="meses" name="meses">
                                <option value="0">Seleccionar</option>
                                <?php for($x = 1; $x <= 6; $x++){echo '<option value="'.$x.'">'.$x.' meses</option>';}?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="institucion">I. de Educación Superior:</label>
                        <div class="col-md-8 selectContainer">
                            <select class="form-control input-sm" id="institucion" name="institucion" required="required">
                                <option value="">Seleccionar</option>
                                <?php
                                $query = 'select id_institucion, nombre_institucion from instituciones;';
                                $resultado = $conexion->query($query);
                                $resultado->data_seek(0);
                                while($registro = $resultado->fetch_assoc()){
                                    echo '<option value="'.$registro['id_institucion'].'">'
                                        .$registro['nombre_institucion'].'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="facultad">Facultad, Instituto o Escuela:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="facultad" name="facultad"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="carrera">Carrera:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm" id="carrera" name="carrera"/>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group">
                        <label class="control-label col-xs-3"></label>
                        <div class="col-xs-6">
                            <button name="cmdAction" type="submit" value="0" class="btn btn-primary col-xs-12">Registrarse!</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <?php include("resources/footer.php");?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!-- FormValidation plugin and the class supports validating Bootstrap form -->
    <script src="js/formValidation/formValidation.min.js"></script>
    <script src="js/formValidation/bootstrap.min.js"></script>
    <!--FormValidation rules-->
    <script src="js/validationRules/contestantRules.js"></script>
    <script type="text/javascript">
        $(window).load(function(){
            $('#meses').parent().parent().hide();
            $('#semestre').parent().parent().hide();
        });
        $('#situacion1').click(function(){
            $('#semestre').parent().parent().show();
            $('#meses').parent().parent().hide().val('0');
        });
        $('#situacion2').click(function(){
            $('#semestre').parent().parent().hide().val('0');
            $('#meses').parent().parent().show();
        });
    </script>
</body>
</html>