# Jóvenes Investigadores del Estado de Chihuahua #

Plataforma de registro, administración y evaluación del concurso Jóvenes Investigadores en el estado de Chihuahua - Version Estatal 2015

### Versión ###

* 2.0
* Actualizado para la version 2015 del certamen
* Mejoras en rendimiento, seguridad y calidad del código vs V1

### Requerimientos ###

* PHP >= 5.3
* MySQL
* Actualizar archivo de las variables de servidor para conectar correctamente con la con la base de datos (resources/server_vars.php)
* Cargar Esquema de la base de datos a MySQL

### To-Do ###

* Escribir Script de inicialización para probar la aplicación (usuarios administradores, capturistas, evaluadores y participantes)
* Optimizar el uso de fechas automáticas y uso de archivos de configuración