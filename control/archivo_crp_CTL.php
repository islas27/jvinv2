<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/19/2015
 * Time: 10:40 PM
 */

if (empty($_FILES['curp'])) {
    echo json_encode(['error'=>'No files found for upload.']);
    return;
}

session_start();
require("../resources/functions.php");
require("../resources/server_vars.php");

$id_participante = $_SESSION['id_participante'];
$nva_curp = "archive/$year/curp/curp_".$id_participante."_".random_string(6).".pdf";
$curp = $_FILES['curp'];

if(rename($curp['tmp_name'],$_SERVER['DOCUMENT_ROOT'].$folder.$nva_curp) &&
    guardarRuta($id_participante, $nva_curp, 'curp')){
    $output = ['uploaded' => $nva_curp];
}else{
    $path = $_SERVER['DOCUMENT_ROOT'].$folder.$nva_curp;
    error_log("Transaction failed: $path is the path to save", 3, "errorlog.txt");
    $output = ['error'=>'Error while uploading images. Contact the system administrator'];
    unlink($_SERVER['DOCUMENT_ROOT'].$folder.$nva_curp);
}

echo json_encode($output);
