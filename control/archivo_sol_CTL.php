<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/19/2015
 * Time: 8:31 PM
 */

if (empty($_FILES['solicitud'])) {
    echo json_encode(['error'=>'No files found for upload.']);
    return;
}

session_start();
require("../resources/functions.php");
require("../resources/server_vars.php");

$id_participante = $_SESSION['id_participante'];
$nva_solicitud = "archive/$year/solicitud/solicitud_".$id_participante."_".random_string(6).".pdf";
$solicitud = $_FILES['solicitud'];

if(rename($solicitud['tmp_name'],$_SERVER['DOCUMENT_ROOT'].$folder.$nva_solicitud) &&
    guardarRuta($id_participante, $nva_solicitud, 'solicitud')){
        $output = ['uploaded' => $nvo_trabajo];
}else{
    $path = $_SERVER['DOCUMENT_ROOT'].$folder.$nva_solicitud;
    error_log("Transaction failed: $path is the path to save", 3, "errorlog.txt");
    $output = ['error'=>'Error while uploading images. Contact the system administrator'];
    unlink($_SERVER['DOCUMENT_ROOT'].$folder.$nva_solicitud);
}

echo json_encode($output);
