<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/26/2015
 * Time: 6:32 PM
 */
function random_string($length) {
    $key = '';
    $keys = range('A', 'Z');
    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }
    return $key;
}

if(isset($_POST['nombre']) || isset($_GET['id'])){
    include("../resources/connection.php");
    include("send_mail.php");

    if (version_compare(PHP_VERSION, '5.5.0', '<')) {
        include("../resources/password_compat_lib.php");
    }

    $conn = my_connection();
    $option = (isset($_GET['id']))? $_GET['cmdAction'] : $_POST['cmdAction'];
    switch($option){
        case 0://agregar datos
            $nombre = $_POST['nombre'];
            $apellido_pat = $_POST['apellido_pat'];
            $apellido_mat = $_POST['apellido_mat'];
            $correo = $_POST['correo'];
            $pass_unhashed = random_string(8);
            $pass = password_hash($pass_unhashed, PASSWORD_BCRYPT);
            $status = "ON";
            $tipo_usuario = $_POST['rol'];
            $query = "select * from usuarios where correo='$correo';";
            $resultado = $conn->query($query);
            if(!$registro = $resultado->fetch_assoc()){
                try {

                    $sql = "insert into usuarios
                              (correo,password, status, fecha_alta, tipo_usuario, nombre, apellido_pat, apellido_mat) VALUES
                              (?, ?, ?, now(), ?, ?, ?, ?);";
                    $stmt = $conn->prepare($sql);
                    if($stmt === false) {
                        throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $conn->error);
                    }
                    /* Bind parameters. Types: s = string, i = integer, d = double,  b = blob */
                    $stmt->bind_param('sssisss',$correo, $pass, $status, $tipo_usuario, $nombre, $apellido_pat, $apellido_mat);
                    /* Execute statement */
                    $stmt->execute();
                    if($stmt->errno > 0) {
                        throw new Exception('Wrong SQL execute 1, Error: ' . $stmt->error);
                    }

                } catch (Exception $e) {
                    $error = $e->getMessage();
                    error_log("Transaction failed: $error\n On Controller jovenCTL.php \n", 3, "error_log.txt");
                    header("Location: ../reporte_coordinacion.php?error=4");//error desconocido con la query o intento de injeccion....
                    exit();
                }

                $id = $conn->insert_id;
                enviar_mail_nuevo_usuario($correo,$pass_unhashed, $conn);
                header("Location: ../reporte_coordinacion.php?success=6");//agregado con exito
                exit();
            }else{
                header("Location: ../reporte_coordinacion.php?error=6");//usuario repetido...
                exit();
            }
            break;
        case 2://Desactivar Participante
            $id = $_GET['id'];
            $query = "update usuarios set status = 'OFF' WHERE id_usuario = $id";
            $conn->query($query);
            if($conn->affected_rows > 0){
                header("Location: ../reporte_participantes.php?success=7");//Eliminado con exito
                exit();
            }else{
                header("Location: ../reporte_participantes.php?error=4");//error desconocido con la query o intento de injeccion....
                exit();
            }
            break;

        case 3://Desactivar Evaluador
            $id = $_GET['id'];
            $query = "update usuarios set status = 'OFF' WHERE id_usuario = $id";
            $conn->query($query);
            if($conn->affected_rows > 0){
                header("Location: ../reporte_evaluadores.php?success=7");//Eliminado con exito
                exit();
            }else{
                header("Location: ../reporte_evaluadores.php?error=4");//error desconocido con la query o intento de injeccion....
                exit();
            }
            break;
        case 4://Desactivar usuario de coordinacion
            $id = $_GET['id'];
            $query = "update usuarios set status = 'OFF' WHERE id_usuario = $id";
            $conn->query($query);
            if($conn->affected_rows > 0){
                header("Location: ../reporte_coordinacion.php?success=7");//Eliminado con exito
                exit();
            }else{
                header("Location: ../reporte_coordinacion.php?error=4");//error desconocido con la query o intento de injeccion....
                exit();
            }
            break;
        case 5://Enviar correo simplificado
            if(send_mail($_POST['correo'],$_POST['nombre'],$_POST['asunto'], $_POST['mensaje'])){
                header("Location: ../enviar_mail.php?success=8");
            }else{
                header("Location: ../enviar_mail.php?error=8");
            }
            break;
        case 6://veredicto final
            $id_participante = $_GET['id'];
            $id_usuario = $_GET['id_usuario'];
            $veredicto = $_GET['veredicto'];
            $query = "update participantes set veredicto = '$veredicto' WHERE id_participante = $id_participante";
            $conn->query($query);
            if($conn->affected_rows > 0){
                header("Location: ../reporte_participante.php?success=10&id=$id_usuario");//Eliminado con exito
                exit();
            }else{
                header("Location: ../reporte_participante.php?error=4&id=$id_usuario");//error desconocido con la query o intento de injeccion....
                exit();
            }
            break;
            break;
    }
}else{
    header("Location: ../reporte_coordinacion.php?error=2"); //Se entro sin informacion relevante...
    exit();
}