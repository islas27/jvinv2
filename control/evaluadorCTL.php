<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/27/2015
 * Time: 2:45 PM
 */
function random_string($length) {
    $key = '';
    $keys = range('A', 'Z');
    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }
    return $key;
}

    if(isset($_POST['nombre']) || isset($_GET['id'])){
        include("../resources/connection.php");
        include("send_mail.php");
        if (version_compare(PHP_VERSION, '5.5.0', '<')) {
            include("../resources/password_compat_lib.php");
        }
        $conn = my_connection();
        session_start();
        (isset($_GET['id']))? $option = $_GET['cmdAction'] : $option = $_POST['cmdAction'];
        switch($option){
            case 0://agregar datos
                $saludo = $_POST['saludo'];
                $titulo = $_POST['titulo_academico'];
                $nombre = $_POST['nombre'];
                $apellido_pat = $_POST['apellido_pat'];
                $apellido_mat = $_POST['apellido_mat'];
                $localidad = $_POST['localidad'];
                $id_municipio = $_POST['municipio'];
                $telefono_casa = $_POST['telefono_casa'];
                $telefono_cel = $_POST['telefono_cel'];
                $correo = $_POST['correo'];
                $unhashed_pass = random_string(8);
                $pass = password_hash($unhashed_pass, PASSWORD_BCRYPT);
                $id_institucion = $_POST['institucion'];
                $facultad = $_POST['facultad'];
                $esSNI = ($_POST['esSNI'])?'Si':'No';
                $nivelSNI = $_POST['nivelSNI'];
                $esPROMEP = ($_POST['esPROMEP'])?'Si':'No';
                $cuerpoAcademico = $_POST['cuerpoAcademico'];
                $claveCA = $_POST['claveCA'];
                $esConsolidado = $_POST['esConsolidado'];
                $area = $_POST['area'];
                $modalidad = $_POST['modalidad'];
                $creado_por = $_SESSION['id_usuario'];
                $status = "ON";
                $tipo_usuario = 4;
                $query = "select * from usuarios where correo='$correo';";
                $resultado = $conn->query($query);
                if(!$registro = $resultado->fetch_assoc()){
                    try {
                        /* switch autocommit status to FALSE. Actually, it starts transaction */
                        $conn->autocommit(FALSE);

                        $sql = "insert into usuarios
                              (correo,password, status, fecha_alta, tipo_usuario, nombre, apellido_pat, apellido_mat) VALUES
                              (?, ?, ?, now(), ?, ?, ?, ?);";
                        $stmt = $conn->prepare($sql);
                        if($stmt === false) {
                            throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $conn->error);
                        }
                        /* Bind parameters. Types: s = string, i = integer, d = double,  b = blob */
                        $stmt->bind_param('sssisss',$correo, $pass, $status, $tipo_usuario, $nombre, $apellido_pat, $apellido_mat);
                        /* Execute statement */
                        $stmt->execute();
                        if($stmt->errno > 0) {
                            throw new Exception('Wrong SQL execute 1, Error: ' . $stmt->error);
                        }

                        $stmt->close();

                        $sql = "insert into evaluadores
                              (abreviatura_titulo,titulo,correo, id_municipio, ciudad, telefono_casa, telefono_cel, id_institucion,
                               facultad, id_area, id_modalidad, es_sni, nivel_sni, es_promep, cuerpo_academico,
                               clave_ca, es_consolidado, fecha_registro,creado_por)
                                  values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, now(),?);";
                        $stmt = $conn->prepare($sql);
                        if($stmt === false) {
                            throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $conn->error);
                        }
                        $stmt->bind_param('sssisssisiissssssi',$saludo, $titulo, $correo, $id_municipio, $localidad, $telefono_casa, $telefono_cel,
                            $id_institucion, $facultad, $area, $modalidad, $esSNI, $nivelSNI,$esPROMEP, $cuerpoAcademico,
                            $claveCA, $esConsolidado, $creado_por);
                        $stmt->execute();
                        if($stmt->errno > 0) {
                            throw new Exception('Wrong SQL execute 2, Error: ' . $stmt->error);
                        }
                        $stmt->close();

                        $conn->commit();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                        error_log("Transaction failed: $error\n On Controller jovenCTL.php \n", 3, "error_log.txt");
                        $conn->rollback();
                        $conn->autocommit(TRUE);
                        header("Location: ../registro_evaluador.php?error=4");//error desconocido con la query o intento de injeccion....
                        exit();
                    }
                    $conn->autocommit(TRUE);
                    enviar_mail_evaluador($correo, $conn, $unhashed_pass);
                    header("Location: ../registro_evaluador.php?success=5");//agregado con exito
                    exit();
                }else{
                    header("Location: ../registro_evaluador.php?error=7");//usuario repetido...
                    exit();
                }
                break;

            case 2://reeenviar mail de confirmacion
                $id = $_GET['id'];
                $query = "select * from usuarios where id_usuario=$id;";
                $resultado = $conn->query($query);
                $registro = $resultado->fetch_assoc();
                $correo = $registro['correo'];
                $unhashed_pass = random_string(8);
                $pass = password_hash($unhashed_pass, PASSWORD_BCRYPT);
                $query = "update usuarios set password = '$pass' where id_usuario = $id";
                $conn->query($query);
                if(enviar_mail_evaluador($correo, $conn, $unhashed_pass)){
                    header("Location: ../reporte_evaluadores.php?success=8");
                    exit();
                }else{
                    header("Location: ../reporte_evaluadores.php?error=8");
                    exit();
                }

                break;
        }
    }else{
        header("Location: ../registro_evaluador.php?error=2"); //Se entro sin informacion relevante...
        exit();
    }