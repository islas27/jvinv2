<?php
    session_start();
    if(isset($_POST['titulo']) || isset($_GET['id_trabajo']) || isset($_POST['id_trabajo'])){
        include("../resources/connection.php");
        $conn = my_connection();
        $option = (!isset($_GET['cmdAction']))?$_POST['cmdAction']:$_GET['cmdAction'];
        switch($option){
            case 0://agregar datos
                $id_participante = $_SESSION['id_participante'];
                $titulo = $_POST['titulo'];
                $id_area = $_POST['area'];
                $id_modalidad = $_POST['modalidad'];
                $colaborador_1 = $_POST['asistente'][0];
                $colaborador_2 = $_POST['asistente'][1];
                $colaborador_3 = $_POST['asistente'][2];
                $grado_academico = $_POST['gradoAcademico'];
                $nombre_asesor = $_POST['nombreAsesor'];
                $apellidos_asesor  = $_POST['apellidosAsesor'];
                $id_institucion_asesor = $_POST['institucionAsesor'];
                $facultad_asesor = $_POST['facultadAsesor'];
                $correo_asesor = $_POST['correoAsesor'];
                $sql = "insert into trabajos (id_participante, titulo, id_area, id_modalidad, colaborador_1,
                          colaborador_2, colaborador_3, grado_academico, nombre_asesor, apellidos_asesor,
                          id_institucion_asesor, facultad_asesor, correo_asesor, fecha_registro) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?, now())";
                $stmt = $conn->prepare($sql);
                if($stmt === false) {
                    error_log('Wrong SQL: ' . $sql . ' Error: ' . $conn->error. 'On trabajoCTL.php', 3, "error_log");
                }
                /* Bind parameters. TYpes: s = string, i = integer, d = double,  b = blob */
                $stmt->bind_param('isiissssssiss', $id_participante, $titulo, $id_area, $id_modalidad, $colaborador_1,
                                    $colaborador_2, $colaborador_3, $grado_academico, $nombre_asesor, $apellidos_asesor,
                                    $id_institucion_asesor, $facultad_asesor, $correo_asesor);
                $stmt->execute();
                if($stmt->errno > 0){
                    header("Location: ../registro_trabajo.php?error=4");//error de ejecucion de query...
                    exit();
                }else{
                    $_SESSION['trabajo_registrado'] = true;
                    header("Location: ../registro_archivos.php?success=2");//agregado con exito
                    exit();
                }
                break;
            case 1://Guardar calif
                $evaluador = $_POST['id_evaluador'];
                $trabajo = $_POST['id_trabajo'];
                $calif = $_POST['p'];
                $calif_total = array_sum($calif);
                $comentario = $_POST['comentario'];
                $sql = "update evaluaciones set calif_1 = $calif[0], calif_2 = $calif[1], calif_3 = $calif[2],
                        calif_4 = $calif[3], calif_5 = $calif[4], calif_6 = $calif[5], calif_7 = $calif[6],
                        calif_8 = $calif[7], calif_9 = $calif[8], calif_10 = $calif[9], calif_total = $calif_total,
                        comentario = '$comentario', fecha_evaluacion = now()
                        where id_evaluador = $evaluador and id_trabajo = $trabajo";
                $resultado = $conn->query($sql);
                   if($resultado){
                       header("Location: ../trabajos_asignados.php?success=4");
                       EXIT();
                   }else{
                       header("Location: ../trabajos_asignados.php?error=9");
                       exit();
                   }
                break;
            case 2://asignacion evaluadores
                $id_trabajo =  $_GET['id_trabajo'];
                $id_participante= $_GET['id_participante'];
                $id_usuario = $_GET['id_usuario'];
                $previa = $_GET['previa'];
                $usuario_mod = $_SESSION['id_usuario'];
                if($previa == 'Si'){
                    $id_1 = $_GET['evaluadores'][0];
                    $id_2 = $_GET['evaluadores'][1];
                    $id_3 = $_GET['evaluadores'][2];
                    try {
                        /* switch autocommit status to FALSE. Actually, it starts transaction */
                        $conn->autocommit(FALSE);

                        $sql = "insert into evaluaciones (id_trabajo, id_evaluador, asignado_por, fecha_asignacion) VALUES (?,?,?, now())";
                        $stmt = $conn->prepare($sql);
                        if($stmt === false) {
                            throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $conn->error);
                        }
                        /* Bind parameters. Types: s = string, i = integer, d = double,  b = blob */
                        $stmt->bind_param('iii',$id_trabajo, $id_1, $_SESSION['id_usuario']);
                        /* Execute statement */
                        $stmt->execute();
                        if($stmt->errno > 0) {
                            throw new Exception('Wrong SQL execute 1, Error: ' . $stmt->error);
                        }
                        $stmt->bind_param('iii',$id_trabajo, $id_2, $_SESSION['id_usuario']);
                        $stmt->execute();
                        if($stmt->errno > 0) {
                            throw new Exception('Wrong SQL execute 2, Error: ' . $stmt->error);
                        }
                        $stmt->bind_param('iii',$id_trabajo, $id_3, $_SESSION['id_usuario']);
                        $stmt->execute();
                        if($stmt->errno > 0) {
                            throw new Exception('Wrong SQL execute 3, Error: ' . $stmt->error);
                        }
                        $sql = "update participantes set eval_previa = 'Verificado', fecha_u_mod = now(), u_mod_por = ?
                                where id_participante = $id_participante";
                        $stmt = $conn->prepare($sql);
                        if($stmt === false) {
                            throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $conn->error);
                        }
                        $stmt->bind_param('i', $_SESSION['id_usuario']);
                        $stmt->execute();
                        if($stmt->errno > 0) {
                            throw new Exception('Wrong SQL execute 4, Error: ' . $stmt->error);
                        }
                        $stmt->close();

                        $conn->commit();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                        error_log("Transaction failed: $error\n On Controller jovenCTL.php \n", 3, "error_log.txt");
                        $conn->rollback();
                        $conn->autocommit(TRUE);
                        header("Location: ../reporte_participante.php?error=4&id=$id_usuario");//error desconocido con la query o intento de injeccion....
                        exit();
                    }
                    $conn->autocommit(TRUE);
                    include("send_mail.php");
                    trabajoAsignado($id_1,$id_trabajo, $id_participante,$conn);
                    trabajoAsignado($id_2,$id_trabajo, $id_participante,$conn);
                    trabajoAsignado($id_3,$id_trabajo, $id_participante,$conn);
                    header("Location: ../reporte_participante.php?success=9&id=$id_usuario");//agregado con exito
                    exit();
                }else{
                    $query = "update participantes set eval_previa = 'Rechazado', fecha_u_mod = now(), u_mod_por = $usuario_mod
                              where id_participante = $id_participante;";
                    $conn->query($query);
                    header("Location: ../reporte_participante.php?error=4&id=$id_usuario");//trabajo rechazado...
                    exit();
                }
                break;
        }
    }else{
        header("Location: ../login.php?error=1"); //Se entro sin informacion relevante...
        exit();
    }