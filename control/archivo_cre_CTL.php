<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/19/2015
 * Time: 10:39 PM
 */

if (empty($_FILES['credencial'])) {
    echo json_encode(['error'=>'No files found for upload.']);
    return;
}

session_start();
require("../resources/functions.php");
require("../resources/server_vars.php");

$id_participante = $_SESSION['id_participante'];
$nva_credencial = "archive/$year/credencial/credencial_".$id_participante."_".random_string(6).".pdf";
$credencial = $_FILES['credencial'];

if(rename($credencial['tmp_name'],$_SERVER['DOCUMENT_ROOT'].$folder.$nva_credencial) &&
    guardarRuta($id_participante, $nva_credencial, 'credencial')){
    $output = ['uploaded' => $nva_credencial];
}else{
    $path = $_SERVER['DOCUMENT_ROOT'].$folder.$nva_credencial;
    error_log("Transaction failed: $path is the path to save", 3, "errorlog.txt");
    $output = ['error'=>'Error while uploading images. Contact the system administrator'];
    unlink($_SERVER['DOCUMENT_ROOT'].$folder.$nva_credencial);
}

echo json_encode($output);
