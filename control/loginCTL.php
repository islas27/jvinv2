<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/6/2015
 * Time: 10:01 PM
 */
if(isset($_POST['correo'])){
    include("../resources/connection.php");
    if (version_compare(PHP_VERSION, '5.5.0', '<')) {
        include("../resources/password_compat_lib.php");
    }
    $conn = my_connection();
    $correo = $_POST['correo'];
    $pass = $_POST['password'];
    $query = "select concat_ws(' ', nombre, apellido_pat) as name, apellido_mat, correo, password, id_usuario, tipo_usuario,
                status from usuarios WHERE correo='$correo';";
    $resultado = $conn->query($query);
    if($registro = $resultado->fetch_assoc()){
        if((password_verify($pass, $registro['password'])) and ($registro['status'] == 'ON')){
            try {
                $sql = "update usuarios set fecha_u_acceso = now() where correo = '$correo';";
                $resultado = $conn->query($sql);
                if($conn->affected_rows == 0) {
                    throw new Exception('Wrong SQL execute , Error: ' . $stmt->error);
                }
            } catch (Exception $e) {
                $error = $e->getMessage();
                error_log("Log on failed: $error \n On Controller loginCTL.php \n ", 3, "error_log.txt");
                header("Location: ../login.php?error=4");//error desconocido con la query o intento de injeccion....
                exit();
            }
            session_start();
            //Aqui puedo agregar una fecha para comparar y que
            // se cierre automaticamente el registro
            if($registro['tipo_usuario'] == 5){
                header("Location: ../login.php?error=10");
                exit();
            }else{
                //puedo repetir la comparacion anterior pero con el tipo = 4
                // y una fecha para cerrar las evaluaciones
                $_SESSION['name'] = $registro['name'];
                $_SESSION['apellido_mat'] = $registro['apellido_mat'];
                $_SESSION['id_usuario'] = $registro['id_usuario'];
                $_SESSION['correo'] = $registro['correo'];
                $_SESSION['tipo_usuario'] = $registro['tipo_usuario'];
                $_SESSION['autenticacion'] = true;
                header("Location: ../inicio.php");//Inicio de sesion correcto
                exit();
            }
        }
    }
    header("Location: ../login.php?error=3");//usuario invalido o no registrado...
    exit();
}else{
    header("Location: ../login.php?error=1"); //Se entro sin informacion relevante...
    exit();
}