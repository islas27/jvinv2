<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/21/2015
 * Time: 12:12 PM
 */

include("send_mail.php");
include("../resources/connection.php");
$conexion = my_connection();
$pass = 1212312;

$query = "select * from usuarios
                LEFT JOIN evaluadores on usuarios.correo = evaluadores.correo
                left join instituciones on evaluadores.id_institucion = instituciones.id_institucion
                left join area_conocimiento on evaluadores.id_area = area_conocimiento.id_area
                left join municipios on evaluadores.id_municipio = municipios.id_municipio
                where usuarios.correo='a@b.com';";
$resultado = $conexion->query($query);
$registro = $resultado->fetch_assoc();
$claveCA = ($registro['clave_ca'] == null)?'No cuenta con clave':'UACHIH-CA-'.$registro['clave_ca'];
$cuerpo = '<br>Esta es la informaci&oacute;n que usted tiene registrada dentro de la plataforma:<br>
                <br>Informaci&oacute;n Personal:
                <br>Nombre: '.$registro['titulo'].' '.$registro['nombre'].' '.$registro['apellido_pat'].' '.$registro['apellido_mat'].
    '<br>Localidad: '.$registro['ciudad'].', '.$registro['nombre_municipio'].
    '<br>Tel&eacute;fonos: '.$registro['telefono_casa'].', '.$registro['telefono_cel'].
    '<br>Correo electr&oacute;nico: '.$registro['correo'].
    '<br>Instituci&oacute;n: '.$registro['nombre_institucion'].
    '<br>Facultad, Instituto o Escuela: '.$registro['facultad'].
    '<br>&Aacute;rea de conocimiento: '.$registro['nombre_area'].
    '<br><br>Distinciones como investigador:'.
    '<br>Miembro del S.N.I: '.$registro['es_sni'].
    '<br>Nivel: '.$registro['nivel_sni'].
    '<br>Perfil PROMEP: '.$registro['es_promep'].
    '<br>Miembro de alg&uacute;n cuerpo academico de la UACH: '.$registro['cuerpo_academico'].
    '<br>Clave: '.$claveCA.
    '<br><br>Contrase&ntilde;a: '.$pass.
    '<br><br>A partir del d&iacute;a lunes 14 de mayo del 2015, la plataforma estar&aacute; abierta para que proceda a calificar los trabajos.
                Utilice el correo donde recibe esta notificaci&oacute;n como usuario y la contrase&ntilde;a aqu&iacute; escrita para poder iniciar sesi&oacute;n
                en el siguiente enlace: <a href="http://jovenesinvestigadores.uach.mx/registro/">Login</a> en la pesta&ntilde;a de evaluadores.'.
    '<br><br>Si usted nota que alguna parte de esta informaci&oacute;n esta incorrecta, incompleta as&iacute;
                como un cambio reciente en alg&uacute;n dato, contacte a la coordinaci&oacute;n, y se le informar&aacute; de que puede proceder.<br>
                <br>Atte.<br> Jonathan Islas<br>Administrador de la Plataforma.<br><br>';

echo $cuerpo;