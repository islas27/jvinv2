<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/26/2015
 * Time: 9:19 PM
 */


function send_mail($correo,$nombre,$asunto,$cuerpo){
    require("../resources/mailTemplate.php");
    /** @var $head String */
    /** @var $middle String */
    /** @var $tail String */
    $mensaje = $head.$asunto.$middle.$cuerpo.$tail;
    // Swift Mailer Library
    require_once '../resources/swiftMailer/swift_required.php';
    // Mail Transport
    $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
        ->setUsername('coord.jinvestigadoresuach@gmail.com') // Your Gmail Username
        ->setPassword('coordinacion2014'); // Your Gmail Password
    // Mailer
    $mailer = Swift_Mailer::newInstance($transport);
    // Create a message
    $message = Swift_Message::newInstance($asunto)
        ->setFrom(array('coord.jinvestigadoresuach@gmail.com' => 'Coordinación J. Investigadores')) // can be $_POST['email'] etc...
        ->setTo(array($correo => $nombre)) // your email / multiple supported.
        ->setBody($mensaje, 'text/html');

    // Send the message
    if ($mailer->send($message)) {
        //echo $mensaje;
        $echo = true;
    } else {
        $echo = false;
    }
    return $echo;
}

function enviar_mail_confirmacion($id_usuario, $conexion){
    /** @var mysqli $conexion */
    $query = "select * from usuarios
        LEFT JOIN participantes ON usuarios.correo = participantes.correo
        LEFT JOIN trabajos ON participantes.id_participante = trabajos.id_participante
        LEFT JOIN instituciones on instituciones.id_institucion = participantes.id_institucion
        LEFT JOIN municipios ON municipios.id_municipio = participantes.id_municipio
        LEFT JOIN area_conocimiento ON trabajos.id_area = area_conocimiento.id_area
        LEFT JOIN modalidad ON trabajos.id_modalidad = modalidad.id_modalidad
                    where id_usuario = $id_usuario";
    $resultado = $conexion->query($query);
    $registro = $resultado->fetch_assoc();
    $query = "select * from instituciones where id_institucion = ".$registro['id_institucion_asesor'].";" ;
    $resultado = $conexion->query($query);
    $registro2 = $resultado->fetch_assoc();
    $estatus = ($registro['es_estudiante'] == 1)?'Estudiante con '.$registro['semestre'].' semestres'
        :'Egresado con '.$registro['tiempo_egresado'].' meses de haber egresado';
    $cuerpo = '<br>Esta es la informaci&oacute;n que usted ha registrado para su participaci&oacute;n:<br>
                <br>Informaci&oacute;n Personal:
                <br>Nombre: '.$registro['nombre'].' '.$registro['apellido_pat'].' '.$registro['apellido_mat'].
        '<br>Domicilio: '.$registro['calle'].' '.$registro['num_ext'].', '.$registro['colonia'].', CP '.$registro['codigo_postal'].
        '<br>Localidad: '.$registro['localidad'].', '.$registro['nombre_municipio'].
        '<br>Tel&eacute;fonos: '.$registro['telefono_casa'].', '.$registro['telefono_cel'].
        '<br>Correo electr&oacute;nico: '.$registro['correo'].
        '<br>Instituci&oacute;n: '.$registro['nombre_institucion'].
        '<br>Facultad, Instituto o Escuela: '.$registro['facultad'].
        '<br>Carrera: '.$registro['carrera'].
        '<br>Estatus: '.$estatus.
        '<br><br>Informaci&oacute;n del trabajo:'.
        '<br>T&iacute;tulo: '.$registro['titulo'].
        '<br>&Aacute;rea de conocimiento: '.$registro['nombre_area'].
        '<br>Modalidad: '.$registro['nombre_modalidad'].
        '<br>Colaboradores: '.$registro['colaborador_1'].' '.$registro['colaborador_2'].' '.$registro['colaborador_3'].
        '<br>Informaci&oacute;n del Asesor:'.
        '<br>Profesor Asesor: '.$registro['grado_academico'].' '.$registro['nombre_asesor'].' '.$registro['apellidos_asesor'].
        '<br>Instituci&oacute;n: '.$registro2['nombre_institucion'].
        '<br>Facultad, Instituto o Escuela: '.$registro['facultad_asesor'].
        '<br>Correo electr&oacute;nico: '.$registro['correo_asesor'].
        '<br><br>A continuaci&oacute;n puede ver en los siguientes enlaces los archivos que ha enviado:'.
        '<br><br>Solicitud de Participaci&oacute;n: <a href="http://jovenesinvestigadores.uach.mx/'.$registro['ubic_solicitud'].'">Descargar</a>'.
        '<br>Trabajo: <a href="http://jovenesinvestigadores.uach.mx/'.$registro['ubic_trabajo'].'">Descargar</a>'.
        '<br>Credencial de Elector: <a href="http://jovenesinvestigadores.uach.mx/'.$registro['ubic_credencial'].'">Descargar</a>'.
        '<br>CURP: <a href="http://jovenesinvestigadores.uach.mx/'.$registro['ubic_curp'].'">Descargar</a>'.
        '<br><br>Folio de Inscripci&oacute;n: <b style="color:#5E9666">'.$registro['folio'].'</b><br>'.
        '<br><br>Si usted nota que alguna parte de esta informaci&oacute;n esta incorrecta, favor de contactar a la coordinaci&oacute;n.<br>
                <br>Atte.<br> Jonathan Islas, Administrador de la Plataforma.<br><br>';
    if(send_mail($registro['correo'],
        $registro['nombre'].' '.$registro['apellido_pat'],
        'Confirmación de Registro',
        $cuerpo)){
        $echo = true;

    } else {
        $echo = false;
    }
    return $echo;
}

function enviar_mail_evaluador($correo, $conexion, $pass){
    /** @var mysqli $conexion */
    $query = "select * from usuarios
                LEFT JOIN evaluadores on usuarios.correo = evaluadores.correo
                left join instituciones on evaluadores.id_institucion = instituciones.id_institucion
                left join area_conocimiento on evaluadores.id_area = area_conocimiento.id_area
                left join municipios on evaluadores.id_municipio = municipios.id_municipio
                where usuarios.correo='$correo';";
    $resultado = $conexion->query($query);
    $registro = $resultado->fetch_assoc();
    $claveCA = ($registro['clave_ca'] == null)?'No cuenta con clave':'UACHIH-CA-'.$registro['clave_ca'];
    $cuerpo = '<br>Esta es la informaci&oacute;n que usted tiene registrada dentro de la plataforma:<br>
                <br>Informaci&oacute;n Personal:
                <br>Nombre: '.$registro['abreviatura_titulo'].' '.$registro['nombre'].' '.$registro['apellido_pat'].' '.$registro['apellido_mat'].
        '<br>Localidad: '.$registro['ciudad'].', '.$registro['nombre_municipio'].
        '<br>Tel&eacute;fonos: '.$registro['telefono_casa'].', '.$registro['telefono_cel'].
        '<br>Correo electr&oacute;nico: '.$registro['correo'].
        '<br>Titulo Academico: '.$registro['titulo'].
        '<br>Instituci&oacute;n: '.$registro['nombre_institucion'].
        '<br>Facultad, Instituto o Escuela: '.$registro['facultad'].
        '<br>&Aacute;rea de conocimiento: '.$registro['nombre_area'].
        '<br><br>Distinciones como investigador:'.
        '<br>Miembro del S.N.I: '.$registro['es_sni'].
        '<br>Nivel: '.$registro['nivel_sni'].
        '<br>Perfil PROMEP: '.$registro['es_promep'].
        '<br>Miembro de alg&uacute;n cuerpo academico de la UACH: '.$registro['cuerpo_academico'].
        '<br>Clave: '.$claveCA.
        '<br><br>Contrase&ntilde;a: '.$pass.
        '<br><br>A partir del d&iacute;a Jueves 14 de mayo del 2015, la plataforma estar&aacute; abierta para que proceda a calificar los trabajos.
                Utilice el correo donde recibe esta notificaci&oacute;n como usuario y la contrase&ntilde;a aqu&iacute; escrita para poder iniciar sesi&oacute;n
                en el siguiente enlace: <a href="http://jovenesinvestigadores.uach.mx/registro/">Login</a> en la pesta&ntilde;a de evaluadores.'.
        '<br><br>Si usted nota que alguna parte de esta informaci&oacute;n esta incorrecta, incompleta as&iacute;
                como un cambio reciente en alg&uacute;n dato, contacte a la coordinaci&oacute;n, y se le informar&aacute; de que puede proceder.<br>
                <br>Atte.<br> Jonathan Islas<br>Administrador de la Plataforma.<br><br>';
    if(send_mail($registro['correo'],
        $registro['abreviatura_titulo'].' '.$registro['nombre'].' '.$registro['apellido_pat'],
        'Confirmación de Registro como Evaluador',
        $cuerpo)){
        $echo = true;

    } else {
        $echo = false;
    }
    return $echo;
}


function enviar_mail_nuevo_usuario($correo, $pass, $conexion){
    /** @var mysqli $conexion */
    $query = "select concat_ws(' ', nombre, apellido_pat) as name from usuarios where correo = '$correo';";
    $resultado = $conexion->query($query);
    $registro = $resultado->fetch_assoc();
    $cuerpo = "Se ha a&ntilde;adido un usuario para usted como parte del personal de coordinaci&oacute;n.<br>
                A continuaci&oacute;n se le proporciona la contrase&ntilde;a para su acceso:<br><br>
                Contrase&ntilde;a: $pass
                <br><br>Agradecemos de antemano su atención y apoyo.
                <br>Reciba un cordial saludo.
                <br>Atte.
                <br>Dirección de Investigación y Posgrado
                <br>Universidad Autónoma de Chihuahua.
                <br>(614) 439-18-22
                <br>Ext. 2206, 2207, 2213 y 2218.";
    if(send_mail($correo, $registro['name'], 'Confirmación de Registro', $cuerpo)){
        $echo = true;
    } else {
        $echo = false;
    }
    return $echo;
}

function trabajoAsignado($id_evaluador, $id_trabajo, $id_participante, $conexion){
    /** @var mysqli $conexion */
    $query = "select
                concat_ws(' ', abreviatura_titulo, nombre, apellido_pat) as nombre,
                evaluadores.correo
              from evaluadores
              LEFT JOIN usuarios ON usuarios.correo = evaluadores.correo
              where id_evaluador = $id_evaluador;" ;
    $query2 = "select * from trabajos
                left join participantes on trabajos.id_participante = participantes.id_participante
                LEFT JOIN usuarios ON usuarios.correo = participantes.correo
                left join area_conocimiento on trabajos.id_area = area_conocimiento.id_area
                left join modalidad on trabajos.id_modalidad = modalidad.id_modalidad
                where trabajos.id_trabajo = $id_trabajo;" ;
    $resultado = $conexion->query($query);$resultado2 = $conexion->query($query2);
    $registro = $resultado->fetch_assoc();$registro2 = $resultado2->fetch_assoc();
    $cuerpo = '<br>Estimado(a) '.$registro['nombre'].',
                usted ha sido elegido para revisar el siguiente Trabajo de Investigación:
                <br><br>&emsp; &emsp;Titulo: '.$registro2['titulo'].'
                <br>&emsp; &emsp;Modalidad de Participación: '.$registro2['nombre_modalidad'].'
                <br>&emsp; &emsp;Área: '.$registro2['nombre_area'].'
                <br>&emsp; &emsp;Autor: '.$registro2['nombre'].' '.$registro2['apellido_pat'].' '.$registro2['apellido_mat'].'
                <br><br>Favor de realizar la revisión, iniciando sesión con su cuenta de correo electrónico y clave
                previamente enviada por el mismo medio, al momento de la notificación de su registro como Evaluador
                de propuestas de Investigación en el <b>3° Encuentro de Jóvenes Investigadores de IES del Estado de Chihuahua</b>.
                <br>Le solicitamos sea tan amable en elaborar la revisión, llenando el formulario de criterios de evaluación
                que encontrará en la plataforma, a más tardar el día <b>24 de mayo del 2015</b>, para cumplir con los tiempos
                acordados en la Convocatoria 2015.
                <br>Agradecemos de antemano su atención y apoyo.
                <br>Reciba un cordial saludo.
                <br>Atte.
                <br>Dirección de Investigación y Posgrado
                <br>Universidad Autónoma de Chihuahua.
                <br>(614) 439-18-22
                <br>Ext. 2206, 2207, 2213 y 2218."';
    if(send_mail($registro['correo'], $registro['nombre'], 'Asignación de trabajo para su evaluación', $cuerpo)){
        $echo = true;
    } else {
        $echo = false;
    }
    return $echo;
}

?>
