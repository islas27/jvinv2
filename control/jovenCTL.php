<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 4/6/2015
     * Time: 10:01 PM
     */
    if(isset($_POST['nombre']) || isset($_GET['id'])){
        include("../resources/connection.php");
        if (version_compare(PHP_VERSION, '5.5.0', '<')) {
            include("../resources/password_compat_lib.php");
        }
        $conn = my_connection();
        (isset($_GET['id']))? $option = $_GET['cmdAction'] : $option = $_POST['cmdAction'];
        switch($option){
            case 0://agregar datos
                $nombre = $_POST['nombre'];
                $apellido_pat = $_POST['apellido_pat'];
                $apellido_mat = $_POST['apellido_mat'];
                $calle = $_POST['calle'];
                $num_ext = $_POST['numero_ext'];
                $num_int = $_POST['numero_int'];
                $colonia = $_POST['colonia'];
                $codigo_postal = $_POST['codigo_postal'];
                $localidad = $_POST['localidad'];
                $id_municipio = $_POST['municipio'];
                $telefono_casa = $_POST['telefono_casa'];
                $telefono_cel = $_POST['telefono_cel'];
                $correo = $_POST['correo'];
                $pass = password_hash($_POST['pass1'], PASSWORD_BCRYPT);
                $es_estudiante = $_POST['situacion'];
                $semestre = $_POST['semestre'];
                $tiempo_egresado = $_POST['meses'];
                $id_institucion = $_POST['institucion'];
                $facultad = $_POST['facultad'];
                $carrera = $_POST['carrera'];
                $status = "ON";
                $tipo_usuario = 5;
                $query = "select * from usuarios where correo='$correo';";
                $resultado = $conn->query($query);
                if(!$registro = $resultado->fetch_assoc()){
                    try {
                        /* switch autocommit status to FALSE. Actually, it starts transaction */
                        $conn->autocommit(FALSE);

                        $sql = "insert into usuarios
                              (correo,password, status, fecha_alta, tipo_usuario, nombre, apellido_pat, apellido_mat) VALUES
                              (?, ?, ?, now(), ?, ?, ?, ?);";
                        $stmt = $conn->prepare($sql);
                        if($stmt === false) {
                            throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $conn->error);
                        }
                        /* Bind parameters. Types: s = string, i = integer, d = double,  b = blob */
                        $stmt->bind_param('sssisss',$correo, $pass, $status, $tipo_usuario, $nombre, $apellido_pat, $apellido_mat);
                        /* Execute statement */
                        $stmt->execute();
                        if($stmt->errno > 0) {
                            throw new Exception('Wrong SQL execute 1, Error: ' . $stmt->error);
                        }

                        $stmt->close();

                        $sql = "insert into participantes
                              (correo, calle, num_ext, num_int, colonia, codigo_postal, id_municipio, localidad,
                                  telefono_casa, telefono_cel, id_institucion, facultad, carrera, es_estudiante,
                                  semestre, tiempo_egresado) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                        $stmt = $conn->prepare($sql);
                        if($stmt === false) {
                            throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $conn->error);
                        }
                        $stmt->bind_param('ssssssisssisssii',$correo, $calle, $num_ext, $num_int, $colonia, $codigo_postal, $id_municipio, $localidad,
                            $telefono_casa, $telefono_cel, $id_institucion, $facultad, $carrera, $es_estudiante,
                            $semestre, $tiempo_egresado);
                        $stmt->execute();
                        if($stmt->errno > 0) {
                            throw new Exception('Wrong SQL execute 2, Error: ' . $stmt->error);
                        }
                        $stmt->close();

                        $conn->commit();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                        error_log("Transaction failed: $error\n On Controller jovenCTL.php \n", 3, "error_log.txt");
                        $conn->rollback();
                        $conn->autocommit(TRUE);
                        header("Location: ../registro_participante.php?error=4");//error desconocido con la query o intento de injeccion....
                        exit();
                    }
                    $conn->autocommit(TRUE);
                    header("Location: ../login.php?success=1");//agregado con exito
                    exit();
                }else{
                    header("Location: ../registro_participante.php?error=6");//usuario repetido...
                    exit();
                }
                break;
            case 1:
                include("send_mail.php");
                if(enviar_mail_confirmacion($_GET['id'],$conn)){
                    header("Location: ../reporte_participantes.php?success=8");
                    exit();
                }else{
                    header("Location: ../reporte_participantes.php?success=8");
                    exit();
                }
                break;
        }
    }else{
        header("Location: ../registro_participante.php?error=2"); //Se entro sin informacion relevante...
        exit();
    }