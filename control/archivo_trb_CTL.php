<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/19/2015
 * Time: 10:39 PM
 */

if (empty($_FILES['trabajo'])) {
    echo json_encode(['error'=>'No files found for upload.']);
    return;
}

session_start();
require("../resources/functions.php");
require("../resources/server_vars.php");

$id_participante = $_SESSION['id_participante'];
$nvo_trabajo = "archive/$year/trabajo/trabajo_".$id_participante."_".random_string(6).".pdf";
$trabajo = $_FILES['trabajo'];

if(rename($trabajo['tmp_name'],$_SERVER['DOCUMENT_ROOT'].$folder.$nvo_trabajo) &&
    guardarRuta($id_participante, $nvo_trabajo, 'trabajo')){
    $output = ['uploaded' => $nvo_trabajo];
}else{
    $path = $_SERVER['DOCUMENT_ROOT'].$folder.$nvo_trabajo;
    error_log("Transaction failed: $path is the path to save", 3, "errorlog.txt");
    $output = ['error'=>'Error while uploading images. Contact the system administrator'];
    unlink($_SERVER['DOCUMENT_ROOT'].$folder.$nvo_trabajo);
}

echo json_encode($output);
