<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/20/2015
 * Time: 4:12 AM
 */

function borrar_archivos($id_p){
    require("../resources/server_vars.php");
    /** @var string $folder */
    unlink($_SERVER['DOCUMENT_ROOT'].$folder."archive/credencial/credencial_".$id_p."_*");
    unlink($_SERVER['DOCUMENT_ROOT'].$folder."archive/curp/curp_".$id_p."_*");
    unlink($_SERVER['DOCUMENT_ROOT'].$folder."archive/solicitud/solicitud_".$id_p."_*");
    unlink($_SERVER['DOCUMENT_ROOT'].$folder."archive/trabajo/trabajo_".$id_p."_*");
}

require("../resources/connection.php");
require("send_mail.php");

session_start();
$id_participante = $_SESSION['id_participante'];
$conn = my_connection();
$sql = "select * from trabajos where id_participante = $id_participante";
$resultado = $conn->query($sql);
$registro = $resultado->fetch_assoc();
if((is_null($registro['ubic_curp']))
        ||(is_null($registro['ubic_credencial']))
        ||(is_null($registro['ubic_trabajo']))
        ||(is_null($registro['ubic_solicitud']))){
                borrar_archivos($id_participante);
                $sql = "update trabajos set ubic_credencial = null, ubic_curp = null, ubic_solicitud = null, ubic_trabajo = null
                        where id_participante = $id_participante";
                $conn->query($sql);
                $_SESSION['trabajo_subido'] = false;
                json_encode(["error" => "Error desconocido..."]);
}else{
    $sql = "update trabajos set fecha_subida = now() where id_participante = $id_participante";
    $conn->query($sql);
    $sql = "select * from participantes LEFT OUTER JOIN trabajos ON
            participantes.id_participante = trabajos.id_participante
            WHERE participantes.id_participante = $id_participante";
    $resultado = $conn->query($sql);
    $registro = $resultado->fetch_assoc();
    require("../resources/functions.php");
    $folio = sprintf("%04d", $id_participante).'-'.claveTipo($registro['id_modalidad']).'-'.claveArea($registro['id_area']);
    $sql = "update participantes set folio = '$folio' where id_participante = $id_participante";
    $conn->query($sql);
    $_SESSION['trabajo_subido'] = true;
    enviar_mail_confirmacion($_SESSION['id_usuario'],$conn);
    json_encode(["success" => "Everything A-OK"]);
}