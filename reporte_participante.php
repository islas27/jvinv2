<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/22/2015
 * Time: 12:57 AM
 */
    session_start();
    require_once("resources/session_validation.php");
    validate_group();
    require("resources/connection.php");
    if(!isset($_GET['id'])){
        header("Location: reporte_participantes.php");
    }
    $id_usuario = $_GET['id'];
    $conn = my_connection();
    $query = "select * from usuarios
            LEFT JOIN participantes ON usuarios.correo = participantes.correo
            LEFT JOIN trabajos ON participantes.id_participante = trabajos.id_participante
            LEFT JOIN instituciones on instituciones.id_institucion = participantes.id_institucion
            LEFT JOIN municipios ON municipios.id_municipio = participantes.id_municipio
            where id_usuario = $id_usuario";
    /*$query2 = "select * from trabajos
        LEFT JOIN area_conocimiento ON trabajos.id_area = area_conocimiento.id_area
        LEFT JOIN modalidad ON trabajos.id_modalidad = modalidad.id_modalidad
        LEFT JOIN instituciones ON id_institucion = trabajos.id_institucion_asesor
        where id_trabajo = ". $registro['id_trabajo']." ;";*/
    $resultado = $conn->query($query);
    $registro = $resultado->fetch_assoc();
    $asignacion = !(is_null($registro['eval_previa']));
    $eval_previa = $registro['eval_previa'];
    $veredicto = $registro['veredicto'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">

    <!-- Multiselect-->
    <link href="css/bootstrap-multiselect.css" rel="stylesheet">
    <style>.multiselect-container input {display: none}</style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include("resources/navbar.php");?>

<div>
    <h2 style="text-align: center; margin-bottom: 24px">Reporte Individual</h2>
</div>
<?php
    if(isset($_GET['error'])){
        include('resources/msg_loading.php');
        error_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
    }
    if((isset($_GET['success']))){
        include('resources/msg_loading.php');
        success_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1",$_GET['success']);
    }
?>
<div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
    <p>Aquí puede ver toda la información relevante del participante, asignar evaluadores,
        ver calificaciones y validar la documentación.</p>

    <div class="row" style="padding-left: 15px; padding-right: 15px;">
        <?php
            if(!$asignacion && $_SESSION['tipo_usuario'] == 1){
                include('resources/forms/asignar_evaluadores.php');
            }
        ?>
    </div>
    <div>
    <?php
        $estatus = ($registro['es_estudiante'] == 1)?'Estudiante con '.$registro['semestre'].' semestres'
            :'Egresado con '.$registro['tiempo_egresado'].' meses de haber egresado';
        $folio = $registro['folio'];
        $id_participante = $registro['id_participante'];
        $cuerpo = '<br>Informaci&oacute;n Personal:'.
            '<br>Nombre: '.$registro['nombre']." ".$registro['apellido_pat']." ".$registro['apellido_mat'].
            '<br>Domicilio: '.$registro['calle'].' #'.$registro['num_ext'].', '.$registro['colonia'].', CP '.$registro['codigo_postal'].
            '<br>Localidad: '.$registro['localidad'].', '.$registro['nombre_municipio'].
            '<br>Teléfonos: '.$registro['telefono_casa'].', '.$registro['telefono_cel'].
            '<br>Correo electrónico: '.$registro['correo'].
            '<br>Institución: '.$registro['nombre_institucion'].
            '<br>Facultad, Instituto o Escuela: '.$registro['facultad'].
            '<br>Carrera: '.$registro['carrera'].
            '<br>Estatus: '.$estatus.'<br>';
        $query = 'select * from trabajos
                            LEFT JOIN instituciones ON instituciones.id_institucion = trabajos.id_institucion_asesor
                            LEFT JOIN area_conocimiento ON area_conocimiento.id_area = trabajos.id_area
                            LEFT JOIN modalidad ON modalidad.id_modalidad = trabajos.id_modalidad
                            where id_participante = '.$registro["id_participante"].';';
        $resultado = $conn->query($query);
        $registro = $resultado->fetch_assoc();
        $modalidad = $registro['id_modalidad'];
        $cuerpo .= '<br>Información del trabajo:'.
            '<br>Título: <i>"'.$registro['titulo'].'"</i>'.
            '<br>Área de conocimiento: '.$registro['nombre_area'].
            '<br>Modalidad: '.$registro['nombre_modalidad'].
            '<br>Colaboradores: '.(($registro['colaborador_1'] != '')? $registro['colaborador_1'] : '').
            (($registro['colaborador_2'] != '')? ', '.$registro['colaborador_2'] : '').
            (($registro['colaborador_3'] != '')? ', '.$registro['colaborador_3'] : '').
            '<br><br>Información del Asesor:'.
            '<br>Profesor Asesor: '.$registro['grado_academico'].' '.$registro['nombre_asesor'].' '.$registro['apellidos_asesor'].
            '<br>Institución: '.$registro['nombre_institucion'].
            '<br>Facultad, Instituto o Escuela: '.$registro['facultad_asesor'].
            '<br>Correo electrónico: '.$registro['correo_asesor'].'<br>';
        $cuerpo .= '<br>A continuaci&oacuten puede ver en los siguientes enlaces los archivos que ha enviado:'.
            '<br><br>Solicitud de Participación: <a href="http://jovenesinvestigadores.uach.mx/'.$registro['ubic_solicitud'].'">Descargar</a>'.
            '<br>Trabajo: <a href="http://jovenesinvestigadores.uach.mx/'.$registro['ubic_trabajo'].'">Descargar</a>'.
            '<br>Credencial de Elector: <a href="http://jovenesinvestigadores.uach.mx/'.$registro['ubic_credencial'].'">Descargar</a>'.
            '<br>CURP: <a href="http://jovenesinvestigadores.uach.mx/'.$registro['ubic_curp'].'">Descargar</a>'.
            '<br><br>Folio de Inscripción: <b style="color:#D45F4A">'.$folio.'</b><br>';
        echo $cuerpo;
    ?>
    </div>
    <div>
        <?php
            if($eval_previa == 'Verificado'){
                echo "<h3  style='padding-left: 15px;'>Evaluadores y Calificaciones</h3>";
                $id_trabajo = $registro['id_trabajo'];
                $sql = "select concat_ws(' ',abreviatura_titulo, nombre, apellido_pat, apellido_mat) as nombre, evaluaciones.* from evaluaciones
                        LEFT JOIN evaluadores ON evaluadores.id_evaluador = evaluaciones.id_evaluador
                        LEFT JOIN usuarios ON usuarios.correo = evaluadores.correo where id_trabajo = $id_trabajo";
                $resultado = $conn->query($sql);
                $eval1 = $resultado->fetch_array(MYSQLI_BOTH);
                $eval2 = $resultado->fetch_array(MYSQLI_BOTH);
                $eval3 = $resultado->fetch_array(MYSQLI_BOTH);
                $sql = "select criterio from criterios_evaluacion where id_modalidad = $modalidad";
                $resultado = $conn->query($sql);
                echo "Evaluador #1: ".$eval1['nombre'].
                    "<br>Evaluador #2: ".$eval2['nombre'].
                    "<br>Evaluador #3: ".$eval3['nombre'].
                    "<br><br>A continuación observara una tabla con las calificaciones obtenidas de acuerdo a
                                    cada evaluador y por criterio de evaluación:";
                echo "<table class='table table-bordered table-responsive table-hover'>
                        <thead>
                            <tr>
                                <th>Criterio de evaluacion</th>
                                <th>Ev. #1</th>
                                <th>Ev. #2</th>
                                <th>Ev. #3</th>
                            </tr>
                        </thead>
                        <tbody>";
                $x = 5;
                while($registro = $resultado->fetch_array(MYSQLI_NUM)){
                    echo "<tr>
                                <td>$registro[0]</td>
                                <td>$eval1[$x]</td>
                                <td>$eval2[$x]</td>
                                <td>$eval3[$x]</td>
                            </tr>";
                    $x++;
                }
                echo "<tr class='success'>
                        <td>Calificación Total</td>
                        <td>$eval1[$x]</td>
                        <td>$eval2[$x]</td>
                        <td>$eval3[$x]</td>
                    </tr>";

                echo        "</tbody>
                    </table>";
                echo "Comentario #1: ". $eval1['comentario'];
                echo "<br>Comentario #2: ". $eval2['comentario'];
                echo "<br>Comentario #3: ". $eval3['comentario'];
                if($eval1[$x] != null && $eval2[$x] != null && $eval3[$x] != null && $veredicto == null){
                    include('resources/forms/veredicto_final.php');
                }elseif(empty($veredicto)){
                    echo "<h3>Veredicto: Sin valorar aún</h3>";
                }else{
                    echo "<h3>Veredicto: $veredicto</h3>";
                }
            }
        ?>
    </div>
</div>

<?php include("resources/footer.php");?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- FormValidation plugin-->
<script src="js/formValidation/formValidation.min.js"></script>
<script src="js/formValidation/bootstrap.min.js"></script>
<!-- Multiselect plugin-->
<script src="js/bootstrap-multiselect.js"></script>
<script type="text/javascript">
    $(window).load(function(){
        $('#evaluadores').multiselect('disable')
    });
    $('#previa').click(function(){
        if($("#previa").val() == 'Si'){
            $('#evaluadores').multiselect('enable')
        }else{
            $('#evaluadores').multiselect('disable')
        }
    });
    $(document).ready(function() {
        $('#multiselectForm')
            .find('[name="evaluadores"]')
            .multiselect({
                // Re-validate the multiselect field when it is changed
                onChange: function(element, checked) {
                    $('#multiselectForm').formValidation('revalidateField', 'evaluadores');
                },
                buttonClass: 'form-control input-sm',
                buttonWidth: '100%',
                nonSelectedText: 'Seleccione',
                checkboxName: 'evaluadores[]'
            })
            .end()
            .formValidation({
                framework: 'bootstrap',
                // Exclude only disabled fields
                // The invisible fields set by Bootstrap Multiselect must be validated
                excluded: ':disabled',
                fields: {
                    previa: {
                        validators: {
                            notEmpty: {
                                message: 'Debe de seleccionar un valor'
                            }
                        }
                    },
                    evaluadores: {
                        validators: {
                            callback: {
                                message: 'Seleccione 3 evaluadores',
                                callback: function(value, validator, $field) {
                                    // Get the selected options
                                    var options = validator.getFieldElements('evaluadores').val();
                                    return (options != null && options.length == 3);
                                }
                            }
                        }
                    }
                }
            });
    });
    $(document).ready(function(){
        $('#FRMVeredicto').formValidation({
            framework: 'bootstrap',
            fields:{
                veredicto:{
                    validators:{
                        notEmpty:{
                            message: 'Seleccione una opción'
                        }
                    }
                }
            }
        });
    });
</script>
</body>
</html>