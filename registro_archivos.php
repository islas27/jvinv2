<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/19/2015
 * Time: 12:00 AM
 */
session_start();
require_once("resources/session_validation.php");
validate_session();
validate_usertype(5);
include("resources/server_vars.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">
    <link href="css/bootstrap-custom-archivos.css" rel="stylesheet">
    <!--Bootstrap File Input CSS file-->
    <link href="css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!-- Bootstrap File Input Scripts-->
    <script src="js/fileinput.min.js" type="text/javascript"></script>
    <script src="js/fileinput_locale_es.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include("resources/navbar.php");?>
<?php include("resources/carrusel.php");?>
<div class="col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1">
    <h2 style="text-align: center; margin-bottom: 24px">Adjuntar y enviar archivos (Etapa 3/3)</h2>
</div>
<?php
if(isset($_GET['error'])){
    include('resources/msg_loading.php');
    error_load("col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
}
if(isset($_GET['success'])){
    include('resources/msg_loading.php');
    success_load("col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1", $_GET['success']);
}
?>
<div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
    <p>Para concluir su registro adjunte los archivos que se solicitan a continuación
        (únicamente formato .pdf y un peso máximo de 2MB por cada archivo).
        Le recordamos que debe de dar clic en el botón cargar archivo para empezar el proceso.</p>
    <div id="alerta"></div>
    <form class="form-horizontal" method="post">
        <legend><span class="glyphicon glyphicon-cloud-upload"></span>&nbsp; &nbsp;Archivos</legend>

        <div class="form-group" id="solicitud-grp">
            <label for="solicitud" class="col-md-3">Solicitud de Participación</label>
            <input type="file" id="solicitud" name="solicitud">
            <div id="error_solicitud" class="contenedor"></div>
        </div>
        <div class="progress contenedor" id="progreso_solicitud">
            <div class="progress-bar progress-bar-success" style="width: 100%">
                Solicitud de Participación: <span class="glyphicon glyphicon-ok-circle"></span>
            </div>
        </div>

        <div class="form-group" id="trabajo-grp">
            <label for="trabajo" class="col-md-3">Trabajo de investigación</label>
            <input type="file" id="trabajo" name="trabajo">
            <div id="error_trabajo" class="contenedor"></div>
        </div>
        <div class="progress contenedor" id="progreso_trabajo">
            <div class="progress-bar progress-bar-success" style="width: 100%">
                Trabajo de investigación: <span class="glyphicon glyphicon-ok-circle"></span>
            </div>
        </div>

        <div class="form-group" id="credencial-grp">
            <label for="credencial" class="col-md-3">Credencial de Elector</label>
            <input type="file" id="credencial" name="credencial">
            <div id="error_credencial" class="contenedor"></div>
        </div>
        <div class="progress contenedor" id="progreso_credencial">
            <div class="progress-bar progress-bar-success" style="width: 100%">
                Credencial de Elector: <span class="glyphicon glyphicon-ok-circle"></span>
            </div>
        </div>


        <div class="form-group" id="curp-grp">
            <label for="curp" class="col-md-3">CURP</label>
            <input type="file" id="curp" name="curp">
            <div id="error_curp" class="contenedor"></div>
        </div>
        <div class="progress contenedor" id="progreso_curp">
            <div class="progress-bar progress-bar-success" style="width: 100%">
                CURP: <span class="glyphicon glyphicon-ok-circle"></span>
            </div>
        </div>

        <div class="form-group">

        </div>
    </form>
    <a class="btn btn-primary col-md-offset-4 col-md-4 col-xs-offset-3 col-xs-6"
            style="margin-bottom: 1em;" id="salida" href="reporte_personal.php?success=1">Terminar</a>
</div>



<?php include("resources/footer.php");?>
<script>
    var archivos_subidos = 0;
    $(document).ready(function() {
        $("#progreso_solicitud").hide();
        $("#progreso_trabajo").hide();
        $("#progreso_credencial").hide();
        $("#progreso_curp").hide();
        $("#solicitud").fileinput({
            showPreview: false,
            showCancel: false,
            removeLabel: '',
            uploadLabel: '',
            uploadIcon: '<i class="glyphicon glyphicon-floppy-open"></i>',
            browseLabel: 'Buscar',
            elErrorContainer: '#error_solicitud',
            maxFileCount: 1,
            minFileCount: 1,
            maxFileSize: 2048,
            allowedFileExtensions: ['pdf'],
            uploadUrl: "control/archivo_sol_CTL.php" // your upload server url
        }).on('filebatchuploadcomplete', function(){
            $("#solicitud").fileinput('disable');
            $("#solicitud-grp").hide();
            $("#progreso_solicitud").show();
            archivos_subidos++;
        });
        $("#trabajo").fileinput({
            showPreview: false,
            showCancel: false,
            removeLabel: '',
            uploadLabel: '',
            uploadIcon: '<i class="glyphicon glyphicon-floppy-open"></i>',
            browseLabel: 'Buscar',
            elErrorContainer: '#error_trabajo',
            maxFileCount: 1,
            minFileCount: 1,
            maxFileSize: 2048,
            allowedFileExtensions: ['pdf'],
            uploadUrl: "control/archivo_trb_CTL.php" // your upload server url
        }).on('filebatchuploadcomplete', function(){
            $("#trabajo").fileinput('disable');
            $("#trabajo-grp").hide();
            $("#progreso_trabajo").show();
            archivos_subidos++;
        });
        $("#credencial").fileinput({
            showPreview: false,
            showCancel: false,
            removeLabel: '',
            uploadLabel: '',
            uploadIcon: '<i class="glyphicon glyphicon-floppy-open"></i>',
            browseLabel: 'Buscar',
            elErrorContainer: '#error_credencial',
            maxFileCount: 1,
            minFileCount: 1,
            maxFileSize: 2048,
            allowedFileExtensions: ['pdf'],
            uploadUrl: "control/archivo_cre_CTL.php" // your upload server url
        }).on('filebatchuploadcomplete', function(){
            $("#credencial").fileinput('disable');
            $("#credencial-grp").hide();
            $("#progreso_credencial").show();
            archivos_subidos++;
        });
        $("#curp").fileinput({
            showPreview: false,
            showCancel: false,
            removeLabel: '',
            uploadLabel: '',
            uploadIcon: '<i class="glyphicon glyphicon-floppy-open"></i>',
            browseLabel: 'Buscar',
            elErrorContainer: '#error_curp',
            maxFileCount: 1,
            minFileCount: 1,
            maxFileSize: 2048,
            allowedFileExtensions: ['pdf'],
            uploadUrl: "control/archivo_crp_CTL.php" // your upload server url
        }).on('filebatchuploadcomplete', function(){
            $("#curp").fileinput('disable');
            $("#curp-grp").hide();
            $("#progreso_curp").show();
            archivos_subidos++;
        });
    });
    $("#salida").click(function(e){
        if(archivos_subidos === 4){
            $.ajax({url: "control/archivoCTL.php"});
        } else{
            e.preventDefault();
            $('#alerta').html('<div class="alert alert-warning fade in">'
            +'<button type="button" class="close close-alert" data-dismiss="alert" aria-hidden="true">×</button>' +
            '¡No has terminado de subir los archivos! Continua por favor...</div>');
        }
    });

</script>

</body>
</html>