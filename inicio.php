<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/18/2015
 * Time: 9:28 PM
 */
session_start();
require_once("resources/session_validation.php");
validate_session();

$panelHeader = "¡Bienvenido(a) ".$_SESSION['name'].'!';
switch($_SESSION['tipo_usuario']){
    case 1:
        $_SESSION['coordinacion'] = true;
        $panelContent = 'Desplácese por el menú superior para la operación requerida.';
        break;
    case 2:
        $_SESSION['coordinacion'] = true;
        $panelContent = 'Desplácese por el menú superior para la operación requerida.';
        break;
    case 3:
        $_SESSION['coordinacion'] = true;
        $panelContent = 'Desplácese por el menú superior para la operación requerida.';
        break;
    case 4:
        get_evalid();
        $panelHeader = "¡Bienvenido(a) ".$_SESSION['saludo']." ".$_SESSION['name'].'!';
        $panelContent = 'En la barra de menú superior, podrá encontrar las operaciones necesarias
                    para poder efectuar su trabajo, así como correcciones que deba de hacer.';
        break;
    case 5:
        verify_fileupload();
        $panelContent = 'En la barra de menú superior, podrá encontrar los pasos necesarios
                    para completar su registro,
                    así como consultar la información guardada previamente.';
        break;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <?php
        include("resources/navbar.php");
        include("resources/carrusel.php");
    ?>
    <div class="col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1">
        <h2 style="text-align: center; margin-bottom: 24px">Bienvenido</h2>
    </div>
    <div class="panel panel-success col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1" style="margin-top: 24px;">
        <div class="panel-heading">
            <h3 class="panel-title" style="text-align: center;"><?php echo $panelHeader?></h3>
        </div>
        <div class="panel-body"><?php echo $panelContent;?></div>
    </div>

    <?php include("resources/footer.php");?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>