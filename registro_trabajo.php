<?php
    session_start();
    require_once("resources/session_validation.php");
    validate_session();
    validate_usertype(5);
    include("resources/connection.php");
    $conexion = my_connection();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include("resources/navbar.php");?>
<?php include("resources/carrusel.php");?>
    <div class="col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1">
        <h2 style="text-align: center; margin-bottom: 24px">Registrar Trabajo (Etapa 2/3)</h2>
    </div>
    <?php
        if(isset($_GET['error'])){
            include('resources/msg_loading.php');
            error_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
        }
        if(isset($_GET['success'])){
            include('resources/msg_loading.php');
            success_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['success']);
        }
    ?>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
        <form name="registro_trabajo" id="registro_trabajo" action="control/trabajoCTL.php" method="post">
        <div class="col-md-6 col-xs-12">
            <br>
            <legend><span class="glyphicon glyphicon-edit"></span>&nbsp; &nbsp;Título</legend>
            <p>Título del trabajo de investigación con el cual va a participar:</p>
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="titulo" name="titulo"/>
            </div>
            <legend><span class="glyphicon glyphicon-file"></span>&nbsp; &nbsp;Información del Trabajo</legend>
            <p>Seleccione el formato de presentación:</p>
            <div class="form-group selectContainer">
                <select class="form-control input-sm" id="modalidad" name="modalidad" required="required">
                    <option value="">Seleccionar</option>
                    <?php
                    $query = 'select id_modalidad, nombre_modalidad from modalidad;';
                    $resultado = $conexion->query($query);
                    $resultado->data_seek(0);
                    while($registro = $resultado->fetch_assoc()){
                        echo '<option value="'.$registro['id_modalidad'].'">'
                            .$registro['nombre_modalidad'].'</option>';
                    }
                    ?>
                </select>
            </div>
            <p>Seleccione el área:</p>
            <div class="form-group selectContainer">
                <select class="form-control input-sm" id="area" name="area" required="required">
                    <option value="">Seleccionar</option>
                    <?php
                    $query = 'select id_area, nombre_area from area_conocimiento;';
                    $resultado = $conexion->query($query);
                    $resultado->data_seek(0);
                    while($registro = $resultado->fetch_assoc()){
                        echo '<option value="'.$registro['id_area'].'">'
                            .$registro['nombre_area'].'</option>';
                    }
                    ?>
                </select>
            </div>
            <legend><span class="glyphicon glyphicon-wrench"></span>&nbsp; &nbsp;Colaboradores</legend>
            <p>Ingrese por favor las personas que han colaborado en el trabajo realizado (<b>Solamente Alumnos o Egresados</b>):</p>
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="asistente[]" name="asistente[]"/>
            </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="asistente[]" name="asistente[]"/>
            </div>
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="asistente[]" name="asistente[]"/>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <br>
            <legend><span class="glyphicon glyphicon-book"></span>&nbsp; &nbsp;Director de Trabajo</legend>
            <p>Ingrese los datos del Profesor que funge como director del trabajo de investigación:</p>
            <div class="form-group">
                <label for="gradoAcademico">Título Académico Actual</label>
                <input type="text" class="form-control input-sm" id="gradoAcademico" name="gradoAcademico"/>
            </div>
            <div class="form-group">
                <label for="nombreAsesor">Nombre</label>
                <input type="text" class="form-control input-sm" id="nombreAsesor" name="nombreAsesor"/>
            </div>
            <div class="form-group">
                <label for="apellidosAsesor">Apellidos</label>
                <input type="text" class="form-control input-sm" id="apellidosAsesor" name="apellidosAsesor"/>
            </div>
            <div class="form-group selectContainer">
                <label for="institucionAsesor">Institución de Educación Superior:</label>
                <select class="form-control input-sm" id="institucionAsesor" name="institucionAsesor">
                    <option value="">Seleccionar</option>
                    <?php
                    $query = 'select id_institucion, nombre_institucion from instituciones;';
                    $resultado = $conexion->query($query);
                    $resultado->data_seek(0);
                    while($registro = $resultado->fetch_assoc()){
                        echo '<option value="'.$registro['id_institucion'].'">'
                            .$registro['nombre_institucion'].'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="facultadAsesor">Facultad, Instituto o Escuela:</label>
                <input type="text" class="form-control input-sm" id="facultadAsesor" name="facultadAsesor"/>
            </div>
            <div class="form-group">
                <label for="correoAsesor">Correo Electrónico</label>
                <input type="email" class="form-control input-sm"
                       id="correoAsesor" name="correoAsesor"/>
            </div>
            <p>De clic en el botón "siguiente", para adjuntar su trabajo de investigación.</p>
            <div class="form-group">
                <button name="cmdAction" type="submit" value="0"  style="margin-bottom: 24px; margin-top: " class="btn btn-primary col-xs-offset-3 col-xs-6">Siguiente</button>
            </div>
        </div>
        </form>
    </div>

    <?php include("resources/footer.php");?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!-- FormValidation plugin and the class supports validating Bootstrap form -->
    <script src="js/formValidation/formValidation.min.js"></script>
    <script src="js/formValidation/bootstrap.min.js"></script>
    <!--FormValidation rules-->
    <script src="js/validationRules/trabajoRules.js"></script>
</body>
</html>