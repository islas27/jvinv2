<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/21/2015
 * Time: 1:17 PM
 */

file_put_contents("hooks.log", date('m/d/Y h:i:s a')." entro el hook \n", FILE_APPEND);
$repo_dir = '/home/web_inv/jv_inv2.git';
$web_root_dir = '/home/jovenesinvestigadores.uach.mx/docs/registro';
// Full path to git binary is required if git is not in your PHP user's path. Otherwise just use 'git'.
$git_bin_path = '/usr/bin/git';

$update = true;

if(isset($_POST['payload'])){
    // Parse data from Bitbucket hook payload
    $payload = json_decode($_POST['payload']);
    //Analizando las peticiones de bitbucket
    file_put_contents('deploy.log', serialize($_POST['payload']), FILE_APPEND);
}

if ($update) {
    // Do a git checkout to the web root
    exec('cd ' . $repo_dir . ' && ' . $git_bin_path  . ' fetch');
    exec('cd ' . $repo_dir . ' && GIT_WORK_TREE=' . $web_root_dir . ' ' . $git_bin_path  . ' checkout -f');

    // Log the deployment
    $commit_hash = shell_exec('cd ' . $repo_dir . ' && ' . $git_bin_path  . ' rev-parse --short HEAD');
    file_put_contents('deploy.log', date('m/d/Y h:i:s a') . " Deployed branch: deployment Commit: " . $commit_hash . "\n", FILE_APPEND);
}
?>