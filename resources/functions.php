<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/19/2015
 * Time: 11:09 PM
 */

function random_string($length) {
    $key = '';
    $keys = range('A', 'Z');
    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }
    return $key;
}

function claveArea($area){
    switch($area){
        case 1:
            return 'CAB';
            break;
        case 2:
            return 'CEH';
            break;
        case 3:
            return 'CS';
            break;
        case 4:
            return 'CSEA';
            break;
        case 5:
            return 'DSMA';
            break;
        case 6:
            return 'IDT';
            break;
    }

    return null;
}

function claveTipo($tipo){
    switch($tipo){
        case 1:
            return 'PO';
            break;
        case 2:
            return 'PC';
            break;
    }

    return null;
}

function guardarRuta($id_participante, $ruta, $archivo){
    require("connection.php");
    $conn = my_connection();
    $archivo = 'ubic_'.$archivo;
    $sql = "update trabajos set $archivo = '$ruta' where id_participante = $id_participante";
    $conn->query($sql);
    if($conn->affected_rows > 0){
        return true;
    }else{
        return false;
    }
}