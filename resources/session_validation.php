<?php
    function validate_session(){
        if(!isset($_SESSION['autenticacion'])){
            header("Location: index.php");
        }
        if($_SESSION['autenticacion'] != true){
            header("Location: login.php?error=5");
        }
    }

    function validate_usertype($tipo_usuario){
        if($tipo_usuario != $_SESSION['tipo_usuario']){
            header("Location: login.php?error=5");
        }
    }

    function verify_fileupload(){
        if(!isset($_SESSION['trabajo_registrado'])){
            require_once("connection.php");
            $conn = my_connection();
            $correo = $_SESSION['correo'];
            $sql = "select * from participantes where correo = '$correo'";
            $resultado = $conn->query($sql);
            $registro = $resultado->fetch_assoc();
            $_SESSION['id_participante'] = $registro['id_participante'];
            $id_participante = $registro['id_participante'];
            $sql = "select * from trabajos where id_participante = $id_participante";
            $resultado = $conn->query($sql);
            if($registro = $resultado->fetch_assoc()){
                $_SESSION['trabajo_registrado'] = true;
                $_SESSION['trabajo_subido'] = ($registro['ubic_curp'] != null)? true : false;
            }else{
                $_SESSION['trabajo_registrado'] = false;
                $_SESSION['trabajo_subido'] = false;
            }
        }
    }

    function get_evalid(){
        if(!isset($_SESSION['id_evaluador'])){
            require_once("connection.php");
            $conn = my_connection();
            $correo = $_SESSION['correo'];
            $sql = "select * from evaluadores where correo = '$correo'";
            $resultado = $conn->query($sql);
            $registro = $resultado->fetch_assoc();
            $_SESSION['id_evaluador'] = $registro['id_evaluador'];
            $_SESSION['saludo'] = $registro['abreviatura_titulo'];
        }
    }

    function validate_group(){
        if(!isset($_SESSION['coordinacion'])){
            header("Location: inicio.php");
        }
    }
?>