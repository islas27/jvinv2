<ul class="nav navbar-nav">
    <li <?php isActive('inicio')?>><a href="inicio.php">Inicio</a></li>
    <?php
        if($_SESSION['trabajo_registrado'] == false){
            echo '<li '.isActive('registrar_trabajo').'><a href="registro_trabajo.php">Registrar Trabajo</a></li>';
        }
        if(($_SESSION['trabajo_registrado'] == true) and ($_SESSION['trabajo_subido'] == false)){
            echo '<li '.isActive('subida_archivos').'><a href="registro_archivos.php">Subir Archivos</a></li>';
        }
    ?>
    <li <?php isActive('mi_info')?>><a href="reporte_personal.php">Mi información</a></li>
</ul>
<ul class="nav navbar-nav navbar-right">
    <li><a href="login.php?log_off=1">Cerrar Sesión</a></li>
</ul>