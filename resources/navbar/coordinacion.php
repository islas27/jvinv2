<ul class="nav navbar-nav">
    <li><a href="reporte_participantes.php">Participantes</a></li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
           role="button" aria-expanded="false">Evaluadores <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="reporte_evaluadores.php">Reporte de Evaluadores</a></li>
            <li class="divider"></li>
            <li><a href="registro_evaluador.php">Registro de evaluadores</a></li>
        </ul>
    </li>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
           role="button" aria-expanded="false">Herramientas <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="enviar_mail.php">Correo</a></li>
            <li><a href="reporte_coordinacion.php">Administración de usuarios</a></li>
            <li class="divider"></li>
            <li><a href="configuracion.php">Opciones de Mantenimiento</a></li>
        </ul>
    </li>
</ul>
<ul class="nav navbar-nav navbar-right">
    <li><a href="login.php?log_off=1">Cerrar Sesión</a></li>
</ul>