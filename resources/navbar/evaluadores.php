<ul class="nav navbar-nav">
    <li <?php isActive('inicio')?>><a href="inicio.php">Inicio</a></li>
    <li <?php isActive('trabajos_asignados')?>><a href="trabajos_asignados.php">Revisiones Pendientes</a></li>
    <li <?php isActive('reporte_evaluador')?>><a href="reporte_evaluador.php">Mi información</a></li>
</ul>
<ul class="nav navbar-nav navbar-right">
    <li><a href="login.php?log_off=1">Cerrar Sesión</a></li>
</ul>