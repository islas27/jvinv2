<?php
if(isset($_GET['type'])){
    switch($_GET['type']){
        case 1: //Participantes
            include("connection.php");
            $conexion = my_connection();
            $sql = "select
                            folio,
                            nombre,
                            concat_ws(' ', apellido_pat, apellido_mat) as apellidos,
                            concat_ws(' ',calle,'#',num_ext,num_int ,colonia,'CP.',codigo_postal) as domicilio,
                            concat(localidad, ', ', nombre_municipio) as localidad,
                            telefono_casa,
                            telefono_cel,
                            participantes.correo,
                            insti1.nombre_institucion as institucionJoven,
                            facultad,
                            carrera,
                            es_estudiante,
                            semestre,
                            tiempo_egresado,
                            nombre_modalidad as modalidad,
                            nombre_area as area,
                            titulo,
                            grado_academico,
                            nombre_asesor,
                            apellidos_asesor,
                            insti2.nombre_institucion as institucionAsesor,
                            facultad_asesor,
                            correo_asesor
                        from participantes
                    LEFT JOIN usuarios ON usuarios.correo = participantes.correo
                    left join trabajos ON participantes.id_participante = trabajos.id_participante
                    left join municipios ON participantes.id_municipio = municipios.id_municipio
                    left join instituciones as insti1 ON participantes.id_institucion = insti1.id_institucion
                    left join modalidad ON trabajos.id_modalidad = modalidad.id_modalidad
                    left join area_conocimiento ON trabajos.id_area = area_conocimiento.id_area
                    left join instituciones as insti2 ON trabajos.id_institucion_asesor = insti2.id_institucion
                    where usuarios.status = 'on'
                    order by participantes.id_participante;";
            $resultado = $conexion->query($sql);
            $registros = $resultado->num_rows;

            if ($registros > 0) {
                require_once 'phpExcel/PHPExcel.php';
                $objPHPExcel = new PHPExcel();
                // set default font
                $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
                // set default font size
                $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
                // writer already created the first sheet for us, let's get it
                $objSheet = $objPHPExcel->getActiveSheet();
                // rename the sheet
                $objSheet->setTitle('Participantes');
                // let's bold and size the header font and write the header
                // as you can see, we can specify a range of cells, like here: cells from A1 to A4
                $objSheet->getStyle('A1:V3')->getFont()->setBold(true)->setSize(13);
                // write headers
                $objSheet->getCell('A1')->setValue('Registo de participantes del 2° Encuentro Estatal de Jóvenes Investigadores');
                $objSheet->mergeCells('A1:V1');
                $objSheet->getCell('A2')->setValue('Datos del Participante');
                $objSheet->mergeCells('A2:M2');
                $objSheet->getCell('N2')->setValue('Datos del Proyecto');
                $objSheet->mergeCells('N2:P2');
                $objSheet->getCell('Q2')->setValue('Datos del Profesor que funge como Director del Trabajo de Investigación');
                $objSheet->mergeCells('Q2:V2');
                $objSheet->getCell('A3')->setValue('Folio');
                $objSheet->getCell('B3')->setValue('Nombre(s)');
                $objSheet->getCell('C3')->setValue('Apellidos');
                $objSheet->getCell('D3')->setValue('Domicilio(Calle, Número, Colonia, CP)');
                $objSheet->getCell('E3')->setValue('Localidad (Cd., Municipio)');
                $objSheet->getCell('F3')->setValue('Télefono de casa');
                $objSheet->getCell('G3')->setValue('Celular');
                $objSheet->getCell('H3')->setValue('Correo');
                $objSheet->getCell('I3')->setValue('Institución de Educación Superior');
                $objSheet->getCell('J3')->setValue('Facultad, Instituto o Escuela');
                $objSheet->getCell('K3')->setValue('Carrera');
                $objSheet->getCell('L3')->setValue('Estatus');
                $objSheet->getCell('M3')->setValue('Tiempo (Semestre o Meses)');
                $objSheet->getCell('N3')->setValue('Modalidad de participación');
                $objSheet->getCell('O3')->setValue('Área de conocimiento');
                $objSheet->getCell('P3')->setValue('Título del proyecto de investigación');
                $objSheet->getCell('Q3')->setValue('Grado Academico');
                $objSheet->getCell('R3')->setValue('Nombre(s)');
                $objSheet->getCell('S3')->setValue('Apellidos');
                $objSheet->getCell('T3')->setValue('Institución de Educación Superior');
                $objSheet->getCell('U3')->setValue('Facultad, Instituto o Escuela');
                $objSheet->getCell('V3')->setValue('correo');

                //Informacion del excel
                $objPHPExcel->
                getProperties()
                    ->setCreator("Webmaster Jonathan Islas")
                    ->setLastModifiedBy("Webmaster Jonathan Islas")
                    ->setTitle("Participantes JvInv 2015")
                    ->setSubject("Participantes")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("participantes")
                    ->setCategory("participantes");

                $i = 4;
                while ($registro =$resultado->fetch_assoc()) {
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i,$registro['folio'])
                        ->setCellValue('B'.$i,$registro['nombre'])
                        ->setCellValue('C'.$i,$registro['apellidos'])
                        ->setCellValue('D'.$i,$registro['domicilio'])
                        ->setCellValue('E'.$i,$registro['localidad'])
                        ->setCellValue('F'.$i,(string)$registro['telefono_casa'])
                        ->setCellValue('G'.$i,(string)$registro['telefono_cel'])
                        ->setCellValue('H'.$i,$registro['correo'])
                        ->setCellValue('I'.$i,$registro['institucionJoven'])
                        ->setCellValue('J'.$i,$registro['facultad'])
                        ->setCellValue('K'.$i,$registro['carrera'])
                        ->setCellValue('L'.$i,($registro['es_estudiante'] == 1)?'Estudiante':'Egresado')
                        ->setCellValue('M'.$i,($registro['es_estudiante'] == 1)?$registro['semestre'].' Semestres':$registro['tiempo_egresado'].' Meses')
                        ->setCellValue('N'.$i,$registro['modalidad'])
                        ->setCellValue('O'.$i,$registro['area'])
                        ->setCellValue('P'.$i,$registro['titulo'])
                        ->setCellValue('Q'.$i,$registro['grado_academico'])
                        ->setCellValue('R'.$i,$registro['nombre_asesor'])
                        ->setCellValue('S'.$i,$registro['apellidos_asesor'])
                        ->setCellValue('T'.$i,$registro['institucionAsesor'])
                        ->setCellValue('U'.$i,$registro['facultad_asesor'])
                        ->setCellValue('V'.$i,$registro['correo_asesor']);
                    $i++;
                }
                $objSheet->getColumnDimension('A')->setAutoSize(true);
                $objSheet->getColumnDimension('B')->setAutoSize(true);
                $objSheet->getColumnDimension('C')->setAutoSize(true);
                $objSheet->getColumnDimension('D')->setAutoSize(true);
                $objSheet->getColumnDimension('E')->setAutoSize(true);
                $objSheet->getColumnDimension('F')->setAutoSize(true);
                $objSheet->getColumnDimension('G')->setAutoSize(true);
                $objSheet->getColumnDimension('H')->setAutoSize(true);
                $objSheet->getColumnDimension('I')->setAutoSize(true);
                $objSheet->getColumnDimension('J')->setAutoSize(true);
                $objSheet->getColumnDimension('K')->setAutoSize(true);
                $objSheet->getColumnDimension('L')->setAutoSize(true);
                $objSheet->getColumnDimension('M')->setAutoSize(true);
                $objSheet->getColumnDimension('N')->setAutoSize(true);
                $objSheet->getColumnDimension('O')->setAutoSize(true);
                $objSheet->getColumnDimension('P')->setAutoSize(true);
                $objSheet->getColumnDimension('Q')->setAutoSize(true);
                $objSheet->getColumnDimension('R')->setAutoSize(true);
                $objSheet->getColumnDimension('S')->setAutoSize(true);
                $objSheet->getColumnDimension('T')->setAutoSize(true);
                $objSheet->getColumnDimension('U')->setAutoSize(true);
                $objSheet->getColumnDimension('V')->setAutoSize(true);
            }
            header('Content-Type: application/vnd.ms-excel;');
            header('Content-Disposition: attachment;filename="reporteParticipantes"');
            header('Cache-Control: max-age=0');

            $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
            $objWriter->save('php://output');
            $conexion->close();
            exit;
            break;
        case 2:
            //Evaluadores
            include("connection.php");
            $conexion = my_connection();
            $sql = "select
                            id_evaluador,
                            titulo,
                            concat_ws(' ', abreviatura_titulo, nombre) as nombre,
                            concat_ws(' ', apellido_pat, apellido_mat) as apellidos,
                            concat(ciudad, ', ', nombre_municipio) as localidad,
                            telefono_casa,
                            telefono_cel,
                            evaluadores.correo,
                            insti1.nombre_institucion as institucion,
                            facultad,
                            nombre_area as area,
                            es_sni,
                            nivel_sni,
                            es_promep,
                            cuerpo_academico,
                            clave_ca,
                            es_consolidado
                        from
                            evaluadores
                                LEFT JOIN usuarios ON usuarios.correo = evaluadores.correo
                                left join municipios ON evaluadores.id_municipio = municipios.id_municipio
                                left join instituciones as insti1 ON evaluadores.id_institucion = insti1.id_institucion
                                left join area_conocimiento ON evaluadores.id_area = area_conocimiento.id_area
                                WHERE usuarios.status = 'ON'
                                order by evaluadores.id_evaluador;";
            $resultado = $conexion->query($sql);
            $registros = $resultado->num_rows;

            if ($registros > 0) {
                require_once 'phpExcel/PHPExcel.php';
                $objPHPExcel = new PHPExcel();
                // set default font
                $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
                // set default font size
                $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
                // writer already created the first sheet for us, let's get it
                $objSheet = $objPHPExcel->getActiveSheet();
                // rename the sheet
                $objSheet->setTitle(utf8_encode('Evaluadores'));
                // let's bold and size the header font and write the header
                // as you can see, we can specify a range of cells, like here: cells from A1 to A4
                $objSheet->getStyle('A1:Q3')->getFont()->setBold(true)->setSize(13);
                // write headers
                $objSheet->getCell('A1')->setValue('Registo de Evaluadores del 3° Encuentro Estatal de Jóvenes Investigadores');
                $objSheet->mergeCells('A1:Q1');
                $objSheet->getCell('A2')->setValue('Datos del Evaluador');
                $objSheet->mergeCells('A2:K2');
                $objSheet->getCell('L2')->setValue('Distinciones Academicas');
                $objSheet->mergeCells('L2:Q2');
                $objSheet->getCell('A3')->setValue('ID Evaluador');
                $objSheet->getCell('B3')->setValue('Título Academico');
                $objSheet->getCell('C3')->setValue('Nombre(s)');
                $objSheet->getCell('D3')->setValue('Apellidos');
                $objSheet->getCell('E3')->setValue('Localidad (Cd., Municipio)');
                $objSheet->getCell('F3')->setValue('Télefono de casa');
                $objSheet->getCell('G3')->setValue('Celular');
                $objSheet->getCell('H3')->setValue('Correo');
                $objSheet->getCell('I3')->setValue('Institución de Educación Superior');
                $objSheet->getCell('J3')->setValue('Facultad, Instituto o Escuela');
                $objSheet->getCell('K3')->setValue('Área de conocimiento');
                $objSheet->getCell('L3')->setValue('Miembro SNI');
                $objSheet->getCell('M3')->setValue('Nivel SNI');
                $objSheet->getCell('N3')->setValue('Perfil PROMEP');
                $objSheet->getCell('O3')->setValue('Cuerpo Academico');
                $objSheet->getCell('P3')->setValue('Clave CA');
                $objSheet->getCell('Q3')->setValue('Grado de Consolidación');

                //Informacion del excel
                $objPHPExcel->
                getProperties()
                    ->setCreator(utf8_encode("Webmaster Jonathan Islas"))
                    ->setLastModifiedBy(utf8_encode("Webmaster Jonathan Islas"))
                    ->setTitle(utf8_encode("Evaluadores JvInv 2015"))
                    ->setSubject(utf8_encode("Evaluadores"))
                    ->setDescription(utf8_encode("Documento generado con PHPExcel"))
                    ->setKeywords(utf8_encode("Evaluadores"))
                    ->setCategory(utf8_encode("Evaluadores"));

                $i = 4;
                while ($registro = $resultado->fetch_assoc()) {
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i,$registro['id_evaluador'])
                        ->setCellValue('B'.$i,$registro['titulo'])
                        ->setCellValue('C'.$i,$registro['nombre'])
                        ->setCellValue('D'.$i,$registro['apellidos'])
                        ->setCellValue('E'.$i,$registro['localidad'])
                        ->setCellValue('F'.$i,(string)$registro['telefono_casa'])
                        ->setCellValue('G'.$i,(string)$registro['telefono_cel'])
                        ->setCellValue('H'.$i,$registro['correo'])
                        ->setCellValue('I'.$i,$registro['institucion'])
                        ->setCellValue('J'.$i,$registro['facultad'])
                        ->setCellValue('K'.$i,$registro['area'])
                        ->setCellValue('L'.$i,$registro['es_sni'])
                        ->setCellValue('M'.$i,$registro['nivel_sni'])
                        ->setCellValue('N'.$i,$registro['es_promep'])
                        ->setCellValue('O'.$i,$registro['cuerpo_academico'])
                        ->setCellValue('P'.$i,$registro['clave_ca'])
                        ->setCellValue('Q'.$i,$registro['es_consolidado']);
                    $i++;
                }
                $objSheet->getColumnDimension('A')->setAutoSize(true);
                $objSheet->getColumnDimension('B')->setAutoSize(true);
                $objSheet->getColumnDimension('C')->setAutoSize(true);
                $objSheet->getColumnDimension('D')->setAutoSize(true);
                $objSheet->getColumnDimension('E')->setAutoSize(true);
                $objSheet->getColumnDimension('F')->setAutoSize(true);
                $objSheet->getColumnDimension('G')->setAutoSize(true);
                $objSheet->getColumnDimension('H')->setAutoSize(true);
                $objSheet->getColumnDimension('I')->setAutoSize(true);
                $objSheet->getColumnDimension('J')->setAutoSize(true);
                $objSheet->getColumnDimension('K')->setAutoSize(true);
                $objSheet->getColumnDimension('L')->setAutoSize(true);
                $objSheet->getColumnDimension('M')->setAutoSize(true);
                $objSheet->getColumnDimension('N')->setAutoSize(true);
                $objSheet->getColumnDimension('O')->setAutoSize(true);
                $objSheet->getColumnDimension('P')->setAutoSize(true);
                $objSheet->getColumnDimension('Q')->setAutoSize(true);
            }
            header('Content-Type: application/vnd.ms-excel;');
            header('Content-Disposition: attachment;filename="reporteEvaluadores"');
            header('Cache-Control: max-age=0');

            $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
            $objWriter->save('php://output');
            $conexion->close();
            exit;
            break;
        case 3: // reporte final
            include("connection.php");
            $conexion = my_connection();
            $sql = "select folio, concat_ws(' ', nombre, apellido_pat, apellido_mat) as nombre,
                      id_trabajo, titulo, nombre_modalidad, nombre_area
                    from usuarios
                    LEFT JOIN participantes ON participantes.correo = usuarios.correo
                    LEFT JOIN trabajos ON participantes.id_participante = trabajos.id_participante
                    LEFT JOIN area_conocimiento ON area_conocimiento.id_area = trabajos.id_area
                    LEFT JOIN modalidad ON modalidad.id_modalidad = trabajos.id_modalidad
                    where eval_previa = 'Verificado';";
            $resultado = $conexion->query($sql);
            $registros = $resultado->num_rows;

            if ($registros > 0) {
                require_once 'phpExcel/PHPExcel.php';
                $objPHPExcel = new PHPExcel();
                // set default font
                $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
                // set default font size
                $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
                // writer already created the first sheet for us, let's get it
                $objSheet = $objPHPExcel->getActiveSheet();
                // rename the sheet
                $objSheet->setTitle('Reporte Calificaciones');
                // let's bold and size the header font and write the header
                // as you can see, we can specify a range of cells, like here: cells from A1 to A4
                $objSheet->getStyle('A1:V3')->getFont()->setBold(true)->setSize(13);
                // write headers
                $objSheet->getCell('A1')->setValue('Registo de participantes del 3° Encuentro Estatal de Jóvenes Investigadores');
                $objSheet->mergeCells('A1:O1');
                $objSheet->getCell('A2')->setValue('Datos del Participante');
                $objSheet->mergeCells('A2:B2');
                $objSheet->getCell('C2')->setValue('Datos del Trabajo');
                $objSheet->mergeCells('C2:E2');
                $objSheet->getCell('F2')->setValue('Evaluación');
                $objSheet->mergeCells('F2:O2');
                $objSheet->getCell('A3')->setValue('Folio');
                $objSheet->getCell('B3')->setValue('Nombre del participante');
                $objSheet->getCell('C3')->setValue('Título del Trabajo');
                $objSheet->getCell('D3')->setValue('Modalidad');
                $objSheet->getCell('E3')->setValue('Área de Investigación');
                $objSheet->getCell('F3')->setValue('Nombre Evaluador 1');
                $objSheet->getCell('G3')->setValue('Nombre Evaluador 2');
                $objSheet->getCell('H3')->setValue('Nombre Evaluador 3');
                $objSheet->getCell('I3')->setValue('Calificación Total 1');
                $objSheet->getCell('J3')->setValue('Calificación Total 2');
                $objSheet->getCell('K3')->setValue('Calificación Total 3');
                $objSheet->getCell('L3')->setValue('Calificación Final 1era Etapa');
                $objSheet->getCell('M3')->setValue('Comentario 1');
                $objSheet->getCell('N3')->setValue('Comentario 2');
                $objSheet->getCell('O3')->setValue('Comentario 3');


                //Informacion del excel
                $objPHPExcel->
                getProperties()
                    ->setCreator("Webmaster Jonathan Islas")
                    ->setLastModifiedBy("Webmaster Jonathan Islas")
                    ->setTitle("Reporte Calificaciones JvInv 2015")
                    ->setSubject("Calificaciones")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("Calificaciones")
                    ->setCategory("Calificaciones");

                $i = 4;
                while ($registro =$resultado->fetch_assoc()) {
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i,$registro['folio'])
                        ->setCellValue('B'.$i,$registro['nombre'])
                        ->setCellValue('C'.$i,$registro['titulo'])
                        ->setCellValue('D'.$i,$registro['nombre_modalidad'])
                        ->setCellValue('E'.$i,$registro['nombre_area']);

                    $id_trabajo  = $registro['id_trabajo'];

                    $sql = "select concat_ws(' ',abreviatura_titulo, nombre, apellido_pat, apellido_mat) as nombre,
                              evaluaciones.* from evaluaciones
                              LEFT JOIN evaluadores ON evaluadores.id_evaluador = evaluaciones.id_evaluador
                              LEFT JOIN usuarios ON usuarios.correo = evaluadores.correo
                              where id_trabajo = $id_trabajo;";
                    $resultado2 = $conexion->query($sql);
                    $eval1 = $resultado2->fetch_assoc();
                    $eval2 = $resultado2->fetch_assoc();
                    $eval3 = $resultado2->fetch_assoc();

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('F'.$i,$eval1['nombre'])
                        ->setCellValue('G'.$i,$eval2['nombre'])
                        ->setCellValue('H'.$i,$eval3['nombre'])
                        ->setCellValue('I'.$i,$eval1['calif_total'])
                        ->setCellValue('J'.$i,$eval2['calif_total'])
                        ->setCellValue('K'.$i,$eval3['calif_total'])
                        ->setCellValue('L'.$i,($eval3['calif_total']+$eval2['calif_total']+$eval1['calif_total']))
                        ->setCellValue('M'.$i,$eval1['comentario'])
                        ->setCellValue('N'.$i,$eval2['comentario'])
                        ->setCellValue('O'.$i,$eval3['comentario']);

                    $i++;
                }
                $objSheet->getColumnDimension('A')->setAutoSize(true);
                $objSheet->getColumnDimension('B')->setAutoSize(true);
                $objSheet->getColumnDimension('C')->setAutoSize(true);
                $objSheet->getColumnDimension('D')->setAutoSize(true);
                $objSheet->getColumnDimension('E')->setAutoSize(true);
                $objSheet->getColumnDimension('F')->setAutoSize(true);
                $objSheet->getColumnDimension('G')->setAutoSize(true);
                $objSheet->getColumnDimension('H')->setAutoSize(true);
                $objSheet->getColumnDimension('I')->setAutoSize(true);
                $objSheet->getColumnDimension('J')->setAutoSize(true);
                $objSheet->getColumnDimension('K')->setAutoSize(true);
                $objSheet->getColumnDimension('L')->setAutoSize(true);

            }
            header('Content-Type: application/vnd.ms-excel;');
            header('Content-Disposition: attachment;filename="reporteCalificaciones"');
            header('Cache-Control: max-age=0');

            $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
            $objWriter->save('php://output');
            $conexion->close();
            exit;
            break;
        default:
            header("Location: ../inicio.php");
            break;
    }
}else{
    header("Location: ../inicio.php");
}
?>