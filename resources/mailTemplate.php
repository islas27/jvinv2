<?php
include("server_vars.php");
$head = '<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for Linux (vers 25 March 2009), see www.w3.org" />
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for Linux (vers 25 March 2009), see www.w3.org" />
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=320, target-densitydpi=device-dpi" />
<!--[if gte mso 9]>
<style _tmplitem="1250" >
.article-content ol, .article-content ul {
   margin: 0 0 0 24px;
   padding: 0;
   list-style-position: inside;
}
</style>
<![endif]-->
  </head>
  <body style="width: 100% !important;background-color: #EEE;margin: 0;padding: 0;font-family: HelveticaNeue, sans-serif">
<table id="background-table" class="c10" cellpadding="0" cellspacing="0" align="center" border="0" width="100%" style="background-color: #EEE;table-layout: fixed"><tbody><tr style="border-collapse: collapse"><td class="c9" align="center" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif;background-color: #EEE">
<table class="w640 c8" cellpadding="0" cellspacing="0" border="0" width="640" style="margin: 0 10px"><tbody><tr style="border-collapse: collapse"><td class="w640" height="20" width="640" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr><tr style="border-collapse: collapse"><td class="w640" width="640" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif">
<table id="top-bar" class="w640 c1" cellpadding="0" cellspacing="0" border="0" width="640" style="border-radius: 6px 6px 0 0;-moz-border-radius: 6px 6px 0 0;-webkit-border-radius: 6px 6px 0 0;-webkit-font-smoothing: antialiased;background-color: #5E9666;color: #FFF"><tbody><tr style="border-collapse: collapse"><td class="w15" width="15" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w325" align="left" valign="middle" width="350" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif">
<table class="w325" cellpadding="0" cellspacing="0" border="0" width="350"><tbody><tr style="border-collapse: collapse"><td class="w325" height="8" width="350" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr></tbody></table><div class="header-content" style="-webkit-text-size-adjust: none;-ms-text-size-adjust: none;font-size: 12px;color: #FFF">   <a href="http://jovenesinvestigadores.uach.mx/registro" style="font-weight: bold;color: #FFF;text-decoration: none">Iniciar Sesi&oacute;n</a></div>
<table class="w325" cellpadding="0" cellspacing="0" border="0" width="350"><tbody><tr style="border-collapse: collapse"><td class="w325" height="8" width="350" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr></tbody></table></td>
<td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w255" align="right" valign="middle" width="255" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif">
<table class="w255" cellpadding="0" cellspacing="0" border="0" width="255"><tbody><tr style="border-collapse: collapse"><td class="w255" height="8" width="255" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr></tbody></table><table cellpadding="0" cellspacing="0" border="0"><tbody><tr style="border-collapse: collapse"><td style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr></tbody></table><table class="w255" cellpadding="0" cellspacing="0" border="0" width="255"><tbody><tr style="border-collapse: collapse"><td class="w255" height="8" width="255" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr></tbody></table></td>
<td class="w15" width="15" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr></tbody></table></td>
</tr><tr style="border-collapse: collapse"><td id="header" class="w640 c3" align="center" width="640" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif;background-color: #5E9666">
<table class="w640" cellpadding="0" cellspacing="0" border="0" width="640"><tbody><tr style="border-collapse: collapse"><td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w580" height="30" width="580" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr><tr style="border-collapse: collapse"><td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w580" width="580" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif">
<div class="c2" id="headline" style="text-align: center">
<p style="color: #FFF;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;font-size: 30px;text-align: left;margin-top: 0;margin-bottom: 30px"><strong><a href="http://jovenesinvestigadores.uach.mx" style="color: #FFF;text-decoration: none">'.$num_evento.' Encuentro de J&oacute;venes Investigadores de IES del Estado de Chihuahua</a></strong></p>
</div>
</td>
<td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr></tbody></table></td>
</tr><tr style="border-collapse: collapse"><td class="w640 c4" height="30" width="640" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif;background-color: #FFF"></td>
</tr><tr id="simple-content-row" style="border-collapse: collapse"><td class="w640 c4" width="640" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif;background-color: #FFF">
<table class="w640" cellpadding="0" cellspacing="0" align="left" border="0" width="640"><tbody><tr style="border-collapse: collapse"><td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w580" width="580" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif">
<table class="w580" cellpadding="0" cellspacing="0" border="0" width="580"><tbody><tr style="border-collapse: collapse"><td class="w580" width="580" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif">
<p class="article-title c5" style="font-size: 18px;line-height: 24px;color: #518258;font-weight: bold;margin-top: 0;margin-bottom: 18px;font-family: HelveticaNeue, sans-serif;text-align: left">';
$middle = '</p>
<div class="article-content c6" style="font-size: 13px;line-height: 18px;color: #888;margin-top: 0;margin-bottom: 18px;font-family: HelveticaNeue, sans-serif;text-align: left">';
$tail = '</div>
</td>
</tr><tr style="border-collapse: collapse"><td class="w580" height="10" width="580" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr></tbody></table></td>
<td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr></tbody></table></td>
</tr><tr style="border-collapse: collapse"><td class="w640 c4" height="15" width="640" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif;background-color: #FFF"></td>
</tr><tr style="border-collapse: collapse"><td class="w640" width="640" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif">
<table id="footer" class="w640 c1" cellpadding="0" cellspacing="0" border="0" width="640" style="border-radius: 0 0 6px 6px;-moz-border-radius: 0 0 6px 6px;-webkit-border-radius: 0 0 6px 6px;-webkit-font-smoothing: antialiased;background-color: #5E9666;color: #FFF"><tbody><tr style="border-collapse: collapse"><td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w580 h0" height="30" width="360" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w0" width="60" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w0" width="160" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr><tr style="border-collapse: collapse"><td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w580" valign="top" width="360" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif">
<p id="permission-reminder" class="footer-content-left c5" style="-webkit-text-size-adjust: none;-ms-text-size-adjust: none;font-size: 12px;line-height: 15px;color: #FFF;margin-top: 0;margin-bottom: 15px;white-space: normal;text-align: left"><span class="hide">Este es un correo automatizado, favor de no responder.</span></p>
</td>
<td class="hide w0" width="60" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="hide w0" valign="top" width="160" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif">
<p id="street-address" class="footer-content-right c7" style="-webkit-text-size-adjust: none;-ms-text-size-adjust: none;font-size: 11px;line-height: 16px;color: #FFF;margin-top: 0;margin-bottom: 15px;white-space: normal;text-align: right"><span>Chihuahua, Chihuahua, M&eacute;xico</span><br style="line-height: 100%" /><span>'.$year.'(c)</span></p>
</td>
<td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr><tr style="border-collapse: collapse"><td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w580 h0" height="15" width="360" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w0" width="60" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w0" width="160" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
<td class="w30" width="30" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr></tbody></table></td>
</tr><tr style="border-collapse: collapse"><td class="w640" height="60" width="640" style="border-collapse: collapse;font-family: HelveticaNeue, sans-serif"></td>
</tr></tbody></table></td>
</tr></tbody></table></body>
</html>
';