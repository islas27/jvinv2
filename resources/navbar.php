<?php
    function isActive($requestUri){
        $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");
        if ($current_file_name === $requestUri) {
            echo 'class="active"';
        }
    }
?>

<nav class="navbar navbar-default" style="border-radius: 0px; margin-bottom: 0px">
    <div class="container-fluid" style="padding-right: 0px; padding-left: 5px;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#link-list">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://jovenesinvestigadores.uach.mx"
                style="padding-top: 5px; font-size: 18px; line-height: 20px;">
                Jóvenes Investigadores<br>de IES del Estado de Chihuahua
            </a>
        </div>

        <div class="collapse navbar-collapse" id="link-list">
            <?php
                $data = (isset($_SESSION['tipo_usuario']))? $_SESSION['tipo_usuario'] : 0;
                switch($data) {
                    case 1:
                        include("navbar/coordinacion.php");
                        break;
                    case 2:
                        include("navbar/coordinacion.php");
                        break;
                    case 3:
                        include("navbar/coordinacion.php");
                        break;
                    case 4:
                        include("navbar/evaluadores.php");
                        break;
                    case 5:
                        include("navbar/participantes.php");
                        break;
                    default:
                        include("navbar/inicio.php");
                        break;
                }
            ?>
        </div>
    </div>
</nav>