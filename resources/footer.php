<footer>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 fix" style="margin-top: 2em;">
        <ul class="list-unstyled" style="padding-top: 5px;">
            <li style="display: inline;"><a href="http://jovenesinvestigadores.uach.mx/">Regresar</a></li>
            <li style="display: inline; padding-left: 10px"><a href="http://uach.mx">Universidad Autónoma de Chihuahua</a></li>
            <li style="display: inline; padding-left: 10px"><a href="http://www.conacyt.com">Conacyt</a></li>
        </ul>
        <p>Realizado por <a href="http://mx.linkedin.com/pub/jonathan-islas/99/b40/333/">Jonathan Islas</a>. Contacto: <a href="mailto:wm.jivestigadores@gmail.com">wm.jivestigadores@gmail.com</a></p>
        Correo de contacto de la coordinación: <a href="mailto:coord.jinvestigadoresuach@gmail.com">coord.jinvestigadoresuach@gmail.com</a>
        <br>Esta página utiliza Java Script, habilítelo si no puede ver adecuadamente el contenido.
        <br>Esta página es mejor visualizada en la última versión de <a href="https://www.mozilla.org/es-MX/">Mozilla Firefox</a> y <a href="http://www.google.com.mx/intl/es-419/chrome/">Google Chrome</a></p>
    </div>
</footer>
