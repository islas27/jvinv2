<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/24/2015
 * Time: 9:49 AM
 */

/*      Error=0     :       Base de datos cerrada
 *      Error=1     :       Acceso ilegal a otras secciones
 *      Error=2     :       Acceso a controladores sin datos
 *      Error=3     :       Correo invalido/sin registrar
 *      Error=4     :       Error desconocido con la query o intento de injeccion
 *      Error=5     :       Acceso restringido al area (session o tipo)
 *      Error=6     :       Correo Repetido en participantes
 *      Error=7     :       Correo Repetido en evaluadores
 *      Success=8   :       Registro Agregado con exito
 * */
function error_load($dimensions, $message_code){
    switch ($message_code){
        case 0:
            $type = 'alert-danger';
            $header = 'Ha ocurrido un problema';
            $message = 'Su petición no ha podido completarse satisfactoriamente, le pedimos volver a intentralo.
                        <br>Si el problema persiste, contacte a la coordinación o al webmaster.';
            break;
        case 1:
            $type = 'alert-warning';
            $header = 'Acceso Denegado';
            $message = 'Su usuario no posee los permisos para acceder a esa sección';
            break;
        case 2:
            $type = 'alert-danger';
            $header = 'Ha ocurrido un problema';
            $message = 'Su petición no ha podido completarse satisfactoriamente, le pedimos volver a intentralo.
                        <br>Si el problema persiste, contacte a la coordinación o al webmaster.';
            break;
        case 3:
            $type = 'alert-danger';
            $header = '¡Usuario o contrase&ntilde;a Incorrecta!';
            $message = 'Ingrese datos correctos, registrese o verifique la información introducida.';
            break;
        case 4:
            $type = 'alert-warning';
            $header = 'Ha ocurrido un problema';
            $message = 'Su petición no ha podido completarse satisfactoriamente, le pedimos volver a intentralo.
                        <br>Si el problema persiste, contacte a la coordinación o al webmaster.';
            break;
        case 5:
            $type = 'alert-warning';
            $header = 'Acceso Denegado';
            $message = 'Inicie Sesión para acceder a esta sección';
            break;
        case 6:
            $type = 'alert-warning';
            $header = 'Correo registrado con anterioridad';
            $message = 'Ingrese un correo diferente, o verifique su inscripción';
            break;
        case 7:
            $type = 'alert-warning';
            $header = '¡Evaluador ya registrado!';
            $message = 'Verifique el correo ingresado';
            break;
        case 8:
            $type = 'alert-warning';
            $header = 'Envio Fallido';
            $message = 'No se envio el mensaje, vuelva a intentralo.
                        <br>Si el problema persiste, contacte al webmaster.';
            break;
        case 9:
            $type = 'alert-warning';
            $header = 'Evaluación no guardada';
            $message = 'Su petición no ha podido completarse satisfactoriamente, le pedimos volver a intentralo.
                        <br>Si el problema persiste, contacte a la coordinación o al webmaster.';
            break;
        case 10:
            $type = 'alert-success';
            $header = 'Periodo de Inscripción Concluido';
            $message = 'Lo sentimos pero el periodo de inscripciones ha finalizado.<br>
                        Si concluiste tu inscripción y resultas elegido para la presentación, te llegara una
                        notificación a tu correo.<br><b>¡Éxito!</b>';
            break;
        default :
            $type = 'alert-warning';
            $header = 'Ha ocurrido un problema';
            $message = 'Su petición no ha podido completarse satisfactoriamente, le pedimos volver a intentralo.
                        <br>Si el problema persiste, contacte al webmaster.';
            break;
    }

    echo '<div class="alert alert-dismissable '.$type.' '.$dimensions.'">
            <button type="button" class="close" data-dismiss="alert" style="font-size:small">cerrar</button>
            <h4>'.$header.'</h4>
            <p>'.$message.'</p>
        </div>';
}

function success_load($dimensions, $message_code){
    switch ($message_code){
        case 1:
            //Registro de participante exitoso
            $header = 'Sus datos han sido guardados';
            $message = 'A continuación inicie sesión para completar su registro.';
            break;
        case 2:
            //Registro de trabajo exitoso
            $header = 'Puede seguir con su registro';
            $message = 'La información se guardó correctamente.
                    <br>Para concluir el registro suba los archivos solicitados a continuación';
            break;
        case 3:
            //Subida de archivos exitosa
            $header = '¡Registro Concluido!';
            $message = 'La solicitud ha sido registrada, el folio de inscripción será enviado a su correo electrónico.
                        La notificación de aceptación de propuestas evaluadas, será del 25 al 28 de mayo de 2015';
            break;
        case 4:
            //Registro de evaluacion exitoso
            $header = 'Evaluaci&oacute;n Almacenada';
            $message = 'Se ha enviado con éxito la evaluación realizada.';
            break;
        case 5:
            //Registro de evaluador exitoso
            $header = 'Evaluador guardado correctamente';
            $message = 'Se ha enviado un correo automatico con sus credenciales de acceso';
            break;
        case 6:
            //registro de personal exitoso
            $header = 'Nuevo usuario guardado';
            $message = 'Se creó el usuario de manera exitosa. La contraseña se envió a su correo automaticamente.';
            break;
        case 7:
            //Desactivacion exitosa
            $header = 'Baja Exitosa';
            $message = 'Se dio de baja el usuario de manera exitosa.';
            break;
        case 8:
            //Correo Exitoso
            $header = 'Envio Exitoso';
            $message= 'El correo se ha enviado correctamente.';
            break;
        case 9:
            //asignacion de evaluadores exitosa
            $header = 'Asginación almacenada';
            $message = 'Se ha enviado con éxito los correos a los evaluadores correspondientes.';
            break;
        case 10:
            //Veredicto final registrado
            $header = 'Veredicto almacenado';
            $message = 'Se ha almacenado con éxito el valor asignado.';
            break;
        default :
            $header = 'Operación realizada con exito';
            $message = 'Se realizo el registro correctamente';
            break;
    }
    $type = 'alert-success';
    echo '<div class="alert alert-dismissable '.$type.' '.$dimensions.'">
            <button type="button" class="close" data-dismiss="alert" style="font-size:small">cerrar</button>
            <h4>'.$header.'</h4>
            <p>'.$message.'</p>
        </div>';
}
