<?php

class participante{
    public $folio;
    public $id_usuario;
    public $id_participante;
    public $correo;
    public $nombre;
    public $apellido_pat;
    public $apellido_mat;
    public $calle;
    public $num_ext;
    public $num_int;
    public $colonia;
    public $codigo_postal;
    public $municipio;
    public $localidad;
    public $telefono_casa;
    public $telefono_cel;
    public $institucion;
    public $facultad;
    public $carrera;
    public $es_estudiante;
    public $semestre;
    public $tiempo_egresado;
    public $eval1 = "Sin Registrar";
    public $eval2 = "Sin Registrar";
    public $eval3 = "Sin Registrar";
    public $eval1_calif = "Sin Registrar";
    public $eval2_calif = "Sin Registrar";
    public $eval3_calif = "Sin Registrar";
    public $evaluacion_previa;
    public $veredicto;
    public $id_trabajo;
    public $titulo;
    public $area;
    public $modalidad;
    public $colaborador;
    public $grado_academico;
    public $nombre_asesor;
    public $apellidos_asesor;
    public $institucion_asesor;
    public $facultad_asesor;
    public $correo_asesor;
    public $fecha_registro;
    public $fecha_subida;
    public $ubicaciones;

    public function set_folio($folio){
        if(!empty($folio)){
            $this->folio = $folio;
        }else{
            $this->folio = 'Sin Registrar';
        }
    }

    public function set_id_usuario($id_usuario){
        $this->id_usuario = $id_usuario;
    }

    public function set_id_participante($id_participante){
        $this->id_participante = $id_participante;
    }

    public function set_id_trabajo($id_trabajo)
    {
        if(!empty($id_trabajo)){
            $this->id_trabajo = $id_trabajo;
        }else{
            $this->id_trabajo = false;
        }
    }

    public function set_correo($correo){
        $this->correo = $correo;
    }

    public function set_nombre($nombre){
        $this->nombre = $nombre;
    }

    public function set_apellido_pat($apellido_pat){
        $this->apellido_pat = $apellido_pat;
    }

    public function set_apellido_mat($apellido_mat){
        $this->apellido_mat = $apellido_mat;
    }

    public function set_calle($calle){
        $this->calle = $calle;
    }

    public function set_num_ext($num_ext){
        $this->num_ext = $num_ext;
    }

    public function set_num_int($num_int){
        if(!empty($num_int)){
            $this->num_int = $num_int;
        }else{
            $this->num_int = "Sin Registrar";
        }
    }

    public function set_colonia($colonia){
        $this->colonia = $colonia;
    }

    public function set_codigo_postal($codigo_postal){
        $this->codigo_postal = $codigo_postal;
    }

    public function set_municipio($municipio){
        $this->municipio = $municipio;
    }

    public function set_localidad($localidad){
        $this->localidad = $localidad;
    }

    public function set_telefono_casa($telefono_casa){
        $this->telefono_casa = $telefono_casa;
    }

    public function set_telefono_cel($telefono_cel){
        $this->telefono_cel = $telefono_cel;
    }

    public function set_institucion($institucion){
        $this->institucion = $institucion;
    }

    public function set_facultad($facultad){
        if(!empty($facultad)){
            $this->facultad = $facultad;
        }else{
            $this->facultad = "Sin Registrar";
        }
    }

    public function set_carrera($carrera){
        $this->carrera = $carrera;
    }

    public function set_es_estudiante($es_estudiante){
        $this->es_estudiante = $es_estudiante;
    }

    public function set_semestre($semestre){
        if(!empty($semestre)){
            $this->semestre = $semestre;
        }else{
            $this->semestre = "Sin Registrar";
        }
    }

    public function set_tiempo_egresado($tiempo_egresado){
        if(!empty($tiempo_egresado)){
            $this->tiempo_egresado = $tiempo_egresado;
        }else{
            $this->tiempo_egresado = "Sin Registrar";
        }
    }

    public function set_eval1($eval1){
        $this->eval1 = $eval1;
    }

    public function set_eval2($eval2){
        $this->eval2 = $eval2;
    }

    public function set_eval3($eval3){
        $this->eval3 = $eval3;
    }

    public function set_eval1_calif($eval1_calif){
        $this->eval1_calif = (!empty($eval1_calif))?$eval1_calif:'Sin Registrar';
    }

    public function set_eval2_calif($eval2_calif){
        $this->eval2_calif = (!empty($eval2_calif))?$eval2_calif:'Sin Registrar';
    }

    public function set_eval3_calif($eval3_calif){
        $this->eval3_calif = (!empty($eval3_calif))?$eval3_calif:'Sin Registrar';
    }

    public function set_evaluacion_previa($evaluacion_previa){
        if(!empty($evaluacion_previa)){
            $this->evaluacion_previa = $evaluacion_previa;
        }else{
            $this->evaluacion_previa = "Sin Registrar";
        }
    }

    public function set_veredicto($veredicto){
        if(!empty($veredicto)){
            $this->veredicto = $veredicto;
        }else{
            $this->veredicto = "Sin Registrar";
        }
    }

    public function set_titulo($titulo){
        if(!empty($titulo)){
            $this->titulo = $titulo;
        }else{
            $this->titulo = "Sin Registrar";
        }
    }

    public function set_area($area){
        if(!empty($area)){
            $this->area = $area;
        }else{
            $this->area = "Sin Registrar";
        }
    }

    public function set_modalidad($modalidad){
        if(!empty($modalidad)){
            $this->modalidad = $modalidad;
        }else{
            $this->modalidad = "Sin Registrar";
        }
    }

    public function set_colaborador($colaborador){
        if(!empty($colaborador)){
            $this->colaborador = $colaborador;
        }else{
            $this->colaborador = "Sin Registrar";
        }
    }

    public function set_grado_academico($grado_academico){
        if(!empty($grado_academico)){
            $this->grado_academico = $grado_academico;
        }else{
            $this-> grado_academico= "Sin Registrar";
        }
    }

    public function set_nombre_asesor($nombre_asesor){
        if(!empty($nombre_asesor)){
            $this->nombre_asesor = $nombre_asesor;
        }else{
            $this->nombre_asesor = "Sin Registrar";
        }
    }

    public function set_apellidos_asesor($apellidos_asesor){
        if(!empty($apellidos_asesor)){
            $this->apellidos_asesor = $apellidos_asesor;
        }else{
            $this->apellidos_asesor = "Sin Registrar";
        }
    }

    public function set_institucion_asesor($institucion_asesor){
        if(!empty($institucion_asesor)){
            $this->institucion_asesor = $institucion_asesor;
        }else{
            $this->institucion_asesor = "Sin Registrar";
        }
    }

    public function set_facultad_asesor($facultad_asesor){
        if(!empty($facultad_asesor)){
            $this->facultad_asesor = $facultad_asesor;
        }else{
            $this->facultad_asesor = "Sin Registrar";
        }
    }

    public function set_correo_asesor($correo_asesor){
        if(!empty($correo_asesor)){
            $this->correo_asesor = $correo_asesor;
        }else{
            $this->correo_asesor = "Sin Registrar";
        }
    }

    public function set_fecha_registro($fecha_registro){
        $this->fecha_registro = $fecha_registro;
    }

    public function set_fecha_subida($fecha_subida){
        if(!empty($fecha_subida)){
            $this->fecha_subida = $fecha_subida;
        }else{
            $this->$fecha_subida= "Sin Registrar";
        }
    }


}