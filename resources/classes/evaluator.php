<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 5/3/2015
 * Time: 7:52 PM
 */

class evaluator{

    public $id_usuario;
    public $id_evaluador;
    public $correo;
    public $saludo;
    public $nombre;
    public $apellido_pat;
    public $apellido_mat;
    public $titulo_academico;
    public $municipio;
    public $ciudad;
    public $telefono_casa;
    public $telefono_cel;
    public $institucion;
    public $facultad;
    public $area;
    public $modalidad;
    public $es_sni;
    public $nivel_sni;
    public $es_promep;
    public $cuerpo_academico;
    public $clave_ca;
    public $es_consolidado;
    public $fecha_registro;
    public $num_trabajos;

    public function get_id_usuario(){
        return $this->id_usuario;
    }

    public function set_id_usuario($id_usuario){
        $this->id_usuario = $id_usuario;
    }

    public function get_id_evaluador(){
        return $this->id_evaluador;
    }

    public function set_id_evaluador($id_evaluador){
        $this->id_evaluador = $id_evaluador;
    }

    public function get_correo(){
        return $this->correo;
    }

    public function set_correo($correo){
        $this->correo = $correo;
    }

    public function get_saludo(){
        return $this->saludo;
    }

    public function set_saludo($saludo){
        $this->saludo = $saludo;
    }

    public function get_nombre(){
        return $this->nombre;
    }

    public function set_nombre($nombre){
        $this->nombre = $nombre;
    }

    public function get_apellido_pat(){
        return $this->apellido_pat;
    }

    public function set_apellido_pat($apellido_pat){
        $this->apellido_pat = $apellido_pat;
    }

    public function get_apellido_mat(){
        return $this->apellido_mat;
    }

    public function set_apellido_mat($apellido_mat){
        $this->apellido_mat = $apellido_mat;
    }

    public function get_titulo_academico(){
        return $this->titulo_academico;
    }

    public function set_titulo_academico($titulo_academico){
        $this->titulo_academico = $titulo_academico;
    }

    public function get_municipio(){
        return $this->municipio;
    }

    public function set_municipio($municipio){
        $this->municipio = $municipio;
    }

    public function get_ciudad(){
        return $this->ciudad;
    }

    public function set_ciudad($ciudad){
        $this->ciudad = $ciudad;
    }

    public function get_telefono_casa(){
        return $this->telefono_casa;
    }

    public function set_telefono_casa($telefono_casa){
        $this->telefono_casa = $telefono_casa;
    }

    public function get_telefono_cel(){
        return $this->telefono_cel;
    }

    public function set_telefono_cel($telefono_cel){
        $this->telefono_cel = $telefono_cel;
    }

    public function get_institucion(){
        return $this->institucion;
    }

    public function set_institucion($institucion){
        $this->institucion = $institucion;
    }

    public function get_facultad(){
        return $this->facultad;
    }

    public function set_facultad($facultad){
        $this->facultad = $facultad;
    }

    public function get_area(){
        return $this->area;
    }

    public function set_area($area){
        $this->area = $area;
    }

    public function get_modalidad(){
        return $this->modalidad;
    }

    public function set_modalidad($modalidad){
        $this->modalidad = $modalidad;
    }

    public function get_es_sni(){
        return $this->es_sni;
    }

    public function set_es_sni($es_sni){
        $this->es_sni = $es_sni;
    }

    public function get_nivel_sni(){
        return $this->nivel_sni;
    }

    public function set_nivel_sni($nivel_sni){
        $this->nivel_sni = $nivel_sni;
    }

    public function get_es_promep(){
        return $this->es_promep;
    }

    public function set_es_promep($es_promep){
        $this->es_promep = $es_promep;
    }

    public function get_cuerpo_academico(){
        return $this->cuerpo_academico;
    }

    public function set_cuerpo_academico($cuerpo_academico){
        $this->cuerpo_academico = $cuerpo_academico;
    }

    public function get_clave_ca(){
        return $this->clave_ca;
    }

    public function set_clave_ca($clave_ca){
        $this->clave_ca = $clave_ca;
    }

    public function get_es_consolidado(){
        return $this->es_consolidado;
    }

    public function set_es_consolidado($es_consolidado){
        $this->es_consolidado = $es_consolidado;
    }

    public function get_fecha_registro(){
        return $this->fecha_registro;
    }

    public function set_fecha_registro($fecha_registro){
        $this->fecha_registro = $fecha_registro;
    }

    public function get_num_trabajos(){
        return $this->num_trabajos;
    }

    public function set_num_trabajos($num_trabajos){
        $this->num_trabajos = (empty($num_trabajos))? 'Ninguno': $num_trabajos;
    }

}