<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 5/14/2015
 * Time: 2:28 AM
 */
?>

<h3>Evaluación Previa</h3>
<form id="multiselectForm" method="get" class="form-horizontal" action="control/trabajoCTL.php">
    <div class="form-group">
        <label for="previa" class="col-sm-9">¿Cumple este trabajo con los requisitos de la convocatoria y está listo para evaluación?</label>
        <div class="col-sm-3">
            <select class="form-control col-sm-2" style="font-size: 14px; height: 34px;" id="previa" name="previa">
                <option value="">Seleccionar</option>
                <option value="Si">Si</option>
                <option value="No">No</option>
            </select>
        </div>
    </div>
    <div class="form-group" id="multiEv">
        <p>&nbsp; &nbsp; Ahora seleccione los tres evaluadores, el número indica los trabajos que ya tiene asignados tal evaluador.</p>
        <div class="col-sm-12">
            <select class="form-control" id="evaluadores" name="evaluadores" multiple
                data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Este control se activa si selecciona 'Si'<br>en la pregunta anterior">
                <?php
                    $modalidad = $registro['id_modalidad'];
                    $area = $registro['id_area'];
                    $participante = $registro['id_participante'];
                    $trabajo = $registro['id_trabajo'];
                    $archivo = $registro['ubic_trabajo'];
                    $sql = "select evaluadores.id_evaluador, concat_ws(' ',abreviatura_titulo, nombre, apellido_pat, apellido_mat)
                            as nombre, num_trabajos from evaluadores
                            LEFT JOIN usuarios ON usuarios.correo = evaluadores.correo
                            LEFT JOIN trabajos_asignados ON evaluadores.id_evaluador = trabajos_asignados.id_evaluador
                            where id_modalidad = $modalidad and id_area = $area and status = 'ON' ORDER BY num_trabajos, evaluadores.id_evaluador;";
                    $resultado = $conn->query($sql);
                    while($row = $resultado->fetch_assoc()){
                        echo '<option value="'.$row['id_evaluador'].'">'.$row['nombre'].' - '.$row['num_trabajos'].'</option>';
                    }
                ?>
</select>
</div>
</div>
<div class="form-group" style="padding-left: 15px">
    <?php echo "<input type='text' name='id_trabajo' id='id_trabajo' hidden='hidden' value='$trabajo'/>"?>
    <?php echo "<input type='text' name='id_participante' id='id_participante' hidden='hidden' value='$participante'/>"?>
    <?php echo "<input type='text' name='id_usuario' id='id_usuario' hidden='hidden' value='$id_usuario'/>"?>
    <button name="cmdAction" type="submit" class="btn btn-primary" value="2">Guardar</button>
</div>
</form>
<div class="col-sm-12 fix" style="height: 600px; padding-bottom: 20px;">
    <?php echo "<object width='100%' height='100%' type='application/pdf' data='http://jovenesinvestigadores.uach.mx/$archivo'>";?>
    <p>No tienes instalado el plugin para ver PDF's en este navegador.
        Puedes dar <?php echo "<a href='http://jovenesinvestigadores.uach.mx/$archivo'>"?>click aqui</a> Para bajar el documento PDF.</p>
    </object>
</div>