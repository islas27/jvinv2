<h3 style="padding-left: 15px;">Veredicto Final</h3>
<div class="col-xs-12" style="padding-bottom: 15px;">
    <form id="FRMVeredicto" method="get" class="form-horizontal" action="control/coordinacionCTL.php">
        <div class="form-group">
            <label for="previa" class="col-sm-9">¿Desea enviar este trabajo a las presentaciones en vivo?</label>
            <div class="col-sm-3">
                <select class="form-control col-sm-2" style="font-size: 14px; height: 34px;" id="veredicto" name="veredicto">
                    <option value="">Seleccionar</option>
                    <option value="Aprovado">Si</option>
                    <option value="Rechazado">No</option>
                </select>
            </div>
        </div>
        <div class="form-group" style="padding-left: 15px">
            <?php echo "<input type='text' name='id' id='id' hidden='hidden' value='$id_participante'/>"?>
            <?php echo "<input type='text' name='id_usuario' id='id_usuario' hidden='hidden' value='$id_usuario'/>"?>
            <button name="cmdAction" type="submit" class="btn btn-primary" value="6">Guardar</button>
        </div>
    </form>
</div>