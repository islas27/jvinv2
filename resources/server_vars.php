<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/19/2015
 * Time: 11:17 PM
 */

// Configuracion Base de datos
$DBServer = '127.0.0.1'; // e.g 'localhost' or '192.168.1.100'
$DBUser   = 'root';             $DBPass   = "root";                         //Development
//$DBUser   = "usr_jovenesinv";   $DBPass   = "ttyyTFG45790xzc";          //Production
$DBName   = 'inv_fing_db';
$DBPort   = 3306;

// Configuracion de archivos
$folder = 'jv_inv2/';           //development
//$folder = '/';           //Production
$year = '2015';

// Configuracion de sistema
$num_evento ='3er.';
$fecha_inicio_registro = '20 de abril';
$fecha_fin_registro = '13 de mayo';
$fecha_inicio_evaluacion = '14 de mayo';
$fecha_fin_evaluacion = '24 de mayo';