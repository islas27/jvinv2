<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 5/14/2015
 * Time: 12:21 AM
 */

    session_start();
    require_once("resources/session_validation.php");
    require_once("resources/connection.php");
    validate_usertype(4);
    $evaluador = $_SESSION['id_evaluador'];
    if(isset($_GET['id_trabajo'])){
        $trabajo = $_GET['id_trabajo'];
        $conn = my_connection();
        $sql = "select * from evaluaciones
                left join trabajos on trabajos.id_trabajo = evaluaciones.id_trabajo
                LEFT JOIN participantes on participantes.id_participante = trabajos.id_participante
                LEFT JOIN usuarios ON usuarios.correo = participantes.correo
                where evaluaciones.id_evaluador = $evaluador and evaluaciones.id_trabajo = $trabajo;";
        $resultado = $conn->query($sql);
        if($resultado->num_rows == 0){
            header("Location: trabajos_asignados.php?error=4");
        }
        $row = $resultado->fetch_assoc();
    }else{
        $trabajo = false; $row = false;
        header("Location: trabajos_asignados.php?error=4");
    }
    $x = '<option value="">Seleccionar</option>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>';
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Jóvenes Investigadores</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-custom.css" rel="stylesheet">

        <!-- FormValidation CSS file -->
        <link href="css/formValidation.min.css" rel="stylesheet">

        <!-- Footable CSS File-->
        <link href="css/footable.core.min.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php
            include("resources/navbar.php");
            include("resources/carrusel.php");
        ?>
        <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1">
            <h2 style="text-align: center; margin-bottom: 24px">Trabajos por Revisar</h2>
        </div>
        <?php
        if(isset($_GET['error'])){
            include('resources/msg_loading.php');
            error_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
        }
        if(isset($_GET['success'])){
            include('resources/msg_loading.php');
            success_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['success']);
        }
        ?>
        <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
            <!--Asi como usamos col para el espacio horizontal, tenemos row para el espacio vertical-->
            <div class="row" style="padding-left: 15px; padding-top: 15px">
                <p>A continuaci&oacute;n la informaci&oacute;n del trabajo a evaluar:<br><br></p>
                <?php
                    echo '<p>Autor: '.$row['nombre'].' '.$row['apellido_pat'].' '.$row['apellido_mat'].'</p>';
                    echo '<p>T&iacute;tulo: '.$row['titulo'].'</p>';
                    echo '<p>Enlace de descarga: <a href="http://jovenesinvestigadores.uach.mx/'.$row['ubic_trabajo'].'"
                                class="btn btn-xs btn-primary" target="_blank"
                                download="'.str_replace(' ', '_', $row['nombre']).
                                '_'.str_replace(' ', '_', $row['apellido_pat']).
                                '_'.str_replace(' ', '_', $row['apellido_mat']).'.pdf">Descargar</a></p>';
                ?>
                <p>La escala a utilizar en la evaluaci&oacute;n es la siguiente:<br>
                    Escala: 0 = Pésimo, 1 = Deficiente, 2 = Regular, 3 = Bueno, 4 = Muy Bueno, 5 = Sobresaliente<br>
                    <b>Le recordamos que todos los campos son obligatorios, de lo contrario no podra guardar la evaluación.</b><br><br></p>
            </div>
            <div class="row">
                <form id="FRMEval" name="FRMEval" role="form" class="form-horizontal" action="control/trabajoCTL.php" method="post">
                    <div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1">Relevancia del tema</label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="p[]" id="p[]">
                                <?php echo $x;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1">Originalidad del estudio</label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="p[]" id="p[]">
                                <?php echo $x;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1">Problema investigado (Descripción clara)</label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="p[]" id="p[]">
                                <?php echo $x;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1">Objetivos e Hipótesis</label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="p[]" id="p[]">
                                <?php echo $x;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1">Materiales y Métodos (Descripción breve de la metodología y análisis estadístico)</label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="p[]" id="p[]">
                                <?php echo $x;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1">Resultados (Cuadros y/o figuras con resultados sobresalientes)</label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="p[]" id="p[]">
                                <?php echo $x;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1">Discusión (Analiza tendencias y contrastes, relacionando con otros estudios)</label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="p[]" id="p[]">
                                <?php echo $x;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1">Conclusiones (Congruencia con los objetivos)</label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="p[]" id="p[]">
                                <?php echo $x;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1">Bibliografía (Citas de revistas de calidad, recientes y relación directa con el tema)</label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="p[]" id="p[]">
                                <?php echo $x;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1"><?php
                            echo ($row['id_modalidad'] == 1)?'Redacción científica (Estilo, claridad, organización y calidad del documento)':'Calidad del cartel (estilo, claridad, estructura y habilidad al usar cuadros y/o figuras)';
                            ?></label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="p[]" id="p[]">
                                <?php echo $x;?>
                            </select>
                        </div>
                    </div>
                    <!--<div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1">¿Considera este trabajo publicable?</label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="publicable" id="publicable">
                                <option value="">Seleccionar</option>
                                <option value="Si">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-8 col-xs-offset-1">¿Recomienda que el trabajo sea presentado el día del evento?</label>
                        <div class="col-xs-2">
                            <select class="form-control input-sm" name="veredicto" id="veredicto">
                                <option value="">Seleccionar</option>
                                <option value="Si">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>-->
                    <div class="form-group">
                        <label class="col-xs-1 col-xs-offset-1 control-label">Comentarios</label>
                        <div class="col-xs-8 col-xs-offset-1">
                            <textarea class="form-control" rows="3" id="comentario" name="comentario"></textarea>
                            <span class="help-block" style="color: #468847;">Ingrese aquí comentarios o anotaciones que tenga respecto al
                            trabajo de investigación revisado (No lo deje en blanco).</span>
                        </div>
                    </div>
                    <?php echo '<input type="text" hidden="" id="id_evaluador" name="id_evaluador" value="'.$_SESSION['id_evaluador'].'"/>';?>
                    <?php echo '<input type="text" hidden="" id="id_trabajo" name="id_trabajo" value="'.$trabajo.'"/>';?>
                    <div class="form-group">
                        <button name="cmdAction" class="col-xs-offset-1 btn btn-primary" type="submit" value="1">Enviar evaluaci&oacute;n</button>
                    </div>
                </form>
            </div>
        </div>

<?php include("resources/footer.php");?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="js/formValidation/formValidation.min.js"></script>
<script src="js/formValidation/bootstrap.min.js"></script>
<!--FormValidation rules-->
<script src="js/validationRules/evaluationRules.js"></script>
    </body>
</html>