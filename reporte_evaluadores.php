<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/19/2015
 * Time: 12:00 AM
 */
    session_start();
    require_once("resources/session_validation.php");
    require_once("resources/connection.php");
    require("resources/classes/evaluator.php");
    validate_group();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">

    <!-- Footable CSS File-->
    <link href="css/footable.core.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include("resources/navbar.php");?>

<div style="margin-right: 1em; margin-left: 1em;">
    <h2 style="text-align: center; margin-bottom: 24px">Reporte Evaluadores</h2>
    <p>A continuación puede encontrar la información de los participantes,
        así como las acciones necesarias para la administración del concurso.</p>
    <p>Descargar base de datos en Excel: <a href="resources/reportes.php?type=2" class="btn btn-xs btn-primary">Descargar</a></p>
</div>
<?php
    if(isset($_GET['error'])){
        include('resources/msg_loading.php');
        error_load('" style="margin-right: 1em; margin-left: 1em;', $_GET['error']);
    }
    if(isset($_GET['success'])){
        include('resources/msg_loading.php');
        success_load('" style="margin-right: 1em; margin-left: 1em;', $_GET['success']);
    }
?>
<div style="margin-right: 1em; margin-left: 1em;">
    <table class="table footable table-responsive table-bordered">
        <thead>
            <tr>
                <th>Nombre</th>
                <th data-hide="phone,tablet">Área</th>
                <th data-hide="phone">T. Asignados</th>
                <th data-hide="all">Titulo Academico</th>
                <th data-hide="all">Correo</th>
                <th data-hide="all">Localidad</th>
                <th data-hide="all">Institución</th>
                <th data-hide="all">Facultad</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php
            $conn = my_connection();
            $sql = "SELECT evaluadores.*, usuarios.*, area_conocimiento.nombre_area,
                      instituciones.nombre_institucion, municipios.nombre_municipio,
                      trabajos_asignados.num_trabajos FROM evaluadores
                    LEFT JOIN usuarios ON usuarios.correo = evaluadores.correo
                    LEFT JOIN area_conocimiento ON area_conocimiento.id_area = evaluadores.id_area
                    LEFT JOIN instituciones ON instituciones.id_institucion = evaluadores.id_institucion
                    LEFT JOIN modalidad ON modalidad.id_modalidad = evaluadores.id_modalidad
                    LEFT JOIN municipios ON municipios.id_municipio = evaluadores.id_municipio
                    LEFT JOIN trabajos_asignados ON trabajos_asignados.id_evaluador = evaluadores.id_evaluador
                    WHERE usuarios.tipo_usuario = 4 and usuarios.status = 'ON';";
            $resultado = $conn->query($sql);
            while($row = $resultado->fetch_assoc()) {
                $eval = row_to_evaluator($row);
                $button = '<div class="btn-group"><button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">Acciones <span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="enviar_mail.php?id='.$eval->id_usuario.'">Enviar Correo</a></li>
                                <li><a href="reporte_evaluador.php?id='.$eval->id_usuario.'">Reporte Personal</a></li>
                                <li><a href="control/evaluadorCTL.php?cmdAction=2&id='.$eval->id_usuario.'">Reenviar Confirmación</a></li>
                            '.(($_SESSION['tipo_usuario'] == 1)?'<li><a href="#confirmDialogD'.$eval->id_usuario.'" data-toggle="modal">Eliminar</a></li>':'').'
                        </ul></div>';
                echo "<tr>
                        <td>$eval->saludo $eval->nombre $eval->apellido_pat $eval->apellido_mat</td>
                        <td>$eval->area</td>
                        <td>$eval->num_trabajos</td>
                        <td>$eval->titulo_academico</td>
                        <td>$eval->correo</td>
                        <td>$eval->municipio, $eval->ciudad</td>
                        <td>$eval->institucion</td>
                        <td>$eval->facultad</td>
                        <td>$button</td>
                    </tr>";
                echo    '<!-- Modal HTML for Delete -->
                                                <div id="confirmDialogD'.$eval->id_usuario.'" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">Confirmaci&oacute;n</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Esta seguro de eliminar este registro?</p>
                                                                <p class="text-warning"><small>&Eacute;ste se perder&aacute; para siempre si procede...</small></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                                <a class="btn btn-primary" href="control/coordinacionCTL.php?cmdAction=3&id='.$eval->id_usuario.'">Eliminar</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>';
            }

        ?>
        </tbody>
        <tfoot></tfoot>
    </table>
</div>

<?php include("resources/footer.php");?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="js/footable.min.js"
<script src="js/footable.all.min.js" type="text/javascript"></script>
<script type="text/javascript">$(function(){$('.footable').footable();});</script>
</body>
</html>

<?php
    function row_to_evaluator($row){
        $evaluador = new evaluator();
        $evaluador->set_id_evaluador($row['id_evaluador']);
        $evaluador->set_id_usuario($row['id_usuario']);
        $evaluador->set_nombre($row['nombre']);
        $evaluador->set_apellido_pat($row['apellido_pat']);
        $evaluador->set_apellido_mat($row['apellido_mat']);
        $evaluador->set_saludo($row['abreviatura_titulo']);
        $evaluador->set_titulo_academico($row['titulo']);
        $evaluador->set_area($row['nombre_area']);
        $evaluador->set_correo($row['correo']);
        $evaluador->set_municipio($row['nombre_municipio']);
        $evaluador->set_ciudad($row['ciudad']);
        $evaluador->set_institucion($row['nombre_institucion']);
        $evaluador->set_facultad($row['facultad']);
        $evaluador->set_num_trabajos($row['num_trabajos']);
        return $evaluador;
    }
?>