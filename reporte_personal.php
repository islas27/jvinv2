<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/18/2015
 * Time: 10:49 PM
 */
session_start();
require_once("resources/session_validation.php");
validate_session();
require("resources/connection.php");
$id_participante = $_SESSION['id_participante'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <?php include("resources/navbar.php");?>
    <?php include("resources/carrusel.php");?>
    <div class="col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1">
        <h2 style="text-align: center; margin-bottom: 24px">Mi Información</h2>
    </div>
    <?php
    if(isset($_GET['error'])){
        include('resources/msg_loading.php');
        error_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
    }
    if((isset($_GET['success'])) or ($_SESSION['trabajo_subido'] == true)){
        include('resources/msg_loading.php');
        success_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", 3);
    }
    ?>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
        <?php
        $query = "select * from participantes
                    LEFT JOIN instituciones on instituciones.id_institucion = participantes.id_institucion
                    LEFT JOIN municipios ON municipios.id_municipio = participantes.id_municipio
                    where id_participante = $id_participante";
        $conn = my_connection();
        $resultado = $conn->query($query);
        $registro = $resultado->fetch_assoc();
        $estatus = ($registro['es_estudiante'] == 1)?'Estudiante con '.$registro['semestre'].' semestres'
            :'Egresado con '.$registro['tiempo_egresado'].' meses de haber egresado';
        $folio = $registro['folio'];
        $cuerpo = 'Esta es la información que usted ha registrado para su participación:<br>
                <br>Informaci&oacute;n Personal:
                <br>Nombre: '.$_SESSION['name']." ".$_SESSION['apellido_mat'].
            '<br>Domicilio: '.$registro['calle'].' #'.$registro['num_ext'].', '.$registro['colonia'].', CP '.$registro['codigo_postal'].
            '<br>Localidad: '.$registro['localidad'].', '.$registro['nombre_municipio'].
            '<br>Teléfonos: '.$registro['telefono_casa'].', '.$registro['telefono_cel'].
            '<br>Correo electrónico: '.$_SESSION['correo'].
            '<br>Institución: '.$registro['nombre_institucion'].
            '<br>Facultad, Instituto o Escuela: '.$registro['facultad'].
            '<br>Carrera: '.$registro['carrera'].
            '<br>Estatus: '.$estatus.'<br>';
        if($_SESSION['trabajo_registrado']){
            $query = "select * from trabajos
                        LEFT JOIN instituciones ON instituciones.id_institucion = trabajos.id_institucion_asesor
                        LEFT JOIN area_conocimiento ON area_conocimiento.id_area = trabajos.id_area
                        LEFT JOIN modalidad ON modalidad.id_modalidad = trabajos.id_modalidad
                        where id_participante = $id_participante";
            $resultado = $conn->query($query);
            $registro = $resultado->fetch_assoc();
            $cuerpo .= '<br>Información del trabajo:'.
                '<br>Título: <i>"'.$registro['titulo'].'"</i>'.
                '<br>Área de conocimiento: '.$registro['nombre_area'].
                '<br>Modalidad: '.$registro['nombre_modalidad'].
                '<br>Colaboradores: '.$registro['colaborador_1'].' '.$registro['colaborador_2'].' '.$registro['colaborador_3'].
                '<br><br>Información del Asesor:'.
                '<br>Profesor Asesor: '.$registro['grado_academico'].' '.$registro['nombre_asesor'].' '.$registro['apellidos_asesor'].
                '<br>Institución: '.$registro['nombre_institucion'].
                '<br>Facultad, Instituto o Escuela: '.$registro['facultad_asesor'].
                '<br>Correo electrónico: '.$registro['correo_asesor'].'<br>';
        }
        if($_SESSION['trabajo_subido']){
            $cuerpo .= '<br>A continuaci&oacuten puede ver en los siguientes enlaces los archivos que ha enviado:'.
                '<br><br>Solicitud de Participación: <a href="../'.$registro['ubic_solicitud'].'">Descargar</a>'.
                '<br>Trabajo: <a href="../'.$registro['ubic_trabajo'].'">Descargar</a>'.
                '<br>Credencial de Elector: <a href="../'.$registro['ubic_credencial'].'">Descargar</a>'.
                '<br>CURP: <a href="../'.$registro['ubic_curp'].'">Descargar</a>'.
                '<br><br>Folio de Inscripción: <b style="color:#D45F4A">'.$folio.'</b><br>'.
                '<br><br>Si usted nota que alguna parte de esta información esta incorrecta, favor de contactar a la coordinación.<br>
                            <br>Atte.<br> Jonathan Islas, Administrador de la Plataforma.<br><br>';
        }
        echo '<div style="margin-top: 1em; margin-bottom: 1em;">'.$cuerpo.'</div>';
        ?>
    </div>

    <?php include("resources/footer.php");?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>