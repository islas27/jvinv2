<?php
if(isset($_GET['log_off'])){
    session_start();
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }
    session_destroy();
    header("Location: index.php");
}
session_start();
if($_SESSION['autenticacion'] === true){
    header("Location: inicio.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include("resources/navbar.php");?>
<?php include("resources/carrusel.php");?>
    <div class="col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1">
        <h2 style="text-align: center; margin-bottom: 24px">Iniciar sesión</h2>
    </div>
    <?php
        if(isset($_GET['error'])){
            include('resources/msg_loading.php');
            error_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
        }
        if(isset($_GET['success'])){
            include('resources/msg_loading.php');
            success_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['success']);
        }
    ?>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
        <div class="col-md-6 col-xs-12">
            <br>
            <legend><span class="glyphicon glyphicon-log-in"></span>&nbsp; &nbsp;Registro para los jóvenes participantes</legend>
            <div class="form-group col-sm-12">
                <p>1. Descargar y revisar la convocatoria, asi como los formatos de inscripción:</p>
                <div class="col-xs-12"><a href="#" class="btn btn-block btn-xs btn-primary"
                                          data-toggle="modal" data-target="#descarga-documentos"
                        style="margin-bottom: 10px;">
                        Descargar convocatoria y formatos</a></div>
                <p>2. Documentos a preparar para el registro (deberán digitalizarse.
                        Tamaño máximo de cada archivo: 2MB):</p>
                <ol style="list-style:lower-alpha">
                    <li>Documento de acuerdo a la modalidad de participación.</li>
                    <li>Formato de Solicitud</li>
                    <li>CURP y Credencial de Elector</li>
                </ol>
                <p>3. Crear un usuario usando el formulario de registro:</p>
                <div class="col-xs-12">
                    <a href="registro_participante.php" class="btn btn-block btn-xs btn-primary">¡Registrarse!</a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <form name="login" id="login" action="control/loginCTL.php" method="post" class="form-horizontal">
                <br>
                <legend><span class="glyphicon glyphicon-flag"></span>&nbsp; &nbsp;Inicio de sesión</legend>
                <div class="form-group col-sm-12">
                    <label for="correo" class="control-label">Correo</label>
                    <input class="form-control" id="correo" type="email" name="correo">
                </div>
                <div class="form-group col-sm-12">
                    <label for="password" class="control-label">Contraseña</label>
                    <input class="form-control" id="password" type="password" name="password">
                </div>
                <div class="form-group col-sm-12">
                    <button type="submit" name="cmdAction" value="0"
                            class="btn btn-primary btn-sm col-xs-4" style="margin-top: 15px; margin-bottom: 15px;">Entrar</button>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="descarga-documentos" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Descarga de documentos</h4>
                </div>
                <div class="modal-body">
                    <p>Convocatoria Jovenes Investigadores 2015</p>
                    <a href="../pdf/convocatoria_3er._encuentro_de_jovenes_investigadores_de_ies_del_estado_de_chihuahua_2015.pdf"
                       class="btn btn-block btn-xs btn-primary">Descargar</a>
                    <p style="margin-top: 10px;">Formato según el estilo de presentación</p>
                    <div class="btn-group btn-group-justified btn-group-xs">
                        <a href="../pdf/formato_para_presentaciones_orales.pdf" class="btn btn-default btn-primary">
                            Presentación Oral</a>
                        <a href="../pdf/formato_para_presentaciones_en_cartel.pdf" class="btn btn-default btn-primary">
                            Presentación con Cartel</a>
                    </div>
                    <p style="margin-top: 10px;">Solicitud de participación</p>
                    <a href="../pdf/solicitud_de_participacion_en_el 3er._encuentro_estatal_de_jovenes_investigadores.doc"
                       class="btn btn-block btn-xs btn-primary">Descargar</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-xs" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <?php include("resources/footer.php");?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!-- FormValidation plugin and the class supports validating Bootstrap form -->
    <script src="js/formValidation/formValidation.min.js"></script>
    <script src="js/formValidation/bootstrap.min.js"></script>
    <!--FormValidation rules-->
    <script src="js/validationRules/loginRules.js"></script>
</body>
</html>