<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/19/2015
 * Time: 12:01 AM
 */
session_start();
require_once("resources/session_validation.php");
require_once("resources/connection.php");
validate_group();
function echo_val($field, $r){
    if(isset($_GET['id'])){
        echo 'value="'.$r[$field].'"';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">

    <!-- Footable CSS File-->
    <link href="css/footable.core.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include("resources/navbar.php");?>

<div class="col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1">
    <h2 style="text-align: center; margin-bottom: 24px">Listado del Personal de Coordinación </h2>
</div>
<?php
    if(isset($_GET['error'])){
        include('resources/msg_loading.php');
        error_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
    }
    if(isset($_GET['success'])){
        include('resources/msg_loading.php');
        success_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['success']);
    }
?>
<div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
    <div class="col-md-6 col-xs-12">
        <br>
        <legend><span class="glyphicon glyphicon-log-in"></span>&nbsp; &nbsp;Personal</legend>
        <table class="table table-bordered table-responsive footable">
            <thead>
                <tr>
                    <td>Nombre</td>
                    <td>Rol</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
            <?php
            $conn = my_connection();
            if(isset($_GET['id'])){
                $x = $_GET['id'];
                $sql = "select * from usuarios where id_usuario= $x";
                $resultado = $conn->query($sql);
                $r = $resultado->fetch_assoc();
            }else{
                $r = "";
            }
            $sql = "select id_usuario, tipo_usuario, concat_ws(' ', nombre, apellido_pat, apellido_mat) as name, descripcion from usuarios
              LEFT JOIN tipos_usuario ON tipos_usuario.id_tipos_usuario = usuarios.tipo_usuario
              where tipo_usuario < 4 and status = 'ON';";
            $resultado = $conn->query($sql);
            while($row = $resultado->fetch_assoc()){
                echo "<tr>
                        <td>".$row['name']."</td>
                        <td>".$row['descripcion']."</td>".
                    '<td>
                        <div class="btn-group"><button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">Acciones <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="enviar_mail.php?id='.$row['id_usuario'].'">Enviar Correo</a></li>
                            '.(($_SESSION['tipo_usuario'] == 1 && $row['tipo_usuario'] != 1)?
                        '<li><a href="#confirmDialogD'.$row['id_usuario'].'" data-toggle="modal">Eliminar</a></li>'.
                        '<li><a href="reporte_coordinacion.php?id='.$row['id_usuario'].'" data-toggle="modal">Editar</a></li>'
                        :'').'
                        </ul></div>
                    </td>'.
                    "</tr>";
                echo    '<div id="confirmDialogD'.$row['id_usuario'].'" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Confirmaci&oacute;n</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Esta seguro de eliminar este registro?</p>
                                        <p class="text-warning"><small>&Eacute;ste se perder&aacute; para siempre si procede...</small></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <a class="btn btn-primary" href="control/coordinacionCTL.php?cmdAction=4&id='.$row['id_usuario'].'">Eliminar</a>
                                    </div>
                                </div>
                            </div>
                        </div>';
            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-6 col-xs-12">
        <br>
        <legend><span class="glyphicon glyphicon-flag"></span>&nbsp; &nbsp;Registrar Nuevo Personal</legend>
        <form id="coord" name="coord" method="post" action="control/coordinacionCTL.php">
            <?php
                echo ($_SESSION['tipo_usuario'] != 1)? '<fieldset id="form" disabled="disabled">': '<fieldset id="form">';
            ?>
                <div class="form-group">
                    <label for="nombre" class="control-label">Nombre</label>
                    <input type="text" name="nombre" id="nombre" class="form-control input-sm" <?php echo_val('nombre', $r);?>/>
                </div>
                <div class="form-group">
                    <label for="apellido_pat" class="control-label">Apellido Paterno</label>
                    <input type="text" name="apellido_pat" id="apellido_pat" class="form-control input-sm" <?php echo_val('apellido_pat', $r);?>/>
                </div>
                <div class="form-group">
                    <label for="apellido_mat" class="control-label">Apellido Materno</label>
                    <input type="text" name="apellido_mat" id="apellido_mat" class="form-control input-sm" <?php echo_val('apellido_mat', $r);?>/>
                </div>
                <div class="form-group">
                    <label for="correo" class="control-label">Correo Electronico</label>
                    <input type="text" name="correo" id="correo" class="form-control input-sm"<?php echo_val('correo', $r);?>/>
                </div>
                <div class="form-group">
                    <label for="rol" class="control-label">Rol</label>
                    <select name="rol" id="rol" class="form-control input-sm selectContainer">
                        <option value="">Seleccionar</option>
                        <option value="2">Capturista</option>
                        <option value="3">Visor</option>
                    </select>
                </div>
                <div>
                    <input type="submit" name="cmdAction" value="Registrar/Modificar"
                           class="btn btn-primary btn-sm col-xs-offset-3 col-xs-6" style="margin-bottom: 1em;"/>
                </div>
            </fieldset>
        </form>
    </div>
</div>

<?php include("resources/footer.php");?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="js/formValidation/formValidation.min.js"></script>
<script src="js/formValidation/bootstrap.min.js"></script>
<!--FormValidation rules-->
<script src="js/validationRules/personalCoordRules.js"></script>
<script src="js/footable.min.js"
<script src="js/footable.all.min.js" type="text/javascript"></script>
<script type="text/javascript">$(function(){$('.footable').footable();});</script>
</body>
</html>