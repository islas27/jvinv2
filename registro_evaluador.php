<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/18/2015
 * Time: 11:53 PM
 */
    session_start();
    require_once("resources/session_validation.php");
require_once("resources/connection.php");
    validate_session();
    validate_group();
    $conexion = my_connection();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <?php include("resources/navbar.php");?>
    <div class="col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1">
        <h2 style="text-align: center; margin-bottom: 24px">Registro Evaluadores</h2>
    </div>
    <?php
        if(isset($_GET['error'])){
            include('resources/msg_loading.php');
            error_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
        }
        if(isset($_GET['success'])){
            include('resources/msg_loading.php');
            success_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['success']);
        }
    ?>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
        <form name="registro" id="registro" class="form-horizontal" action="control/evaluadorCTL.php" method="post"><br>
        <div class="col-md-6 col-xs-12">
            <legend><span class="glyphicon glyphicon-user"></span>&nbsp; &nbsp;Datos Personales</legend>
            <div class="form-group">
                <label class="control-label col-md-4" for="saludo">Saludo:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control input-sm" name="saludo" id="saludo" placeholder="Dr., Sr., etc."/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="titulo_academico">Titulo:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control input-sm" name="titulo_academico" id="titulo_academico" placeholder="Grado y Especialidad"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="nombre">Nombre:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control input-sm" id="nombre" name="nombre"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="apellido_pat">Apellido Pat:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control input-sm" id="apellido_pat" name="apellido_pat"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="apellido_mat">Apellido Mat:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control input-sm" id="apellido_mat" name="apellido_mat"/>
                </div>
            </div>
            <legend><span class="glyphicon glyphicon-home"></span>&nbsp; &nbsp;Localidad:</legend>
            <div class="form-group">
                <label class="control-label col-md-4" for="localidad">Ciudad:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control input-sm" id="localidad" name="localidad"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="municipio">Municipio:</label>
                <div class="col-md-8 selectContainer">
                    <select class="form-control input-sm" id="municipio" name="municipio">
                        <option value="">Seleccionar</option>
                        <?php
                        $query = 'select id_municipio, nombre_municipio from municipios;';
                        $resultado = $conexion->query($query);
                        $resultado->data_seek(0);
                        while($registro = $resultado->fetch_assoc()){
                            echo '<option value="'.$registro['id_municipio'].'">'.$registro['nombre_municipio'].'</option>';
                        }
                        $resultado->free();
                        ?>
                    </select>
                </div>
            </div>
            <legend><span class="glyphicon glyphicon-phone-alt"></span>&nbsp; &nbsp;Contacto</legend>
            <div class="form-group">
                <label class="control-label col-md-4" for="telefono_casa">Teléfono:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control input-sm" id="telefono_casa" name="telefono_casa"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" for="telefono_cel">Celular:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control input-sm" id="telefono_cel" name="telefono_cel"/>
                </div>
            </div>
            <legend><span class="glyphicon glyphicon-envelope"></span>&nbsp; &nbsp;Datos de Usuario</legend>
            <div class="form-group">
                <label class="control-label col-md-4" for="correo">Correo:</label>
                <div class="col-md-8">
                    <input type="email" class="form-control input-sm" id="correo" name="correo" placeholder="Correo Electrónico"/>
                    <p>Se le enviara de manera automatica la contraseña de acceso al evaluador.</p>
                </div>
            </div>
            <legend><span class="glyphicon glyphicon-book"></span>&nbsp; &nbsp;Institución Academica</legend>
            <div class="form-group selectContainer">
                <label for="institucion" class="control-label col-md-4">Institución:</label>
                <div class="col-md-8">
                    <select class="form-control input-sm" id="institucion" name="institucion">
                        <option value="">Seleccionar</option>
                        <?php
                        $query = 'select id_institucion, nombre_institucion from instituciones;';
                        $resultado = $conexion->query($query);
                        $resultado->data_seek(0);
                        while($registro = $resultado->fetch_assoc()){
                            echo '<option value="'.$registro['id_institucion'].'">'
                                .$registro['nombre_institucion'].'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="facultad" class="control-label col-md-4">Facultad:</label>
                <div class="col-md-8">
                    <input type="text" class="form-control input-sm" id="facultad" name="facultad"/>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <legend><span class="glyphicon glyphicon-education"></span>&nbsp; &nbsp;Distinciones como Investigador</legend>
            <div class="col-xs-12">
                <div class="input-group form-group">
                    <label for="esSNI" class="form-control">¿Es miembro del S.N.I.?</label>
                    <span class="input-group-addon"><input type="checkbox" name="esSNI" id="esSNI"></span>
                </div><!-- /input-group -->
                <div class="form-group">
                    <label for="nivelSNI">Seleccione el nivel:</label>
                    <select class="form-control input-sm" id="nivelSNI" name="nivelSNI">
                        <option value="Sin Asignar" selected="">Sin Asignar</option>
                        <option value="Candidato">Candidato</option>
                        <option value="Nivel I">Nivel I</option>
                        <option value="Nivel II">Nivel II</option>
                        <option value="Nivel III">Nivel III</option>
                    </select>
                </div>
                <div class="input-group form-group">
                    <label for="esPROMEP" class="form-control">¿Cuenta con Perfil PROMEP?</label>
                    <span class="input-group-addon"><input type="checkbox" name="esPROMEP" id="esPROMEP"></span>
                </div><!-- /input-group -->
                <div class="form-group">
                    <label for="cuerpoAcademico">¿Es miembro de un CA de la UACH?</label>
                    <select class="form-control input-sm " id="cuerpoAcademico" name="cuerpoAcademico">
                        <option value="No" selected="">No es miembro</option>
                        <option value="Integrante">Integrante</option>
                        <option value="Colaborador">Colaborador</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="claveCA">Clave del Cuerpo Académico</label>
                    <div class="input-group input-group-sm" id="claveCA">
                        <span class="input-group-addon">UACHIH-CA-</span>
                        <input type="text" class="form-control" id="claveCA" name="claveCA">
                    </div>
                </div>
                <div class="form-group" id="con">
                    <label for="esConsolidado">Nivel de consolidación</label>
                    <select class="form-control input-sm " id="esConsolidado" name="esConsolidado">
                        <option value="No" selected="">Sin Consolidar</option>
                        <option value="Consolidado">Consolidado</option>
                        <option value="En Consolidación">En Consolidación</option>
                        <option value="En Formación">En Formación</option>
                    </select>
                </div>
            </div>
            <legend><span class="glyphicon glyphicon-edit"></span>&nbsp; &nbsp;Sistema de Evaluación</legend>
            <div class="form-group selectContainer">
                <label for="area" class="control-label col-md-4">Area:</label>
                <div class="col-md-8">
                    <select class="form-control input-sm" id="area" name="area" required="required">
                        <option value="">Seleccionar</option>
                        <?php
                        $query = 'select id_area, nombre_area from area_conocimiento;';
                        $resultado = $conexion->query($query);
                        $resultado->data_seek(0);
                        while($registro = $resultado->fetch_assoc()){
                            echo '<option value="'.$registro['id_area'].'">'
                                .$registro['nombre_area'].'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group selectContainer">
                <label for="modalidad" class="control-label col-md-4">Modalidad</label>
                <div class="col-md-8">
                    <select class="form-control input-sm" id="modalidad" name="modalidad" required="required">
                        <option value="">Seleccionar</option>
                        <?php
                        $query = 'select id_modalidad, nombre_modalidad from modalidad;';
                        $resultado = $conexion->query($query);
                        $resultado->data_seek(0);
                        while($registro = $resultado->fetch_assoc()){
                            echo '<option value="'.$registro['id_modalidad'].'">'
                                .$registro['nombre_modalidad'].'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <button name="cmdAction" type="submit" value="0"  style="margin-bottom: 24px; margin-top: " class="btn btn-primary col-xs-offset-3 col-xs-6">Siguiente</button>
            </div>
        </div>
        </form>
    </div>

    <?php include("resources/footer.php");?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!-- FormValidation plugin and the class supports validating Bootstrap form -->
    <script src="js/formValidation/formValidation.min.js"></script>
    <script src="js/formValidation/bootstrap.min.js"></script>
    <!--FormValidation rules-->
    <script src="js/validationRules/evaluadorRules.js"></script>
    <script type="text/javascript">
        $(window).load(function(){
            $('#con').hide();
            $('#claveCA').parent().hide();
            $('#nivelSNI').parent().hide();
        });
        $('#esSNI').click(function(){
            if($("#esSNI").is(':checked'))
                $('#nivelSNI').parent().show();
            else{
                $('#nivelSNI').parent().hide();
                $('#nivelSNI').val('Sin Asignar');
            }
        });
        $('#cuerpoAcademico').click(function(){
            if($( "#cuerpoAcademico option:selected").val() != 'No'){
                $('#claveCA').parent().show();
                $('#con').show();
            }else{
                $('#claveCA').parent().hide();
                $('#con').hide();
                $('#claveCA').val('Sin Asignar');
                $('#esConsolidado').val('No');
            }
        });
    </script>
</body>
</html>