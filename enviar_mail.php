<?php
session_start();
require_once("resources/session_validation.php");
require_once("resources/connection.php");
require("resources/classes/contestant.php");
validate_group();

if(isset($_GET['id'])){
    $conexion = my_connection();
    $query = 'select nombre,apellido_pat,correo from usuarios where id_usuario='.$_GET['id'];
    if($resultado = $conexion->query($query)){
        $registro = $resultado->fetch_assoc();
        $correo = $registro['correo'];
        $nombre = $registro['nombre'].' '.$registro['apellido_pat'];
    }
}
if(isset($_GET['id_e'])){
    $conexion = my_connection();
    $query = 'select abreviatura_titulo,nombre,apellido_pat,evaluadores.correo from evaluadores
      LEFT JOIN usuarios ON usuarios.correo = evaluadores.correo
      where id_evaluador='.$_GET['id_e'];
    if($resultado = $conexion->query($query)){
        $registro = $resultado->fetch_assoc();
        $correo = $registro['correo'];
        $nombre = $registro['abreviatura_titulo'].' '.$registro['nombre'].' '.$registro['apellido_pat'];
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">
    <!--Font Awesome-->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- include summernote css/js-->
    <link href="css/summernote.css" rel="stylesheet">
    <link href="css/summernote-bs3.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php
include("resources/navbar.php");
include("resources/carrusel.php");
?>
<div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1">
    <h2 style="text-align: center; margin-bottom: 24px">Enviar Correo</h2>
</div>
<?php
if(isset($_GET['error'])){
    include('resources/msg_loading.php');
    error_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
}
if(isset($_GET['success'])){
    include('resources/msg_loading.php');
    success_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['success']);
}
?>
<div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel"><!--Contenido-->
    <div class="row"><!--Texto Ayuda-->
        <p style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">A continuación puede redactar los correos, asi como escribir las direcciones a las cuales se dirijen.</p>
    </div>
    <form id="summernoteForm" method="post" class="form-horizontal" action="control/coordinacionCTL.php">
        <div class="row"><!--Formulario-->
            <div class="form-group">
                <label class="col-md-2 control-label">Correo(s)</label>
                <div class="col-md-5" id="correo">
                    <?php
                    if(isset($_GET['id']) || isset($_GET['id_e'])){
                        echo '<input type="text" class="form-control" value="'.$correo.'" name="correo"/>';
                        echo '<input type="text" id="nombre" class="form-control" value="'.$nombre.'" name="nombre"/>';
                    }else{
                        echo '<input type="text" class="form-control" name="correo"/>';
                    }
                    ?>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <label for="aTodos" class="form-control">¿Enviar a un grupo?</label>
                        <span class="input-group-addon"><input disabled="disabled" type="checkbox" name="aTodos" id="aTodos"></span>
                    </div><!-- /input-group -->
                </div>
                <div class="col-md-5 selectContainer">
                    <select class="form-control" id="grupo" name="grupo" disabled="disabled">
                        <option>Seleccionar</option>
                        <option value="1">Participantes</option>
                        <option value="2">Participantes sin archivos</option>
                        <option value="3">Participantes con archivos</option>
                        <option value="4">Evaluadores</option>
                        <option value="5">Participantes Aceptados</option>
                        <option value="6">Participantes Rechzados</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Asunto</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="asunto"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Contenido</label>
                <div class="col-md-9">
                    <textarea class="form-control" id="content" name="mensaje" style="height: 400px;"></textarea>
                </div>
            </div>
        </div>
        <div class="row"><!--Boton-->
            <div class="form-group" style="padding-top: 25px; padding-bottom: 5px;">
                <button type="submit" name="cmdAction" value="5" class="btn btn-primary col-sm-4 col-sm-offset-4">Enviar</button>
            </div>
        </div>
    </form>
</div>
<?php include("resources/footer.php");?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="js/formValidation/formValidation.min.js"></script>
<script src="js/formValidation/bootstrap.min.js"></script>
<!--FormValidation rules-->
<script src="js/validationRules/contestantRules.js"></script>
<!--Summernote script-->
<script src="js/summernote.min.js"></script>
<script>
    $(document).ready(function() {
        function validateEditor() {
            // Revalidate the content when its value is changed by Summernote
            $('#summernoteForm').formValidation('revalidateField', 'content');
        }

        $('#summernoteForm')
            .formValidation({
                framework: 'bootstrap',
                excluded: [':disabled'],
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    correo: {
                        validators: {
                            notEmpty: {
                                message: 'Ingrese un correo válido'
                            },emailAddress: {
                                message: 'Ingrese un correo válido'
                            }
                        }
                    }, asunto: {
                        validators: {
                            notEmpty: {
                                message: 'Escriba el asunto a tratar'
                            }
                        }
                    }, mensaje: {
                        validators: {
                            callback: {
                                message: 'Es necesario escribir un mensaje para el correo',
                                callback: function(value, validator, $field) {
                                    var code = $('[name="content"]').code();
                                    // <p><br></p> is code generated by Summernote for empty content
                                    return (code !== '' && code !== '<p><br></p>');
                                }
                            }
                        }
                    }
                }
            })
            .find('[name="mensaje"]')
            .summernote({
                height: 400,
                onkeyup: function() {
                    validateEditor();
                },
                onpaste: function() {
                    validateEditor();
                },
                toolbar: [
                    ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link']]
                ]
            })
            .end();
    });
</script>
<script type="text/javascript">
    $(window).load(function(){
        $('#grupo').hide();
        $('#nombre').hide();
    });
    $('#aTodos').click(function(){
        if($("#aTodos").is(':checked')){
            $('#grupo').show();
            $('#correo').hide();
            validateGroup();
        }else{
            $('#grupo').hide();
            $('#correo').show();
            validateGroup();
        }
    });
</script>
</body>
</html>