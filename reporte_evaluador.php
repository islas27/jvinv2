<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 5/14/2015
 * Time: 12:22 AM
 */

    session_start();
    require_once("resources/session_validation.php");
    validate_session();
    require("resources/connection.php");
    if($_SESSION['tipo_usuario'] <= 3){
        $id = $_GET['id'];
        $query = "select * from usuarios
                LEFT JOIN evaluadores on usuarios.correo = evaluadores.correo
                left join instituciones on evaluadores.id_institucion = instituciones.id_institucion
                left join area_conocimiento on evaluadores.id_area = area_conocimiento.id_area
                left join municipios on evaluadores.id_municipio = municipios.id_municipio
                where usuarios.id_usuario=$id;";
        $titulo_pag = "Reporte Evaluador";
        $mensaje_ini = "Esta es la informaci&oacute;n que tiene registrada dentro de la plataforma:";
        $msg_extra = "";
    }else{
        $id_evaluador = $_SESSION['id_evaluador'];
        $correo = $_SESSION['correo'];
        $query = "select * from usuarios
                LEFT JOIN evaluadores on usuarios.correo = evaluadores.correo
                left join instituciones on evaluadores.id_institucion = instituciones.id_institucion
                left join area_conocimiento on evaluadores.id_area = area_conocimiento.id_area
                left join municipios on evaluadores.id_municipio = municipios.id_municipio
                where usuarios.correo='$correo';";
        $titulo_pag = "Mi Información";
        $mensaje_ini = "Esta es la informaci&oacute;n que usted tiene registrada dentro de la plataforma:";
        $msg_extra = "<br>Si usted nota que alguna parte de esta informaci&oacute;n esta incorrecta, incompleta as&iacute;
                como un cambio reciente en alg&uacute;n dato, contacte a la coordinaci&oacute;n, y se le informar&aacute; de que puede proceder.<br>
                <br>Atte.<br> Jonathan Islas<br>Administrador de la Plataforma.<br><br>";
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <?php include("resources/navbar.php");?>
    <?php include("resources/carrusel.php");?>
    <div class="col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1">
        <h2 style="text-align: center; margin-bottom: 24px"><?php echo $titulo_pag?></h2>
    </div>
    <?php
        if(isset($_GET['error'])){
            include('resources/msg_loading.php');
            error_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
        }
        if(isset($_GET['success'])){
            include('resources/msg_loading.php');
            success_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['success']);
    }
    ?>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
        <?php
        $conexion = my_connection();

        $resultado = $conexion->query($query);
        $registro = $resultado->fetch_assoc();
        $claveCA = ($registro['clave_ca'] == null)?'No cuenta con clave':'UACHIH-CA-'.$registro['clave_ca'];
        $cuerpo = '<br>'.$mensaje_ini.'<br>
                <br>Informaci&oacute;n Personal:
                <br>Nombre: '.$registro['abreviatura_titulo'].' '.$registro['nombre'].' '.$registro['apellido_pat'].' '.$registro['apellido_mat'].
            '<br>Localidad: '.$registro['ciudad'].', '.$registro['nombre_municipio'].
            '<br>Tel&eacute;fonos: '.$registro['telefono_casa'].', '.$registro['telefono_cel'].
            '<br>Correo electr&oacute;nico: '.$registro['correo'].
            '<br>Titulo Academico: '.$registro['titulo'].
            '<br>Instituci&oacute;n: '.$registro['nombre_institucion'].
            '<br>Facultad, Instituto o Escuela: '.$registro['facultad'].
            '<br>&Aacute;rea de conocimiento: '.$registro['nombre_area'].
            '<br><br>Distinciones como investigador:'.
            '<br>Miembro del S.N.I: '.$registro['es_sni'].
            '<br>Nivel: '.$registro['nivel_sni'].
            '<br>Perfil PROMEP: '.$registro['es_promep'].
            '<br>Miembro de alg&uacute;n cuerpo academico de la UACH: '.$registro['cuerpo_academico'].
            '<br>Clave: '.$claveCA.
            '<br>'.$msg_extra;
        echo $cuerpo;
        if($_SESSION['tipo_usuario'] <= 3){
            $id_evaluador = $registro['id_evaluador'];
            $sql = "select * from evaluaciones
                                    left join trabajos on trabajos.id_trabajo = evaluaciones.id_trabajo
                                    LEFT JOIN participantes on participantes.id_participante = trabajos.id_participante
                                    LEFT JOIN usuarios ON usuarios.correo = participantes.correo
                                    where evaluaciones.id_evaluador = $id_evaluador;";
            $resultado = $conexion->query($sql);
            if($resultado->num_rows > 0){
                echo "<h3>Trabajos Asignados</h3>";
                echo "<table class='table table-responsive table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Título</th>
                                <th>Autor</th>
                                <th>Calif. Total</th>
                                <th>Fecha Evaluación</th>
                            </tr>
                        </thead>
                        <tbody>";
                $x = 1;
                while($row = $resultado->fetch_assoc()){
                    $autor = $row['nombre'].' '.$row['apellido_pat'];
                    $titulo = $row['titulo'];
                    if(is_null($row['calif_total'])){
                        $calificacion = 'Sin Calificar';
                    }else{
                        $calificacion = $row['calif_total'];
                    }
                    $fecha = $row['fecha_evaluacion'];
                    echo "<tr>
                                <td>$x</td>
                                <td>$titulo</td>
                                <td>$autor</td>
                                <td>$calificacion</td>
                                <td>$fecha</td>
                            </tr>";
                    $x++;
                }
echo "</tbody>
</table>";
            }
        }
        ?>
    </div>
    <?php include("resources/footer.php");?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>