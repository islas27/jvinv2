<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 5/14/2015
 * Time: 12:22 AM
 */

    session_start();
    require_once("resources/session_validation.php");
    require_once("resources/connection.php");
    validate_usertype(4);
    $evaluador = $_SESSION['id_evaluador'];
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Jóvenes Investigadores</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-custom.css" rel="stylesheet">

        <!-- FormValidation CSS file -->
        <link href="css/formValidation.min.css" rel="stylesheet">

        <!-- Footable CSS File-->
        <link href="css/footable.core.min.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php
            include("resources/navbar.php");
            include("resources/carrusel.php");
        ?>

        <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1">
            <h2 style="text-align: center; margin-bottom: 24px">Trabajos por Revisar</h2>
        </div>
        <?php
        if(isset($_GET['error'])){
            include('resources/msg_loading.php');
            error_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['error']);
        }
        if(isset($_GET['success'])){
            include('resources/msg_loading.php');
            success_load("col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1", $_GET['success']);
        }
        ?>
        <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
            <!--Asi como usamos col para el espacio horizontal, tenemos row para el espacio vertical-->
            <div class="row" style="padding-left: 15px; padding-top: 15px">
                En la siguiente tabla puede encontrar el listado de trabajos que le han sido
                asignados para su revisión. Si considera que alguno de estos trabajos son inadecuados,
                favor de reportarlo a la coordinación.<br><br>
            </div>
            <div class="row" style="padding: 15px;">
                <table class="table table-hover table-condensed">
                    <thead>
                    <tr>
                        <th class="col-xs-3">Autor(a)</th>
                        <th class="col-xs-6">T&iacute;tulo</th>
                        <th>Calif.</th>
                        <!--<th>Veredicto</th>-->
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            $conn = my_connection();
                            $sql = "select * from evaluaciones
                                    left join trabajos on trabajos.id_trabajo = evaluaciones.id_trabajo
                                    LEFT JOIN participantes on participantes.id_participante = trabajos.id_participante
                                    LEFT JOIN usuarios ON usuarios.correo = participantes.correo
                                    where evaluaciones.id_evaluador = $evaluador;";
                            $resultado = $conn->query($sql);
                            while($row = $resultado->fetch_assoc()){
                                $autor = $row['nombre'].' '.$row['apellido_pat'];
                                $titulo = $row['titulo'];
                                if(is_null($row['calif_total'])){
                                    $calificacion = 'Sin Calificar';
                                    $calificado = false;
                                    $id_trabajo = $row['id_trabajo'];
                                }else{
                                    $calificacion = $row['calif_total'];
                                    $calificado = true;
                                    $id_trabajo = -1;
                                }
                                $button = ($calificado)? "<a class='btn btn-xs disabled' href='#' style='text-decoration-color: green'>Evaluado</a>":
                                                         "<a class='btn btn-primary btn-xs' href='evaluacion.php?id_trabajo=$id_trabajo'>Evaluar</a>";
                                echo "<tr>
                                            <td>$autor</td>
                                            <td>$titulo</td>
                                            <td>$calificacion</td>
                                            <td>$button</td>
                                        </tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>


        <?php include("resources/footer.php");?>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <!-- FormValidation plugin and the class supports validating Bootstrap form -->
        <script src="js/formValidation/formValidation.min.js"></script>
        <script src="js/formValidation/bootstrap.min.js"></script>
        <!--FormValidation rules-->
        <script src="js/validationRules/contestantRules.js"></script>
    </body>
</html>