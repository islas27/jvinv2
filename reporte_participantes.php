<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 4/19/2015
 * Time: 12:00 AM
 */
    session_start();
    require_once("resources/session_validation.php");
    require_once("resources/connection.php");
    require("resources/classes/contestant.php");
    validate_group();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jóvenes Investigadores</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-custom.css" rel="stylesheet">

    <!-- FormValidation CSS file -->
    <link href="css/formValidation.min.css" rel="stylesheet">

    <!-- Footable CSS File-->
    <link href="css/footable.core.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include("resources/navbar.php");?>

<div style="margin-right: 1em; margin-left: 1em;">
    <h2 style="text-align: center; margin-bottom: 24px">Reporte Participantes</h2>
    <p>A continuación puede encontrar la información de los participantes,
        así como las acciones necesarias para la administración del concurso.</p>
    <p>Descargar base de datos en Excel: <a href="resources/reportes.php?type=1" class="btn btn-xs btn-primary">Descargar</a></p>
    <p>Descargar base de datos de calificaciones en Excel: <a href="resources/reportes.php?type=3" class="btn btn-xs btn-primary">Descargar</a></p>
</div>
<?php
    if(isset($_GET['error'])){
        include('resources/msg_loading.php');
        error_load('" style="margin-right: 1em; margin-left: 1em;', $_GET['error']);
    }
    if(isset($_GET['success'])){
        include('resources/msg_loading.php');
        success_load('" style="margin-right: 1em; margin-left: 1em;', $_GET['success']);
    }
?>
<div style="margin-right: 1em; margin-left: 1em;">
    <table class="table footable table-responsive table-bordered">
        <thead>
            <tr>
                <th class="col-sm-2 col-xs-3"  data-sort-initial="true">Folio</th>
                <th>Nombre</th>
                <th data-hide="all">Institución</th>
                <th data-hide="all">Correo</th>
                <th data-hide="all">Área</th>
                <th>Título</th>
                <th data-hide="all">E. Previa</th>
                <th data-hide="all">Modalidad</th>
                <th data-hide="phone">Veredicto</th>
                <th data-hide="all">Evaluador #1</th>
                <th data-hide="all">Calificación</th>
                <th data-hide="all">Evaluador #2</th>
                <th data-hide="all">Calificación</th>
                <th data-hide="all">Evaluador #3</th>
                <th data-hide="all">Calificación</th>
                <th data-sort-ignore="true"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $conn = my_connection();
            $sql = "select * from participantes
    LEFT JOIN usuarios ON usuarios.correo = participantes.correo
	LEFT JOIN trabajos ON trabajos.id_participante = participantes.id_participante
    LEFT JOIN instituciones on instituciones.id_institucion = participantes.id_institucion
    LEFT JOIN modalidad ON modalidad.id_modalidad = trabajos.id_modalidad
    LEFT JOIN area_conocimiento ON area_conocimiento.id_area = trabajos.id_area
    WHERE usuarios.status = 'ON';";
            $resultado = $conn->query($sql);
            while($row = $resultado->fetch_assoc()){
                $cntt = row_to_participant($row, $conn);
                $revision = (!empty($row['folio']))?'<li><a href="reporte_participante.php?id='.$cntt->id_usuario.'">Revisión y Estatus</a></li>
                <li><a href="control/jovenCTL.php?cmdAction=1&id='.$cntt->id_usuario.'">Reenviar Confirmación</a></li>':'';
                echo "<tr>
                    <td>$cntt->folio</td>
                    <td>$cntt->nombre $cntt->apellido_pat $cntt->apellido_mat</td>
                    <td>$cntt->institucion</td>
                    <td>$cntt->correo</td>
                    <td>$cntt->area</td>
                    <td>$cntt->titulo</td>
                    <td>$cntt->evaluacion_previa</td>
                    <td>$cntt->modalidad</td>
                    <td>$cntt->veredicto</td>
                    <td>$cntt->eval1</td>
                    <td>$cntt->eval1_calif</td>
                    <td>$cntt->eval2</td>
                    <td>$cntt->eval2_calif</td>
                    <td>$cntt->eval3</td>
                    <td>$cntt->eval3_calif</td>";
                echo '<td>
                        <div class="btn-group"><button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">Acciones <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="enviar_mail.php?id='.$cntt->id_usuario.'">Enviar Correo</a></li>
                            '.$revision.(($_SESSION['tipo_usuario'] == 1)?'<li><a href="#confirmDialogD'.$cntt->id_usuario.'" data-toggle="modal">Eliminar</a></li>':'').'
                        </ul></div>
                    </td>';
                echo "</tr>";
                echo    '<!-- Modal HTML for Delete -->
                                            <div id="confirmDialogD'.$cntt->id_usuario.'" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Confirmaci&oacute;n</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Esta seguro de eliminar este registro?</p>
                                                            <p class="text-warning"><small>&Eacute;ste se perder&aacute; para siempre si procede...</small></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                            <a class="btn btn-primary" href="control/coordinacionCTL.php?cmdAction=2&id='.$cntt->id_usuario.'">Eliminar</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>';
            }
            ?>
        </tbody>
        <tfoot></tfoot>
    </table>
</div>

<?php include("resources/footer.php");?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="js/footable.min.js"
<script src="js/footable.all.min.js" type="text/javascript"></script>
<script type="text/javascript">$(function(){$('.footable').footable();});</script>
</body>
</html>

<?php

    function row_to_participant($row, $conn){
        $participante = new participante();
        $participante->set_folio($row['folio']);
        $participante->set_nombre($row['nombre']);
        $participante->set_apellido_pat($row['apellido_pat']);
        $participante->set_apellido_mat($row['apellido_mat']);
        $participante->set_correo($row['correo']);
        $participante->set_institucion($row['nombre_institucion']);
        $participante->set_modalidad($row['nombre_modalidad']);
        $participante->set_evaluacion_previa($row['eval_previa']);
        $participante->set_veredicto($row['veredicto']);
        $participante->set_area($row['nombre_area']);
        $participante->set_titulo($row['titulo']);
        $participante->set_id_trabajo($row['id_trabajo']);
        $participante->set_id_usuario($row['id_usuario']);

        if($participante->id_trabajo){
            $id = (int) $participante->id_trabajo;
            $sql2 = "SELECT concat_ws(' ', evaluadores.abreviatura_titulo, usuarios.nombre, usuarios.apellido_pat, usuarios.apellido_mat)
            as nombre, evaluaciones.calif_total
            FROM evaluaciones
            LEFT JOIN evaluadores ON evaluadores.id_evaluador = evaluaciones.id_evaluador
            LEFT JOIN usuarios ON usuarios.correo = evaluadores.correo
            WHERE id_trabajo = $id";
            /** @var mysqli $conn */
            $resultado = $conn->query($sql2);
            if($resultado->num_rows > 0){
                $row = $resultado->fetch_assoc();
                $participante->set_eval1($row['nombre']);
                $participante->set_eval1_calif($row['calif_total']);
                $row = $resultado->fetch_assoc();
                $participante->set_eval2($row['nombre']);
                $participante->set_eval2_calif($row['calif_total']);
                $row = $resultado->fetch_assoc();
                $participante->set_eval3($row['nombre']);
                $participante->set_eval3_calif($row['calif_total']);
            }
        }

        return $participante;
    }
?>